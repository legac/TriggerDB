#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" NAME
        appOnlineL0Rates -- analyse the Online L0 rate

    SYNOPSIS
        appOnlineL0Rates [Options]

    DESCRIPTION
        Analyse online LO Rates for a given fill, run or period of time.
        The data are extracted from the PVSS archive and trending plots.
        They are analysed and plot using the pandas library.
        The output is a PDF file.

    OPTIONS
            --version
                Return the version number of this scripts

            --fill
                fill number

    EXAMPLES

        > lb-run MooreOnline/latest ./appOnlineL0Rates.py -f 2536

    AUTHOR
        R. Le Gac, legac@cppm.in2p3.fr

"""
import matplotlib as mpl
import matplotlib.pyplot as plt
import pandas as pd
import rdbjson
import sys


from appFillDB import create_sensors
from base import log, print_options
from datetime import datetime, timedelta
from matplotlib.backends.backend_pdf import PdfPages
from sensors import (SensorPvss,
                     SensorRdbF,
                     SensorRdbR,
                     SensorSvc,
                     SensorTrend,
                     median)


HZ = 1.
KHZ = 0.001
NOW = datetime.now()


def collect(svc, ds, de):
    """Collect the data and returns a DataFrame.
    Time series are sample on the one minute basis.

    Args:
        svc (SensorSvc): service
        ds (datetime): start date
        de (datetime): end date

    Returns:
        pandas.DataFrame

    """

    data = {}
    sensors = ["L0:DiMuon",
               "L0:Electron",
               "L0:Hadron",
               "L0:Muon",
               "L0:Photon",
               "L0:DiEM,lowMult",
               "L0:DiHadron,lowMult",
               "L0:DiMuon,lowMult",
               "L0:Electron,lowMult",
               "L0:Muon,lowMult",
               "L0:Photon,lowMult",
               "L0:SPDLowMult",
               "TrgRate_L0",
               "TrgRate_L0DiMuon",
               "TrgRate_L0Electron",
               "TrgRate_L0Hadron",
               "TrgRate_L0Muon",
               "TrgRate_L0Photon",
               "TrgRate_L0raw",
               #
               # HLT1 section
               #
               "HltRate_HLT1_DiMuon",
               "HltRate_HLT1_Electron",
               "HltRate_HLT1_Global",
               "HltRate_HLT1_Lumi",
               "HltRate_HLT1_MVA",
               "HltRate_HLT1_NoBias",
               "HltRate_HLT1_SingleMuon",
               "Hlt1MVA Lines",
               "HltRate_HLT1_LowMult",
               "Hlt1CEP Lines",
               "Hlt1Beauty Lines",
               "HltRate_HLT1_Beauty",
               "HltRate_HLT1_Bottomonium",
               "Hlt1Bottomonium Lines",
               "RoutingBit46"]

    svcget = svc.get

    for name in sensors:
        sensor = svcget(name, ds=ds, de=de)
        data[name] = pd.Series(sensor.y, index=sensor.x)

    # re-sample the rate on the minute basis
    # unit is kHz
    df = pd.DataFrame(data)
    df = df.resample("1Min")*KHZ

    # Add mu to the data frame
    sensor = svcget("Mu", ds=ds, de=de)
    ts = pd.Series(sensor.y, index=sensor.x)
    ts = ts.resample("1Min")
    df["Mu"] = ts

    # compute the readout dead time
    df["DeadTime"] = (1 - df['TrgRate_L0'] / df['TrgRate_L0raw']) * 100.

    return df


def get_bb_crossing(svc, ds_fill, de_fill):
    """Return the number of beam-beam crossing for the fill.
    the latter is defined by its starting and ending dates.
    The algorithm is the one defined in appfillDB.

    Args:
        svc (SensorSvc): sensor service
        ds_fill (datetime):
        de_fill (datetime):

    Returns:
        int or None is not found.

    """
    s1 = svc.get('Ncollisions_exp', ds=ds_fill-timedelta(hours=3), de=de_fill)
    if s1.y:
        for i in range(-1, -(len(s1.y)+1), -1):
            if s1.y[i]:
                return s1.y[i]

    return None


def get_dates(svc, opt):
    """Return the start and the end date for the scan. It depends on the
    user options.

    Args:
        svc (SensorSvc): service
        opt (argparse.Namespace): user criteria

    Return:
        tuple: containing datetime object

    """
    svcget = svc.get

    if opt.fill:
        ds_fill = svcget('fillStartTime', fill=opt.fill).value()
        time_total = svcget('time_total', fill=opt.fill).value()
        de_fill = ds_fill + timedelta(seconds=time_total)
        de_fill = de_fill.replace(microsecond=0)

        log("Fill start: %s" % ds_fill)
        log("Fill end  : %s" % de_fill)

    return (ds_fill, de_fill)


def get_runs(svc, fill_number, ds_fill, de_fill):
    """Return a list of dictionary for run belonging to the fill.
    The dictionary contains, the run number as well as the start and end time
    encoded in utc.

    Args:
        svc (SensorSvc): sensor service
        fill_number (int):
        ds_fill (datetime):
        de_fill (datetime):

    Returns:
        list: of dictionary with the keys
            * ds
            * de
            * ds_utc
            * de_utc
            * duration
            * mean_x for a series of sensors
            * median_x for a series of sensors

    """
    runs = []
    sensors = ['Mu',
               'TrgRate_L0',
               'TrgRate_L0raw',
               'TrgRate_L0Hadron',
               'TrgRate_L0Electron',
               'TrgRate_L0Muon',
               'TrgRate_L0DiMuon',
               'TrgRate_L0Photon']

    log("Interrogate the run database....")
    for run_stat in rdbjson.get_runs_for_fill(fill_number):

        run = run_stat["runid"]

        ds_run = svc.get('startTime', run=run).value()
        de_run = svc.get('endTime', run=run).value()

        if  ds_run == None or de_run == None:
            continue

        if ds_run < ds_fill or ds_run > de_fill:
            continue

        if svc.run_stat['LHCState'] != 'PHYSICS':
            continue

        print run, svc.run_stat['LHCState']

        di = dict(run=run, ds=ds_run,
                           de=de_run,
                           duration=(de_run-ds_run).seconds)

        # add median and mean information for the sensors
        # mu, L0Physics, L0Hadron, L0Electron, L0MUon,.....
        for name in sensors:
            si = svc.get(name)
            di['mean_%s' % name] = si.y_mean(ds=ds_run, de=de_run)
            di['median_%s' % name] = si.y_median(ds=ds_run, de=de_run)

        runs.append(di)

    return runs


def l0hadron_pollution(svc, ds_fill, de_fill):
    """Determine the rate of the pollution affecting the L0 Hadron rate
    when it is measured by the L0DU. It happens during ee, eb and be crossing
    and mainly due to to satellite bunches, LED calibration, ....
    It is determined by measuring the rate before data taking start.

    Args:
        svc (SensorSvc): service
        ds_fill (datetime): start of fill
        de_fill (datetime): end of fill

    Returns:
        float: rate in Hz.

    """
    svcget = svc.get

    # Have a look to the rate at the end of the phase adjust
    # The interval is between start of fill - 10 minutes
    # and start of fill - 5 minutes
    s1 = svcget('TrgRate_L0Hadron')
    pollution = s1.y_median(ds=ds_fill-timedelta(minutes=15),
                            de=ds_fill-timedelta(minutes=10),
                            excludeZero=True)

    if pollution is None:
        pollution = 0.0

    # check that the pollution rate is above minimum
    s2 = svc.get('TrgRate_L0Hadron')
    ymin = s2.y_min(ds=ds_fill, de=de_fill-timedelta(minutes=3))

    if ymin is None:
        ymin = 0.0

    if ymin < pollution:
        print 'Reduced pollution rate from %f to %f' % (pollution, ymin)
        pollution = ymin

    return pollution


def page_hlt1(title, df, runs):
    """Plot to control HLT1 rates

    Args:
        title (str):
        df (pandas.DataFrame):
        runs (list):

    """
    fig = plt.figure()
    write_figure_title(fig, title)

    #.........................................................................
    #
    # physics htl1 rates

    ax = plt.subplot(311)

    df["HltRate_HLT1_Physics"] = df["HltRate_HLT1_Global"] - df["HltRate_HLT1_Lumi"]

    li = ["HltRate_HLT1_Global",
          "RoutingBit46",
          "HltRate_HLT1_Physics"]

    df[li].plot(ax=ax, kind="line")
    ax.minorticks_on()
    ax.set_ylabel("HLT1 Rate [kHz]", y=1, horizontalalignment='right')

    show_runs(ax, runs)

    ax = plt.subplot(312)

    li = ["HltRate_HLT1_MVA",
          "HltRate_HLT1_DiMuon",
          "HltRate_HLT1_Electron",
          "HltRate_HLT1_LowMult",
          "HltRate_HLT1_NoBias",
          "HltRate_HLT1_SingleMuon",
          "HltRate_HLT1_Physics"]

    df[li].plot(ax=ax, kind="line")
    ax.minorticks_on()
    ax.set_ylabel("HLT1 Rate [kHz]", y=1, horizontalalignment='right')

    show_runs(ax, runs)


def page_l0_hadron(title, l0hadron_startfill, df, pollution, runs, ds_fill):
    """Plot to control the subtraction of the L0 Hadron pollution.

    Args:
        title (str):
        l0hadron_startfill (pandas.Series): l0 hadron rate at the
            begining of the fill
        df (pandas.DataFRame):
        pollution (float): pollution of the l0 Hadron rate due to satellite
            bunches, LED calibration, ....
        runs (list):
        ds_fill (datetime): start of the fill

    """
    fig = plt.figure()
    write_figure_title(fig, title)

    #.........................................................................
    #
    # the L0 hadron rate at the beginning of the fill

    ax = plt.subplot(311)

    if l0hadron_startfill.size > 0:
        l0hadron_startfill.plot(ax=ax, kind="line", logy=True)
        ax.minorticks_on()
        ax.set_ylabel("L0 Hadron rate [kHz]", y=1, horizontalalignment='right')

        # show the estimate pollution level
        ax.axhline(pollution, linestyle='--', color="grey")

        xmin, xmax = ax.get_xlim()
        x = xmin + (xmax - xmin)/2.
        ax.text(x, pollution, "ESTIMATED POLLUTION LEVEL", fontsize=8, color="grey")

        msg = "The L0DU Hadron rate at the beginning of the fill."
        ax.text(0., 1., msg, fontsize=8, transform=ax.transAxes, style="italic")

        show_fill_start(ax, ds_fill)
        show_runs(ax, runs)

    #.........................................................................
    #
    # the different version of the L0 hadron rates

    ax = plt.subplot(312)

    df[["TrgRate_L0Hadron", "TrgRate_L0Hadron_Corr", "L0:Hadron"]].plot(
        ax=ax,
        kind="line")

    ax.minorticks_on()
    ax.set_ylabel("L0 Hadron rates [kHz]", y=1, horizontalalignment='right')

    show_runs(ax, runs)

    #.........................................................................
    #
    # compare l0HadronCorr and hlt1L0Hadron with the dead time

    ax = plt.subplot(313)

    delta = (1 - df["L0:Hadron"] / df["TrgRate_L0Hadron_Corr"]) * 100.
    di = {"(1 - L0:Hadron / TrgRate_L0Hadron_Corr)": delta,
          "dead time = (1 - L0Physics / l0PhysicsRaw)": df["DeadTime"]}

    pd.DataFrame(di).plot(ax=ax,
                          kind="line",
                          ylim=[0., 20.])

    ax.minorticks_on()
    ax.set_ylabel("percentage [%]",
                  y=1,
                  horizontalalignment='right')

    msg = "Compare L0:Hadron (hlt1), TrgRate_L0Hadron_Corr (L0DU) with dead time..."
    ax.text(0., 1., msg, fontsize=8, transform=ax.transAxes, style="italic")

    show_runs(ax, runs)


def page_l0_lowmult(title, df, runs):
    """show low multiplicity lines

    Args:
        title (str):
        df (pandas.DataFrame):
        runs (list):

    """
    fig = plt.figure()
    write_figure_title(fig, title)

    ax = plt.subplot(311)

    li = ['L0:DiEM,lowMult',
          'L0:DiHadron,lowMult',
          'L0:DiMuon,lowMult',
          'L0:Electron,lowMult',
          'L0:Muon,lowMult',
          'L0:Photon,lowMult',
          'L0:SPDLowMult']

    df[li].plot(ax=ax, kind="line")
    ax.minorticks_on()
    ax.set_ylabel("Rate [kHz]", y=1, horizontalalignment='right')

    show_runs(ax, runs)


def page_l0_rates_1(title, df, runs):
    """L0 Rates Physics, Muon, and Electron.

    Args:
        title (str): title of the figure
        df (pandas.DataFrame):
        runs (list):

    """
    fig = plt.figure()
    write_figure_title(fig, title)

    plt.subplots_adjust(hspace=0.07)

    ax = plt.subplot(311)

    df[["TrgRate_L0raw", "TrgRate_L0"]].plot(
        ax=ax,
        kind="line")

    ax.minorticks_on()
    ax.set_ylabel("L0 Physics rates [kHz]", y=1, horizontalalignment='right')

    show_runs(ax, runs)

    msg = "The difference is due to the readout dead time."
    ax.text(0., 1., msg, fontsize=8, transform=ax.transAxes, style="italic")

    #.........................................................................

    ax = plt.subplot(312)

    df[["TrgRate_L0Muon", "L0:Muon"]].plot(
        ax=ax,
        kind="line",
        sharex=True)

    ax.minorticks_on()
    ax.set_ylabel("L0 Muon rates [kHz]", y=1, horizontalalignment='right')

    show_runs(ax, runs)

    msg = "The difference is due to the readout dead time."
    ax.text(0., 1., msg, fontsize=8, transform=ax.transAxes, style="italic")

    #.........................................................................

    ax = plt.subplot(313)

    df[["TrgRate_L0Electron", "L0:Electron"]].plot(
        ax=ax,
        kind="line",
        sharex=True)

    ax.minorticks_on()
    ax.set_ylabel("L0 Electron rates [kHz]", y=1, horizontalalignment='right')

    show_runs(ax, runs)

    msg = "The difference is due to the readout dead time."
    ax.text(0., 1., msg, fontsize=8, transform=ax.transAxes, style="italic")


def page_l0_rates_2(title, df, runs):
    """L0 Rates Dimuon, Photon, Ratios

    Args:
        title (str): title of the figure
        df (pandas.DataFrame):
        runs (list):

    """
    fig = plt.figure()
    write_figure_title(fig, title)

    plt.subplots_adjust(hspace=0.07)

    ax = plt.subplot(311)

    df[["TrgRate_L0Photon", "L0:Photon"]].plot(
        ax=ax,
        kind="line",
        sharex=True)

    ax.minorticks_on()
    ax.set_ylabel("L0 Photon rates [kHz]", y=1, horizontalalignment='right')

    show_runs(ax, runs)

    msg = "The difference is due to the readout dead time."
    ax.text(0., 1., msg, fontsize=8, transform=ax.transAxes, style="italic")

    #.........................................................................

    ax = plt.subplot(312)

    df[["TrgRate_L0DiMuon", "L0:DiMuon"]].plot(
        ax=ax,
        kind="line",
        sharex=True)

    ax.minorticks_on()
    ax.set_ylabel("L0 DiMuon rates [kHz]", y=1, horizontalalignment='right')

    show_runs(ax, runs)

    msg = "The difference is due to the readout dead time."
    ax.text(0., 1., msg, fontsize=8, transform=ax.transAxes, style="italic")

    #.........................................................................
    #
    # difference between HLT1 and L0DU rates are due to readout deadtime

    ax = plt.subplot(313)

    di = {
        "Dead time": df["DeadTime"],
        "Electron": (1 - df["L0:Electron"] / df["TrgRate_L0Electron"]) * 100.,
        "Hadron": (1 - df["L0:Hadron"] / df["TrgRate_L0Hadron_Corr"]) * 100.,
        "Muon": (1 - df["L0:Muon"] / df["TrgRate_L0Muon"]) * 100.}

    df = pd.DataFrame(di)

    df.plot(
        ax=ax,
        kind="line",
        sharex=True,
        ylim=[0., 20.])

    msg = "Differences between HLT1 and L0DU rates are due to readout dead time..."
    ax.text(0., 1., msg, fontsize=8, transform=ax.transAxes, style="italic")

    ax.minorticks_on()
    ax.set_ylabel("(1-HLT1/L0DU)*100 [%]", y=1, horizontalalignment='right')

    show_runs(ax, runs)


def page_l0_working_point(title, df, runs):
    """The L0 working point: mu, L0 Physics rate, dead time

    Args:
        title (str):
        df (pandas.DataFrame):
        runs (list):

    """
    fig = plt.figure()
    write_figure_title(fig, title)

    plt.subplots_adjust(hspace=0.07)

    ax = plt.subplot(311)

    df['Mu'].plot(ax=ax, kind="line")
    ax.minorticks_on()
    ax.set_ylabel("$\mu$", y=1, horizontalalignment='right')

    show_runs(ax, runs)

    #.........................................................................

    ax = plt.subplot(312)
    df['TrgRate_L0'].plot(ax=ax,
                          kind="line",
                          sharex=True)

    ax.minorticks_on()
    ax.set_ylabel("L0 physics rate [kHz]", y=1, horizontalalignment='right')

    show_runs(ax, runs)

    msg = "The L0 physics rates once the readout dead time is applied."
    ax.text(0., 1., msg, fontsize=8, transform=ax.transAxes, style="italic")

    #.........................................................................

    ax = plt.subplot(313)
    ts = (1 - df['TrgRate_L0'] / df['TrgRate_L0raw']) * 100.
    ts.plot(ax=ax,
            kind="line",
            sharex=True)

    ax.minorticks_on()
    ax.set_ylabel("Dead time [%]", y=1, horizontalalignment='right')

    show_runs(ax, runs)

    msg = "The readout dead time is the difference between L0 Physics raw and L0 Physics."
    ax.text(0., 1., msg, fontsize=8, transform=ax.transAxes, style="italic")


def show_fill_start(ax, ds_fill):
    """Draw the start of fill line.

    Args:
        ax (matplotlib.Axes):
        ds_fill (datetime): start of the fill (PHYSICS declared)

    """
    ax.axvline(ds_fill, linestyle='--', color="red")

    ymin, ymax = ax.get_ylim()
    y = ymin + (ymax - ymin) / 2.

    ax.text(ds_fill, y, "PHYSICS", color="red",
                                   fontsize=8,
                                   rotation="vertical",
                                   va="center")


def show_runs(ax, runs):
    """Draw the run boundaries.

    Args:
        ax (matplotlib.Axes):
        runs (list):

    """
    ymin, ymax = ax.get_ylim()
    y = ymin + (ymax - ymin) / 2.

    for run in runs:
        x = run["ds"]
        ax.axvline(x, linestyle="--", color="violet")
        ax.text(x, y, run["run"], color="violet",
                                  fontsize=8,
                                  rotation="vertical",
                                  va="center")


def write_figure_title(fig, title):
    """Write the title for the figure.

    Args:
        ax (matplotlib.Figure):
        title (str)

    """
    msg = NOW.strftime("%b %d, %Y %H:%M\nday %j -- week %W")

    fig.text(0.05, 0.9, title, fontsize=14)
    fig.text(0.94, 0.89, msg, fontsize=8, horizontalalignment='right')


if __name__ == "__main__":


    from argparse import ArgumentParser

    #--------------------------------------------------------------------------
    #
    # script options
    #
    #--------------------------------------------------------------------------
    AGP = ArgumentParser()

    AGP.add_argument(
        "-d", "--debug",
        action="store_true",
        help="Activate the debug mode [%default].")

    AGP.add_argument(
        "-f", "--fill",
        help="Select a fill number.",
        metavar="<number>",
        type=int)

    ARGS = AGP.parse_args()

    log("Start appOnlineL0Rates.py")
    print_options(vars(ARGS))

    if not ARGS.fill:
        print "\n\tFill number is missing.\n"
        sys.exit(1)

    #.........................................................................
    #
    # collect main rates for the selected period

    svc = create_sensors()
    ds, de = get_dates(svc, ARGS)
    df = collect(svc, ds, de)

    #.........................................................................
    #
    # collect additional data for the selected period
    #  - ee pollution on the L0 Hadron rate (satelite bunches)
    #  - number of colliding bunches
    #  - list of runs

    delta = timedelta(minutes=20)

    sensor = svc.get('TrgRate_L0Hadron', ds=ds-delta, de=ds+delta)
    ts = pd.Series(sensor.y, index=sensor.x)
    ts_l0hadron_startfill= ts.resample("1Min")*KHZ

    pollution = l0hadron_pollution(svc, ds, de)*KHZ
    df["TrgRate_L0Hadron_Corr"] = df["TrgRate_L0Hadron"] - pollution

    nbb = get_bb_crossing(svc, ds, de)
    runs = get_runs(svc, ARGS.fill, ds, de)

    log("Number of crossing: %i" % nbb)
    log("Median Mu for fill: %.2f" % (df['Mu'].median(),))
    log("Median L0Raw for fill: %.3f kHz" % (df['TrgRate_L0raw'].median(),))
    log("Median readout dead time: %.1f %%" % (df["DeadTime"].median(),))
    log("L0 Hadron pollution (satellite bunches): %.3f kHz" % pollution)

    #.........................................................................
    #
    # configure matplotlib

    mpl.rcParams['axes.labelsize'] = 9
    mpl.rcParams['figure.figsize'] = (29.7 / 2.54, 21 / 2.54)
    mpl.rcParams['figure.subplot.top'] = 0.85
    mpl.rcParams['legend.fontsize'] = 7
    mpl.rcParams['lines.linewidth'] = 1.
    mpl.rcParams['xtick.major.size'] = 8
    mpl.rcParams['xtick.minor.size'] = 4
    mpl.rcParams['xtick.labelsize'] = 8
    mpl.rcParams['ytick.major.size'] = 8
    mpl.rcParams['ytick.minor.size'] = 4
    mpl.rcParams['ytick.labelsize'] = 8

    #.........................................................................
    #
    # create the pdf file

    pdf = PdfPages('fill_%i.pdf' % ARGS.fill)
    prefix = 'Fill %i -- %i bb' % (ARGS.fill, nbb)

    #.........................................................................
    #
    # generate figures

    title = '%s -- L0 working point ' % prefix
    page_l0_working_point(title, df, runs)
    pdf.savefig()

    title = '%s -- L0 Hadron ' % prefix
    page_l0_hadron(title, ts_l0hadron_startfill, df, pollution, runs, ds)
    pdf.savefig()

    title = '%s -- L0 Rates -- Physics, Muon, and Electron' % prefix
    page_l0_rates_1(title, df, runs)
    pdf.savefig()

    title = '%s -- L0 Rates -- Dimuon, Photon and ratio Hlt1/L0DU' % prefix
    page_l0_rates_2(title, df, runs)
    pdf.savefig()

    title = '%s -- L0 LowMult Rates' % prefix
    page_l0_lowmult(title, df, runs)
    pdf.savefig()

    title = '%s -- HLT1 Rates' % prefix
    page_hlt1(title, df, runs)
    pdf.savefig()

    #.........................................................................
    #

    log("End appOnlineL0Rates.py")

    pdf.close()
    sys.exit(0)
