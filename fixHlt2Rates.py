#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" NAME
        fixHlt2Rates -- fix Hlt2 rates

    SYNOPSIS
        fixHlt2Rates [Options]

    DESCRIPTION
        In June 2016, add the HltRate_Hlt2_* sensors and a corresponding
        entry in the database.

        The script recover the history.

    OPTIONS

    EXAMPLES

        > SetupProject MooreOnline
        > ./fixHlt2Rates.py

    AUTHOR
        R. Le Gac, legac@cppm.in2p3.fr

"""
import sys

from appFillDB import create_sensors, insert_or_update, SENSORS_HLT2_RATES
from base import (create_db,
                  log,
                  print_options)

def fix(db, svc, opt):
    """Fill measures_hlt1_l0rates.mdnrun_l0_lowmult for 2016

    Args:
        db (sqlite3.connection): database connection
        svc (SensorSvc): sensors service
        opt (argparse.Namespace): user criteria

    """
    svcget = svc.get

    # existing runs
    sql = """SELECT id, id_measures_fills, run_number
             FROM measures_runs
             WHERE substr(start_time, 1, 4) = '2016'
    """

    for row in db.execute(sql):

        id_fill = row["id_measures_fills"]
        id_run = row["id"]
        run = row["run_number"]

        data = dict(id_measures_fills=id_fill,
                    id_measures_runs=id_run)

        sql = """SELECT id
                 FROM measures_hlt2_rates
                 WHERE id_measures_fills = ? and id_measures_runs = ? """

        row1 = db.execute(sql, (id_fill, id_run)).fetchone()
        if row1 is not None:
            data["id"] = row1["id"]

        for name in SENSORS_HLT2_RATES:

            tmp = name.lower().replace("hltrate_", "").replace("_rate", "")
            field_name = "mdnrun_%s" % tmp

            if name in svc:
                s1 = svcget(name, run=run)
                value = s1.y_median(n=2, excludeZero=True, nz=True)
                data[field_name] = value

        log("Update %i" % run)
        insert_or_update(db, "measures_hlt2_rates", **data)


if __name__ == "__main__":

    from argparse import ArgumentParser

    #.........................................................................
    #
    # user criteria
    #

    AGP = ArgumentParser()

    AGP.add_argument(
        "--db",
        default="triggerdb.sqlite",
        help="File name for the sqlite database [%(default)s].")

    ARGS = AGP.parse_args()

    log("Start fixHlt2Rates.py")
    print_options(vars(ARGS))

    #.........................................................................
    #
    # data base connection and sensors
    #

    DB = create_db(ARGS.db)
    SVC = create_sensors()

    #.........................................................................
    #
    # do the job and exit
    #

    fix(DB, SVC, ARGS)

    log("End fixHlt2Rates.py")
    sys.exit(0)

