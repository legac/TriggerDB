#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" NAME
        appReport -- build report using the trigger database

    SYNOPSIS
        appReport [Options]

    DESCRIPTION
        Build report using the trigger database.
        Report are export as file.
        Possible format are csv, html and json.

    OPTIONS
        --version
            Return the version number of this scripts

    EXAMPLES

        > lb-run MooreOnline ./appReport.py -h

    AUTHOR
        R. Le Gac, legac@cppm.in2p3.fr

"""
import csv
import os
import re
import sys

# Application requires Moore to get the correct version of python
if ("GAUDIROOT" not in os.environ) and ("MOOREROOT" not in os.environ):
    print "\n\tDefine the MooreOnline environment !"
    print "\t> lb-run MooreOnline/latest ./appReport.py ....\n"
    sys.exit(1)

from base import (create_db,
                  create_sql_functions,
                  get_beam_energy,
                  is_inhibit,
                  log,
                  print_options)
from datetime import datetime, timedelta


# helper function
FN_BASE = \
lambda s, opt: '_'.join(['last_report', str(opt.year), str(int(get_beam_energy(opt))), s])

# constants
HTML_HEAD = \
"""<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>%s</title>
    </head>
"""

BLUE_GREY = '#e6e6ff'
YELLOW4 = '#e6e64c'

DATE_FORMAT = '%Y-%m-%d'
DATETIME_FORMAT = DATE_FORMAT + ' %H:%M:%S'

RX_DATETIME = re.compile(r'\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}')
RX_DATE = re.compile(r'\d{4}-\d{2}-\d{2}')
RX_DURATION = re.compile(r'(\d{2}):(\d{2}):(\d{2})')


class Table(list):
    """ class Table
    Transform a select request on the database to a csv table.
    Additional processing is possible on columns: cumul, ....

    """
    def __init__(self, name, cursor, precision=2):
        """ Fill the table from a database cursor (sqlite3).
        None value are replaced by an empty string.

        """
        list.__init__(self)

        self.header = [el[0] for el in cursor.description]
        self.name = name

        for row in cursor:
            self.append([])
            for col in self.header:
                v = row[col]
                if v is None:
                    v = ''
                elif isinstance(v, float):
                    v = round(v, precision)
                self[-1].append(v)

    def col_index(self, name):
        """ return the column index.

        """
        return self.header.index(name)

    def cumulate(self, source=None, target=None):
        """ Fill the column target by cumulate data in column source.

        """
        i_src = self.col_index(source)
        i_trg = self.col_index(target)

        value = 0.0
        for row in self:
            value += row[i_src]
            row[i_trg] = value

    def row_number(self, column):
        """ replace the content of column with the row number.
        The row number start at one.

        """
        i_col = self.col_index(column)
        for i in range(len(self)):
            self[i][i_col] = i + 1

    def to_csv(self, file_name):
        """ write the table in a CSV file.

        """
        fn = open(file_name, "wb")
        writer = csv.writer(fn, quoting=csv.QUOTE_NONNUMERIC)
        writer.writerow(self.header)
        writer.writerows(self)
        fn.close()
        log("CSV file %s written" % file_name)

    def to_json(self, file_name):
        """ Dump the table as a JSON string embedded in an HTML file.
        It is useful to produce charts using ExtJS library.

        """
        fn = open(file_name, "wb")

        lines = ['{']
        lines.append('metaData:{')
        lines.append('"root":"rows",',)
        lines.append('"fields":%s,' % self.header)
        lines.append('},')
        lines.append('"rows":[')

        for row in self:
            di = {}
            for i in range(len(self.header)):
                val = row[i]
                if isinstance(val, unicode):
                    val = val.encode('utf8')
                di[self.header[i]] = val
            lines.append('%s,' % str(di))

        lines.append(']}')
        fn.writelines(lines)

        fn.close()
        log("HTML JSON file %s written" % file_name)

    def to_html(self, file_name):
        """ write the table as an HTML table.

        """
        fn = open(file_name, "wb")
        fn.write(HTML_HEAD % file_name)
        fn.write('<body><table border=1>')

        fn.write('<tr>')
        for el in self.header:
            s = el.replace('_', ' ').replace('*', ' ')
            s = s.replace('(', '').replace(')', '')
            fn.write('<th>%s</th>' % s)
        fn.write('</tr>')

        for row in self:
            fn.write('<tr>')
            for el in row:
                fn.write('<td>%s</td>' % el)
            fn.write('</tr>')

        fn.write('</table></body></html>')
        fn.close()
        log("HTML file %s written" % file_name)


def to_date(value):
    """Helper function converting a date, date time or duration string
    into a Date, Date Time or TimeDelta objects.

    """
    if not isinstance(value, (str, unicode)):
        return value

    match = RX_DATETIME.match(value)
    if match:
        return datetime.strptime(value, DATETIME_FORMAT)

    match = RX_DATE.match(value)
    if match:
        return datetime.strptime(value, DATE_FORMAT)

    match = RX_DURATION.match(value)
    if match:
        return timedelta(hours=int(match.group(1)),
                         minutes=int(match.group(2)),
                         seconds=int(match.group(3)))

    return value


def report_day(db, opt):
    """ Build a report where measurements are computed on a day basis.

    Args:
        db (sqlite3.connection): database connection
        opt (argparse.Namespace): command line arguments

    Returns:
        Table

    """

    sql = """SELECT month,
                    week,
                    day,
                    delivered_lumi*pb() as delivered_lumi_pb,
                    recorded_lumi*pb() AS recorded_lumi_pb,
                    time_total*s() AS time_total_delivered_s
             FROM measures_days
             WHERE year=?
             ORDER BY day
          """

    tb = Table('day', db.execute(sql, (opt.year,)))
    return tb


def report_fill(db, opt):
    """Build a report where measurements are computed on a fill basis.

    Args:
        db (sqlite3.connection): database connection
        opt (argparse.Namespace): command line arguments

    Returns:
        Table

    """
    sql = \
    """SELECT fill_number AS fill,
              zip_tcks(group_concat(tck2s(tck))) as tcks,
              magnet_polarity AS polarity,
              day,
              measures_fills.start_time AS start_time,
              strftime('%H:%M:%S', '00:00:00', time_total || 'seconds') AS duration,
              lumi_total*nb() AS delivered_lumi_nb,
              lumi_logged*nb() AS recorded_lumi_nb,
              lumi_logged*100/lumi_total AS efficiency_pct,
              (lumi_total-lumi_hvon)*100/lumi_total AS lost_hvon_pct,
              (lumi_hvon-lumi_veloin)*100/lumi_total AS lost_veloin_pct,
              (lumi_veloin-lumi_running)*100/lumi_total AS lost_running_pct,
              (lumi_running-lumi_logged)*100/lumi_total AS lost_logged_pct,
              '' AS cumu_delivered_lumi_nb,
              '' AS cumu_recorded_lumi_nb,
              crossing_bunches,
              max(mu_at_peak) AS max_mu,
              max(lumi_at_peak)*pubs() AS max_lumi_pubs,
              max(bb_rate_at_peak)*kHz() AS max_bb_rate_kHz,
              max(l0_rate_at_peak)*kHz() AS max_l0_rate_kHz,
              max(hlt2_rate_at_peak)*kHz() AS max_hlt_rate_kHz,
              max(hlt2_physics_rate_at_peak)*kHz() AS max_hlt_phyiscs_rate_kHz,
              max(avg_hlt_time_at_peak)*ms(),
              avg(beta_star) AS avg_beta_star,
              avg(avg_evt_size)*kB() AS avg_evt_size_input_Hlt_kB,
              mdnfill_bb_rate*kHz() AS avg_bb_colliding_rate_kHz,
              mdnfill_l0_physics_rate*kHz() AS avg_l0_physics_rate_kHz,
              median(mdnrun_total_hlt_rate, run_duration)*kHz() AS avg_hlt_rate_kHz,
              median(mdnrun_total_hlt_physics_rate, run_duration)*kHz() AS avg_hlt_physics_rate_kHz,
              mdnfill_lumi*pubs() AS avg_lumi_pubs,
              mdnfill_mu AS avg_mu,
              median(mdnrun_hlt_time, run_duration)*ms() AS avg_hlt_time_ms
       FROM measures_runs,
            axis_times,
            axis_triggers,
            measures_fills,
            measures_triggers
       WHERE measures_runs.id_axis_times = axis_times.id
       AND   measures_runs.id_axis_triggers = axis_triggers.id
       AND   measures_runs.id_measures_fills = measures_fills.id
       AND   measures_runs.id_measures_triggers = measures_triggers.id
       AND   year=?
       GROUP BY fill_number
       ORDER BY fill_number
    """

    tb = Table('fill', db.execute(sql, (opt.year,)))
    tb.cumulate(source='delivered_lumi_nb', target='cumu_delivered_lumi_nb')
    tb.cumulate(source='recorded_lumi_nb', target='cumu_recorded_lumi_nb')
    return tb


def report_hlt2(db, opt):
    """Build a report showing the list of run not yet processed by HLT2.

    Args:
        db (sqlite3.connection): database connection
        opt (argparse.Namespace): command line arguments

    Returns:
        Table

    """
    # SQL template get the list of runs not yet processed by HLT2
    # Avoid the early measurement period at the beginning of the Run 2
    # A that time HLT2 was not working.
    #
    sql = \
    """SELECT fill_number AS fill,
              run_number AS run,
              measures_runs.start_time AS start_time,
              measures_runs.end_time AS end_time,
              tck2s(tck) AS tck
       FROM measures_fills,
            measures_runs,
            measures_streams,
            axis_times,
            axis_triggers
       WHERE measures_streams.id_measures_runs = measures_runs.id
       AND   measures_streams.id_measures_fills = measures_fills.id
       AND   measures_runs.id_axis_times = axis_times.id
       AND   measures_runs.id_axis_triggers = axis_triggers.id
       AND   run_number > 160000
       AND   measures_streams.mdnrun_full_physstat ISNULL
       AND   year = ?
       ORDER BY fill_number, run_number
    """
    tb = Table('hlt2', db.execute(sql, (opt.year,)))
    return tb


def report_l0(db, opt):
    """Build a report where L0 measurements are computed on a run basis.

    Args:
        db (sqlite3.connection): database connection
        opt (argparse.Namespace): command line arguments

    Returns:
        Table

    """
    sql = \
    """SELECT fill_number AS fill,
              run_number AS run,
              tck2s(tck) AS tck,
              magnet_polarity AS polarity,
              day,
              measures_runs.start_time AS start_time,
              measures_runs.end_time AS end_time,
              run_duration*s(),
              integrated_lumi*nb(),
              mdnrun_l0du_electron_rate*kHz(),
              mdnrun_l0du_hadron_rate*kHz(),
              mdnrun_l0du_muon_rate*kHz(),
              mdnrun_hlt_l0_hadron_rate*kHz(),
              mdnrun_hlt_l0_muon_dimuon_rate*kHz(),
              mdnfill_led_rate*kHz()
       FROM measures_runs,
            axis_times,
            axis_triggers,
            measures_fills,
            measures_triggers
       WHERE measures_runs.id_axis_times = axis_times.id
       AND   measures_runs.id_axis_triggers = axis_triggers.id
       AND   measures_runs.id_measures_fills = measures_fills.id
       AND   measures_runs.id_measures_triggers = measures_triggers.id
       AND   is_collision_alldetector_veloclosed=1
       AND   year=?
       ORDER BY run_number
    """

    tb = Table('l0', db.execute(sql, (opt.year,)))
    return tb


def report_run(db, opt):
    """Build a report where measurements are computed on a run basis.

    Args:
        db (sqlite3.connection): database connection
        opt (argparse.Namespace): command line arguments

    Returns:
        Table

    """
    sql = \
    """SELECT run_number AS run,
              tck2s(tck) AS tck,
              magnet_polarity AS polarity,
              day,
              measures_runs.start_time AS start_time,
              measures_runs.end_time AS end_time,
              strftime('%H:%M:%S', '00:00:00', run_duration || 'seconds') AS duration,
              integrated_lumi*nb(),
              mu_at_peak,
              crossing_bunches,
              beta_star,
              lumi_at_peak*pubs(),
              bb_rate_at_peak*kHz(),
              l0_rate_at_peak*kHz(),
              hlt2_rate_at_peak*kHz(),
              hlt2_physics_rate_at_peak*kHz(),
              avg_hlt_time_at_peak*ms(),
              avg_evt_size*kB(),
              mdnrun_bb_rate*kHz(),
              mdnrun_l0_physics_rate*kHz(),
              mdnrun_total_hlt_rate*kHz(),
              mdnrun_total_hlt_physics_rate*kHz(),
              mdnrun_lumi*pubs(),
              mdnrun_mu,
              mdnrun_hlt_time*ms(),
              fill_number AS fill
       FROM measures_runs,
            axis_times,
            axis_triggers,
            measures_fills,
            measures_triggers
       WHERE measures_runs.id_axis_times = axis_times.id
       AND   measures_runs.id_axis_triggers = axis_triggers.id
       AND   measures_runs.id_measures_fills = measures_fills.id
       AND   measures_runs.id_measures_triggers = measures_triggers.id
       AND   is_collision_alldetector_veloclosed=1
       AND   year=?
       ORDER BY run_number
    """

    tb = Table('run', db.execute(sql, (opt.year,)))
    return tb


def report_tck(db, opt):
    """Build a report where measurements are computed on a tck basis.

    Args:
        db (sqlite3.connection): database connection
        opt (argparse.Namespace): command line arguments

    Returns:
        Table

    """

    sql = \
    """SELECT tck2s(tck) AS tck,
              total(integrated_lumi)*nb() as integrated_lumi_nb,
              total(CASE WHEN magnet_polarity='UP' THEN integrated_lumi END)*nb() AS integrated_lumi_up_nb,
              total(CASE WHEN magnet_polarity='DOWN' THEN integrated_lumi END)*nb() AS integrated_lumi_dw_nb,
              max(day)-min(day) AS days,
              nickname
       FROM measures_runs,
            axis_times,
            axis_triggers
       WHERE measures_runs.id_axis_times = axis_times.id
       AND   measures_runs.id_axis_triggers = axis_triggers.id
       AND   year=?
       GROUP BY tck
    """

    tb = Table('tck', db.execute(sql, (opt.year,)))
    return tb


def report_tckl0(db, opt):
    """Build a report where measurements are computed on a tck basis.

    Args:
        db (sqlite3.connection): database connection
        opt (argparse.Namespace): command line arguments

    Returns:
        Table

    """

    sql = \
    """SELECT tck2s(l0) AS tck_l0,
              total(integrated_lumi)*nb() as integrated_lumi_nb,
              total(CASE WHEN magnet_polarity='UP' THEN integrated_lumi END)*nb() AS integrated_lumi_up_nb,
              total(CASE WHEN magnet_polarity='DOWN' THEN integrated_lumi END)*nb() AS integrated_lumi_dw_nb,
              max(day)-min(day) AS days
       FROM measures_runs,
            axis_times,
            axis_triggers
       WHERE measures_runs.id_axis_times = axis_times.id
       AND   measures_runs.id_axis_triggers = axis_triggers.id
       AND   year=?
       GROUP BY l0
    """

    tb = Table('tckl0', db.execute(sql, (opt.year,)))
    return tb


def report_week(db, opt):
    """ Build a report where measurements are computed on a week basis.

    Args:
        db (sqlite3.connection): database connection
        opt (argparse.Namespace): command line arguments

    Returns:
        Table

    """

    sql = """SELECT month,
                    week,
                    total(delivered_lumi)*pb() as delivered_lumi_pb,
                    total(recorded_lumi)*pb() AS recorded_lumi_pb,
                    total(time_total)*s() AS time_total_delivered_s,
                    total(time_total)*s()/604800 AS hubner_factor
             FROM measures_days
             WHERE year=?
             GROUP BY week
             ORDER BY week
          """

    tb = Table('week', db.execute(sql, (opt.year,)))
    return tb


def do_report(db, opt):
    """ build all reports according to user options.

    Args:
        db (sqlite3.connection): database connection
        opt (argparse.Namespace): command line arguments

    """
    # build Table
    tb_week = report_week(db, opt)
    tb_day = report_day(db, opt)
    tb_fill = report_fill(db, opt)
    tb_hlt2 = report_hlt2(db, opt)
    tb_l0 = report_l0(db, opt)
    tb_run = report_run(db, opt)
    tb_tck = report_tck(db, opt)
    tb_tckl0 = report_tckl0(db, opt)

    # convert tables to CVS/HTML/JSON files
    tables = \
    (tb_week, tb_day, tb_fill, tb_l0, tb_hlt2, tb_run, tb_tck, tb_tckl0)

    for tb in tables:

        fn = FN_BASE(tb.name, opt)

        if opt.csv:
            tb.to_csv('%s.csv' % fn)

        if opt.html:
            tb.to_html('%s.html' % fn)

        if opt.json:
            tb.to_json('%s.json' % fn)


if __name__ == "__main__":

    from argparse import ArgumentParser

    # Define and process script options
    AGP = ArgumentParser()

    AGP.add_argument(
        "-C", "--csv",
        action="store_true",
        help="Save reports in CSV files [%(default)s].")

    AGP.add_argument(
        "--db",
        default="triggerdb.sqlite",
        help="File name for the sqlite database [%(default)s].",
        metavar="<name>")

    AGP.add_argument(
        "-H", "--html",
        action="store_true",
        help="Save reports in HTML files [%(default)s].")

    AGP.add_argument(
        "-J", "--json",
        action="store_true",
        help="Save reports in JSON files [%(default)s].")

    AGP.add_argument(
        "-y", "--year",
        default=datetime.now().year,
        help="Select the year [%(default)s].",
        metavar="<yyyy>",
        type=int)

    ARGS = AGP.parse_args()

    log("Start appReport.py")
    print_options(vars(ARGS))

    # Connection to the database
    DB = create_db(ARGS.db)
    create_sql_functions(DB)

    # inhibit state exit
    if is_inhibit(DB, ARGS.year):
        log("database in inhibit state, exit.")
        sys.exit(0)

    # create report
    do_report(DB, ARGS)

    # exit gently
    log("End appReport.py")
    sys.exit(0)
