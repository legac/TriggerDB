#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" NAME
        appCtrl - Control the state of the database

    SYNOPSIS
        appCtrl [Options]

    DESCRIPTION
        Control the state of the database.
        Three states are available:

            None
                Process can read and write in the database

            Lock
                The database is lock by another process.
                Current process can't write in the database.
                After 3 hours, the current process can remove the lock
                and write in the database.

            Inhibit
                Process are not authorised to read or write in the database
                and should exit immediately. It is useful to inhibit process
                running in cron job and to stop the use of PVSS.
                Useful in debug, recovering phases.

    OPTIONS

        -i, --inhibit
        -l, --lock
        -r, --reset
        -s, --status

    EXAMPLES

        > ./appCtrl.py -h

    AUTHOR
        R. Le Gac, legac@cppm.in2p3.fr

"""
import argparse
import re
import sys


from base import (INHIBIT_FLAG,
                  db_name,
                  create_db,
                  db_name,
                  unlock)
from datetime import datetime


RX_DATE = re.compile('\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}')


class Inhibit(argparse.Action):
    """Set inhibit state.

    """
    def __call__(self, parser, namespace, values, option_string):

        db = create_db(namespace.db)
        year = namespace.year

        sql = 'UPDATE scans SET status=? WHERE year=?'
        db.execute(sql, (INHIBIT_FLAG, year))

        print "\tInhibit state is set (%s)." % year


class Lock(argparse.Action):
    """Set the lock state.

    """
    def __call__(self, parser, namespace, values, option_string):

        db = create_db(namespace.db)
        year = namespace.year

        sql = 'UPDATE scans SET status=? WHERE year=?'
        db.execute(sql, (datetime.now().strftime('%Y-%m-%d %H:%M:%S'), year))

        print "\tLock sate is set (%s)." % year


class Reset(argparse.Action):
    """Reset the state and exit.

    """
    def __call__(self, parser, namespace, values, option_string):

        db = create_db(namespace.db)
        year = namespace.year

        unlock(db, year)
        print "\tThe lock/inhibit state is reset (%s)." % year


class Status(argparse.Action):
    """Print the lock state as a function of year.

    """
    def __call__(self, parser, namespace, values, option_string):

        db = create_db(namespace.db)

        sql = "select status, year from scans"
        for row in db.execute(sql):
            status = row["status"]
            year = row["year"]

            if status == None:
                print '\t(%s) Running state' % year

            elif status == INHIBIT_FLAG:
                print '\t(%s) Inhibit state' % year

            elif RX_DATE.match(status):
                print '\t(%s) Locked state  %s' %(year, status)

        sys.exit(0)


if __name__ == "__main__":

    AGP = argparse.ArgumentParser()

    AGP.add_argument(
        "--db",
        default='triggerdb.sqlite',
        help="File name for the sqlite database [%(default)s]."
             "The special name ':memory:' create the database in RAM.",
        metavar="<name>")

    AGP.add_argument(
        "-i", "--inhibit",
        action=Inhibit,
        help="Activate the inhibit state and exit.",
        nargs=0)

    AGP.add_argument(
        "-l", "--lock",
        action=Lock,
        help="Activate the lock state and exit.",
        nargs=0)

    AGP.add_argument(
        "-r", "--reset",
        action=Reset,
        help="Reset the lock / inhibit state and exit.",
        nargs=0)

    AGP.add_argument(
        "-s", "--status",
        action=Status,
        help="print the database state and exit.",
        nargs=0)

    AGP.add_argument(
        "-y", "--year",
        default=datetime.now().year,
        help="Select the year [%(default)s].",
        metavar="<yyyy>",
        type=int)

    OPT = AGP.parse_args()

    print "\n\tSelect one option: --inhibit, --lock, --reset or --status.\n"
    sys.exit(0)