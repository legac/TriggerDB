"""Sensors module

A collection of classes to extract and manipulate data from the PVSS archive,
the online monitoring tasks and the run database.

A sensor class is associated to each source.
Sensor are manipulated through the sensors service SensorSvc.
It has two main methods register and get.

Four steps to run a sensor:
    1. create the sensors service once
    2. register the sensor once
    3. get and read the sensor for a run, a period of time,....
    4. manipulate the sensor data, getting average, maximum, rounding, ....

"""
import os
import sys
import csv
import re
import rdbjson
import ROOT
import time


from base import median
from datetime import datetime, timedelta
from pprint import pprint
from subprocess import call, Popen
from tempfile import TemporaryFile
from threading import Timer


IS_FLOAT = re.compile("^\d+\.\d+$")
IS_INT = re.compile("^\d+$")

# constant defining the path for histograms savesets
PATH_BYRUN = '/hist/Savesets/ByRun/%s/%i0000/%i000/%s-run%i.root'
DIR_EOR = '/hist/Savesets/%i/LHCb/%s/%02i/%02i'

# constant defining the timeout when querying PVSS archive
# NOTE: Clara recommend a timeout of 2 minutes
# Increase to 5 minutes in order to avoid timeout getting Disk Usage data....
TIMEOUT = 300.
WAITTIME = 2.

# constant handling time conversion
SECOND = 1.
MINUTE = 60.
HOUR = 3600.

# constant defining stream when using the run database
STREAMS = ("BEAMGAS", "CALIB", "EXPRESS", "FULL", "LUMI", "TURBO", "TURCAL")


class SensorException(Exception):
    """Exception for sensors classes

    """


class SensorSvc(dict):
    """The sensor service holding a collection of sensors
    (PVSS archive, monitoring, run database)

    """
    def __init__(self):

        dict.__init__(self)

        # cache containing the open ROOT files
        self.tfiles = {}

        # cache containing last fill information
        self.fill = None  # current fill number
        self.fill_stat = {}  # stat for the current fill

        # cache containing last run information
        self.run = None  # current run number
        self.run_stat = {}  # stat for the current run
        self.run_data = {}  # run data for each stream

        for el in STREAMS:
            self.run_data[el] = {}

    def get(self, name, **kwargs):
        """Return a sensor identifies by its name.
        Read it when args / kwargs are specified.
        The argument depends on the sensor.
        They are those used by the read method

        """
        if kwargs:
            return self[name].read(**kwargs)

        else:
            return self[name]

    def load_fill(self, fill):
        """load fill information in the cache.
        Such information is shared by several sensors.

        Args:
            fill (int, str or dict): either the fill identifier or the
                fill_stat dictionary return by the run database JSON API
                (rdbjson.get_fill_indo, get_physics_fills)

        """
        new_fill = False

        if isinstance(fill, (int, str, unicode)) and fill != self.fill:

            fill_stat = rdbjson.get_fill_info(fill)

            if "fill_id" not in fill_stat:
                msg = "fill %s not found in the run database." % fill
                raise Exception(msg)

            new_fill = True


        elif isinstance(fill, dict) and fill["fill_id"] != self.fill:

            new_fill = True
            fill_stat = fill


        # data manipulation, conversion, backward compatibility, ....
        if new_fill:

            string_to_number(fill_stat)
            self.fill = fill_stat["fill_id"]

            # NOTE
            # preserve the reference to the initial directory
            self.fill_stat.clear()
            self.fill_stat.update(fill_stat)

            start_date = fill_stat["start_date"]
            start_date = start_date.replace("z", "")

            ds = string_to_date(start_date, "%Y-%m-%dT%H:%M:%S")
            self.fill_stat['startTime'] = ds

    def load_run(self, run):
        """load run information in the cache.
        Such information is shared by several sensors.

        Args:
            run (int or dict): either the run identifier or the
                dictionary return by the run database JSON API
                (rdbjson.get_run_data, get_run_info, ...)

        """
        new_run = False

        # a run number
        if isinstance(run, int) and run != self.run:

            data = rdbjson.get_run_data(run)

            if "run_data" not in data:
                msg = "run %s not found in the run database." % run
                raise Exception(msg)

            new_run = True

        # a dictionary with only run_stat information
        elif isinstance(run, dict) \
            and "runid" in run and run["runid"] != self.run:

            data = rdbjson.get_run_data(run["runid"])

            if "run_data" not in data:
                msg = "run %s not found in the run database." % run
                raise Exception(msg)

            new_run = True

        # a dictionary with run_stat and
        elif isinstance(run, dict) \
            and "run_data" in run and run["run_data"]["runid"] != self.run:

            data = run_data
            new_run = True

        # data manipulation, conversion, backward compatibility, ....
        if new_run:

            run_stat, run_data = data["run_data"], data["stream_data"]
            string_to_number(run_stat)

            run_stat["starttime"] = \
                string_to_date(run_stat["starttime"], "%Y-%m-%dT%H:%M:%S")

            run_stat["endtime"] = \
                string_to_date(run_stat["endtime"], "%Y-%m-%dT%H:%M:%S")

            # NOTE
            # preserve the reference to the initial directories
            self.run = run_stat["runid"]

            self.run_stat.clear()
            self.run_stat.update(run_stat)

            for el in STREAMS:
                self.run_data[el].clear()
                if el in run_data:
                    self.run_data[el].update(run_data[el])

    def load_tfile(self, fn):
        """load the TFile in the cache
        TFiles are shared by several sensors.
        If the file does not exist is it registered as None.

        """
        if fn not in self.tfiles:
            if not os.path.exists(fn):
                print 'No monitoring file %s !' % fn
                self.tfiles[fn] = None

            else:
                self.tfiles[fn] = ROOT.TFile(fn, "READ")

        return self.tfiles[fn]

    def register(self, name, sensor_cls, *args, **kwargs):
        """Register a sensor by its name. Return the sensor instance.

        The type of sensor is defined by the argument keyword used.
        If more than one keyword arguments is used an error is raised.

            name
                name of the sensor

            sensor_cls
                class of the sensor SensorMnt, SensorPvss,...

            kwargs
                dictionary with keyword arguments of the sensor constructor

        """
        if name in self:
            raise Exception('Name %s already exits.' % name)

        self[name] = sensor_cls(self, name, *args, **kwargs)

        return self[name]


class SensorBase(object):
    """Base class to create a sensor.

    Args:
        svc (SensorSvc):
        name (str): name of the sensor

    """

    def __init__(self, svc, name):
        self.debug = False
        self.name = name
        self.svc = svc
        self.y = None

    def get_kwargs(self, *args, **kwargs):
        """Helper method to check and to get the value of the keyword arguments

        """
        li = []
        for arg in args:

            if arg not in kwargs:
                raise SensorException('Keyword argument %s is missing' % arg)

            li.append(kwargs[arg])

        return tuple(li)

    def is_empty(self):
        """Return True is the internal list of values is empty.

        """
        return not self.y

    def prtdbg(self, debug_flag=False):
        """Abstract method printing debug information
        when the debug attribute is set to true.

        """
        if self.debug or debug_flag:
            print '-' * 80
            print self.name
            pprint(self.y)
            print '-' * 80

    def read(self, **kwargs):
        """Abstract method that should be implemented in all subclasses.

        """

    def round(self, val, n=None):
        """Round the value to n digit after the decimal point.

        """
        if (n is not None) and isinstance(val, float):
            val = round(val, n)
        return val


class SensorMnt(SensorBase):
    """Sensor associates to a monitoring histogram recording values
    as a function of time, namely profile histogram.

    """
    def __init__(self,
                 svc,
                 name,
                 histo=None,
                 run=None,
                 histopaths=[],
                 unit=MINUTE):
        """Several constructor are available

            SensorMnt(svc, name, histopaths=['task1/directory1/title1',
                                             'task2/directory2/title2',
                                             'task3/title3'])

            SensorMnt(svc, name, histo=h1, run=1234)

        The syntax of histopath is task/directory/title or task/title
        The task is the name of the monitoring task producing the histograms.

        The keywork unit give the time unit of the underlying histogram.
        It is use when converting the time of profile histogram
        into and absolute datetime.

        """

        # scale factor to be compliant with time unit
        if unit not in (SECOND, MINUTE, HOUR):
            raise Exception('Invalid time unit.')

        SensorBase.__init__(self, svc, name)

        self.h = None
        self.scale = unit
        self.run = None
        self.y = []

        if histo and run:
            self.h = histo
            self.run = run
            self.y = self._get_y_values()
            return

        self.tasks = []
        self.hdirs = []
        self.htitles = []

        if histopaths:
            for histopath in histopaths:
                li = histopath.split(os.sep)
                if len(li) == 3:
                    self.tasks.append(li[0])
                    self.hdirs.append(li[1])
                    self.htitles.append(li[2])

                elif len(li) == 2:
                    self.tasks.append(li[0])
                    self.hdirs.append(None)
                    self.htitles.append(li[1])

                else:
                    raise Exception("Can't decode path %s" % histopath)

    def _get_y_values(self):
        """Build the list of y values recorded during the run.
        Return a list.

        """
        if not self.h:
            return []

        li = []
        for i in range(1, self.h.GetNbinsX() + 1):
            li.append(self.h.GetBinContent(i))

        return li

    def _path_byrun(self, ipath, run):
        """build the path of the saveset using the ByRun approach.
        Return a string or None if the file does not exist.

        """
        irun = int(run)
        path = PATH_BYRUN % (self.tasks[ipath],
                             irun / 10000,
                             irun / 1000,
                             self.tasks[ipath],
                             irun)

        if not os.path.exists(path):
            return None

        return path

    def _path_eor(self, ipath, run):
        """build the path of the saveset using the EOR approach.
        Return a string or None if the file does not exist.

        """
        self.svc.load_run(run)
        ds_run = self.svc.run_stat['starttime']

        path = DIR_EOR % (ds_run.year,
                          self.tasks[ipath],
                          ds_run.month,
                          ds_run.day)

        if not os.path.exists(path):
            return None

        # look for the file
        li = []
        for el in os.listdir(path):
            if 'EOR' in el and str(run) in el:
                li.append(el)

        if not li:
            return None

        if li and len(li) > 1:
            print 'Many EOR savesets found:'
            pprint(li)
            print 'Select the first one.'

        return os.path.join(path, li[0])

    def ib_at(self, y, diff=0.01):
        """Return the bin number for the bin with a content equal to y.
        The bin number start at one.
        Return None if the histogram does not exist.
        Return None if the y value is not found.

        """
        if not self.h:
            return None

        for i in range(len(self.y)):
            if abs(self.y[i] - y) <= diff:
                return i + 1

        return None

    def get_bin_content(self, ibin, n=None, nz=False):
        """Return the y value for the bin ibin. Bin numbering start at 1.

        The value is rounded to n digit if n is specified.
        Return None if the histogram does not exist.

        Return None when the value equal 0 and when nz is True

        """
        if not self.h:
            return None

        val = self.h.GetBinContent(ibin)

        if not val and nz:
            return None

        return self.round(val, n)

    def get_mean(self, n=None, nz=False):
        """Return the mean value.

        The value is round to n if n is specified .
        Return None if the histogram does not exist.

        Return None when the value equal 0 and when nz is True

        """
        if not self.h:
            return None

        val = self.h.GetMean()

        if not val and nz:
            return None

        return self.round(val, n)

    def prtdbg(self):
        """Print debug information when the debug flag is set to true.

        """
        if self.debug:
            print '-' * 80

            print self.name
            for i in range(len(self.y)):
                print ' ' * 4, '% 4i' % (i + 1), self.y[i]

            print '-' * 80

    def read(self, **kwargs):
        """Read the sensor for a given run.
        Return the sensor itself.

        """
        (run,) = self.get_kwargs('run', **kwargs)

        # reset
        self.h = None
        self.y = []
        self.run = run

        # scan the different histogram paths
        for ipath in range(len(self.tasks)):

            # try the EOR approach then the ByRun
            path = self._path_eor(ipath, run)
            if not path:
                path = self._path_byrun(ipath, run)
                if not path:
                    continue

            # print for debug
            if self.debug:
                print path

            # read the root file
            tfile = self.svc.load_tfile(path)

            # extract the histogram and fill the y attribute
            if tfile:
                if self.hdirs[ipath]:
                    rdir = tfile.GetDirectory(self.hdirs[ipath])
                    if rdir:
                        self.h = rdir.Get(self.htitles[ipath])
                else:
                    self.h = ROOT.gDirectory.Get(self.htitles[ipath])

                if self.h:
                    self.y = self._get_y_values()
                    break

        self.prtdbg()
        return self

    def x_at(self, y, diff=0.01):
        """Return the x value for the bin with a content equal to y.
        Return None if the histogram does not exist or if the value
        is not found.

        """
        if not self.h:
            return None

        for i in range(len(self.y)):
            if abs(self.y[i] - y) <= diff:

                ib = i + 1
                xc = self.h.GetXaxis().GetBinCenter(ib)

                self.svc.load_run(self.run)
                x = self.svc.run_stat['starttime'] + \
                    timedelta(seconds=xc * self.scale)
                return x

        return None

    def x_edge(self, ib):
        """Determine the lower and upper edge of the bin ib.
        Return a tuple with datetime

        """
        x1 = self.h.GetBinLowEdge(ib)
        x2 = x1 + self.h.GetBinWidth(ib)

        self.svc.load_run(self.run)
        x1 = self.svc.run_stat['starttime'] + timedelta(seconds=x1 * self.scale)
        x2 = self.svc.run_stat['starttime'] + timedelta(seconds=x2 * self.scale)

        return (x1, x2)

    def y_at(self, x, n=None):
        """Return the y value for the bin containing x.
        The value is round to n if n is specified .
        Return None if the histogram does not exist.

        """
        if not self.h:
            return None

        i = self.h.GetXaxis().FindBin(x)
        val = self.h.GetBinContent(i)

        return self.round(val, n)

    def y_max(self, n=None, ni=False, nz=False):
        """Return the maximum value of y.

        The value is round to n if n is specified .
        Return None if the histogram does not exist.

        When ni is True the maximum can not be an isolated value.
        In other word it can't be a peak value surrounded by zero.

        Return None when the maximum is equal to 0 and when nz is True.

        """
        if not self.h:
            return None

        val = max(self.y)

        if not ni:
            val = max(self.y)

        else:
            li = self.y
            val = li[0]
            for i in range(1, len(li)):
                if li[i] > val:
                    # condition to check that the i-value is not isolated
                    # i.e surrounded by zero value
                    ok = (i == len(li) - 1 and li[i - 1]) or \
                         (li[i - 1] and li[i + 1])
                    if ok:
                        val = li[i]

        if not val and nz:
            return None

        return self.round(val, n)

    def y_mean(self, n=None, excludeZero=False, nz=False):
        """Average the y value between the start and the end of the run.
        The start of the run is assume to be in the first bin.

        The value is round to n if n is specified .
        Return None if the histogram does not exist.

        Exclude value equal to 0 when excludeZero is True.

        Return None when the minimum is equal to 0 and nz is True.

        """
        if not self.h:
            return None

        if excludeZero:
            li = [y for y in self.y if y]
        else:
            li = self.y

        if len(li):
            val = sum(li) / float(len(li))
        else:
            val = None

        return self.round(val, n)

    def y_median(self, n=None, excludeZero=False, nz=False):
        """Compute the median of the y values recorded during the run.
        This method is robust against outliers which is not the case
        of the mean.

        The value is round to n if n is specified .
        Return None if the histogram does not exist.

        Exclude value equal to 0 when excludeZero is True.

        Return None when the minimum is equal to 0 and nz is True.

        """
        if not self.h:
            return None

        if excludeZero:
            li = [y for y in self.y if y]
        else:
            li = self.y

        val = median(li)

        if not val and nz:
            return None

        return self.round(val, n)

    def y_min(self, n=None, excludeZero=False, nz=False):
        """Return the minimum value of y.

        The value is round to n if n is specified .
        Return None if the histogram does not exist.

        Exclude value equal to 0 when excludeZero is True.

        Return None when the minimum is equal to 0 and nz is True.

        """
        if not self.h:
            return None

        if excludeZero:
            li = [y for y in self.y if y]
        else:
            li = self.y

        if not li:
            return None

        val = min(li)

        if not val and nz:
            return None

        return self.round(val, n)


class SensorMix(SensorBase):
    """A collection of sensors

    Read the first sensor in the list. If it is empty go to the next.
    and so on so forth.

    """
    def __init__(self, svc, name, *args):

        SensorBase.__init__(self, svc, name)
        self.sensors = args

    def read(self, *args, **kwargs):
        """Read the sensor in the list one after the others
        and stop when the sensor values are not empty.

        Return the sensor itself

        """

        for sensor in self.sensors:
            s1 = sensor.read(*args, **kwargs)
            if not s1.is_empty():
                return s1

        return self.sensors[-1]


class SensorPvss(SensorBase):
    """Sensor associates to a PVSS data point.

    Args:
        svc (SensorSvc):
        name (str): name of the sensor
        datapoint (str): PVSS data point
        positive (bool): keep only positive value when true
        scale (float): scale factor applied to the found values
        start (datetime): define the beginning of the validity period
        end (datetime): define the ending of the validity period

    """
    def __init__(self,
                 svc,
                 name,
                 datapoint=None,
                 positive=False,
                 scale=1.,
                 start=None,
                 end=None):

        SensorBase.__init__(self, svc, name)

        self.dp = datapoint
        self.positive = positive  # Keep positive value only
        self.scale = scale  # apply a scale factor on the PVSS values
        self.start = start  # date defining the beginning of the range
        self.end = end  # date defining the end of the validity range
        self.x = []
        self.y = []

    def is_valid_range(self, ds, de):
        """Check that the range define by the date ds and de is in
        the validity range of the sensors. The later is defined by the date
        start and end.

        Both date ds and de have to be in the validity range.

        """
        if self.start is None and self.end is None:
            return True

        elif self.start and self.end is None:
            return ds >= self.start

        elif self.start is None and self.end:
            return de <= self.end

        else:
            return ds >= self.start and de <= self.end

    def prtdbg(self, debug_flag=False):
        """Print debug information when the debug flag is set to true.

        """
        if self.debug or debug_flag:
            print '-' * 80

            print self.name
            for i in range(len(self.x)):
                print ' ' * 4, self.x[i].isoformat(), self.y[i]

            print '-' * 80

    def read(self, **kwargs):
        """Read the sensor between the datetime ds and de.
        Check that date are in the validity range.

        Return the sensor itself.

        """
        (ds, de) = self.get_kwargs('ds', 'de', **kwargs)

        self.x = []
        self.y = []

        if not self.is_valid_range(ds, de):
            self.prtdbg()
            return self

        csvfn = "pvss_%s_%s_%s.csv" % (self.name,
                                       ds.strftime('%d-%m-%YT%H:%M:%S'),
                                       de.strftime('%d-%m-%YT%H:%M:%S'))

        # remove the previous csv file
        fn = os.path.join(os.getcwd(), csvfn)
        if os.path.exists(fn):
            os.remove(fn)

        # build the PVSSExport command
        cmd = ['/group/online/ecs/util/PVSSExport_noPVSS/PVSSExport.sh',
               '-o', '"%s"' % fn,
               '-s', '"%s"' % ds.strftime('%d-%m-%Y %H:%M:%S'),
               '-e', '"%s"' % de.strftime('%d-%m-%Y %H:%M:%S'),
               '-d', '"%s"' % self.dp]

        if self.debug:
            print self.dp
            print str(ds)
            print str(de)
            print ' '.join(cmd)

        # execute the script PVSSExport
        # from time to time the PVSSExport script is stuck
        # try n times....
        fail = True
        for i in range(1):
            errors = []
            execute(cmd, TIMEOUT, errors)
            if not errors:
                fail = False
                break
            print 'Time out on:', self.dp
            time.sleep(WAITTIME)

        # no file when there is no data
        if fail or not os.path.exists(fn):
            return self

        # At this stage the process finish properly
        # retrieve the CSV data skipping the first rows
        with open(fn, 'rb') as fi:

            reader = csv.reader(fi)
            for row in reader:

                if reader.line_num == 1:
                    continue

                # protection against non well form row
                if len(row) < 2:
                    continue

                y = float(row[1])

                if (self.positive and y > 0) or not self.positive:
                    y = y * self.scale
                    self.x.append(string_to_date(row[0], '%d-%m-%Y %H:%M:%S'))
                    self.y.append(y)

            os.remove(fn)

        self.prtdbg()
        return self

    def to_profile(self, binwidth=60., ds=None, de=None):
        """Convert the sensors data into a TProfile histograms.
        Return  a ROOT.TProfile.

        The lower an upper edge of the histogram are defined by the
        starting and ending date.

        The unit of the time axis is second starting at 0
        The unit of the binwidth is also in second.

        Return None if the data point does not exist or if y is not found.


        """
        if not self.y:
            return None

        if ds and de:
            nbin = round((de - ds).seconds / binwidth)
        else:
            nbin = round((self.x[-1] - self.x[0]).seconds / binwidth)

        nbin = int(nbin)
        xmax = nbin * binwidth

        hn = "profile_%s" % self.name
        h1 = ROOT.TProfile(hn, hn, nbin, 0., xmax)

        if ds and de:
            for i in range(len(self.x)):
                if ds <= self.x[i] < de:
                    dt = self.x[i] - ds
                    h1.Fill(dt.seconds, self.y[i])

        else:
            for i in range(len(self.x)):
                dt = self.x[i] - self.x[0]
                h1.Fill(dt.seconds, self.y[i])

        return h1

    def x_at(self, y, n=None):
        """Return the x value at y.
        Return None if the data point does not exist or if y is not found.

        """
        if not self.y:
            return None

        try:
            i = self.y.index(y)
            val = self.x[i]

        except:
            val = None

        return self.round(val, n)

    def y_at(self, x, n=None):
        """Return the y value for the bin containing x.
        The value is round to n if n is specified .
        Return None if the data point does not exist or if x is not found.

        """
        if not self.y:
            return None

        try:
            i = self.x.index(x)
            val = self.y[i]

        except:
            val = None

        return self.round(val, n)

    def y_between(self, ds, de, excludeZero=False):
        """Build the list of y values when x is between ds and de.
        The arguments ds and de are datetime.
        Return a list or None

        Exclude value equal to 0 in the y list when excludeZero is True.

        """
        if not self.y:
            return None

        li = []
        for i in range(len(self.x)):
            if ds <= self.x[i] < de:
                li.append(self.y[i])

        if excludeZero:
            li = [y for y in li if y]

        return li

    def y_max(self, n=None, ni=False, nz=False, ds=None, de=None):
        """Return the maximum value of y.

        The value is round to n if n is specified .
        Return None if the data point does not exist.

        Return None if the max is 0 when nz is True.

        When ni is True the maximum can not be an isolated value.
        In other word it can't be a peak value surrounded by zero.

        Compute the maximum between the two dates ds and de when specified.

        """

        if not self.y:
            return None

        li = self.y

        if ds and de:
            li = self.y_between(ds, de)

        if not li:
            return None

        if not ni:
            val = max(li)

        else:
            val = li[0]
            for i in range(1, len(li)):
                if li[i] > val:
                    # condition to check that the i-value is not isolated
                    # i.e surrounded by zero value
                    ok = (i == len(li) - 1 and li[i - 1]) or \
                         (li[i - 1] and li[i + 1])
                    if ok:
                        val = li[i]

        if not val and nz:
            return None

        return self.round(val, n)

    def y_mean(self, n=None, excludeZero=False, nz=False, ds=None, de=None):
        """Average the y value.

        The value is round to n if n is specified .
        Return None if the data point does not exist.

        Exclude value equal to 0 in the y list when excludeZero is True.

        Return None when the mean is equal to 0 and nz is True.

        Compute the minimum between the two dates ds and de when specified.

        """
        if not self.y:
            return None

        li = self.y

        if ds and de:
            li = self.y_between(ds, de, excludeZero)

        if not li:
            return None

        val = sum(li, 0) / len(li)

        if not val and nz:
            return None

        return self.round(val, n)

    def y_median(self, n=None, excludeZero=False, nz=False, ds=None, de=None):
        """Compute the median of the y values recorded during the run.
        This method is robust against outliers which is not the case
        of the mean.

        The value is round to n if n is specified .
        Return None if the histogram does not exist.

        Exclude value equal to 0 in the y list when excludeZero is True.

        Return None when the minimum is equal to 0 and nz is True.

        Compute the median between the two dates ds and de when specified.

        """
        if not self.y:
            return None

        li = self.y

        if ds and de:
            li = self.y_between(ds, de, excludeZero)

        if not li:
            return None

        val = median(li)

        if not val and nz:
            return None

        return self.round(val, n)

    def y_min(self, n=None, excludeZero=False, nz=False, ds=None, de=None):
        """Return the minimum value of y.

        The value is round to n if n is specified .
        Return None if the data point does not exist.

        Exclude value equal to 0 in the y list when excludeZero is True.
        Return None if it is the only choice.

        Return None when the mean is equal to 0 and nz is True.

        Compute the minimum between the two dates ds and de when specified.

        """
        if not self.y:
            return None

        li = self.y

        if ds and de:
            li = self.y_between(ds, de, excludeZero)

        if not li:
            return None

        val = min(li)

        if not val and nz:
            return None

        return self.round(val, n)


class SensorRdbF(SensorBase):
    """Sensor associates to a fill in the run database.

    Args:
        svc (SensorSvc):
        name (str): name of the sensor
        key (str): key in the fill_stat dictionary of the run database.

    """
    def __init__(self, svc, name, key=None):

        SensorBase.__init__(self, svc, name)
        self._k = key  # the key in the run database directory
        self.y = None

    def read(self, **kwargs):

        (fill,) = self.get_kwargs('fill', **kwargs)
        self.svc.load_fill(fill)

        if self._k in self.svc.fill_stat:
            self.y = self.svc.fill_stat[self._k]
        else:
            self.y = None

        self.prtdbg()
        return self

    def value(self, n=None, cvt=None, nz=False):
        """Return the value of the sensor.

        The value is round to n if n is specified .
        The function cvt is applied to the value if specified.

        Return None when the value is equal to 0 and nz is True.

        """
        val = self.y

        if nz and (not val or (isinstance(val, str) and val == '0')):
            return None

        if cvt:
            val = cvt(val)

        return self.round(val, n)


class SensorRdbR(SensorBase):
    """Sensor associates to 'run stat key' in the run database.

    Args:
        svc (SensorSvc):
        name (str): name of the sensor
        key (str): key in the run_stat dictionary of the run database.
        dic (dict): reference to a run_data dictionary for a stream.

    """
    def __init__(self, svc, name, key=None, dic=None):

        SensorBase.__init__(self, svc, name)

        self._d = svc.run_stat
        self._k = key
        self.y = None

        if dic is not None:
            self._d = dic  # runb stream dictionary

    def rate(self, n=None):
        """Transform a number of event in rate.
        It divides the value by the run duration.

        Args:
            n (int): the return value is round to n digit
                after the decimal point.

        Returns:
            float: the rate in Hz

        """
        val = self.y
        if not isinstance(val, (float, int, long)):
            return 0.

        rs = self.svc.run_stat
        delta = rs["endtime"] - rs["starttime"]

        val = val / delta.total_seconds()

        if n is not None:
            val = round(val, n)

        return val

    def read(self, **kwargs):

        (run,) = self.get_kwargs('run', **kwargs)

        self.svc.load_run(run)

        if self._k in self._d:
            self.y = self._d[self._k]
        else:
            self.y = None

        self.prtdbg()
        return self

    def value(self, n=None, cvt=None, nz=False):
        """Return the value of the sensor.

        The value is round to n if n is specified .
        The function cvt is applied to the value if specified.

        Return None when the value is equal to 0 and nz is True.

        """
        val = self.y

        if nz and (not val or (isinstance(val, str) and val == '0')):
            return None

        if cvt:
            val = cvt(val)

        return self.round(val, n)


class SensorTrend(SensorPvss):
    """Sensor associates to a TrendingHistogram.

    """
    def __init__(self, svc, name, file=None, tag=None, positive=False):

        SensorBase.__init__(self, svc, name)

        self.file = file
        self.tag = tag
        self.positive = positive  # Keep positive value only
        self.x = []
        self.y = []

    def read(self, **kwargs):
        """Read the sensor between the datetime ds and de.
        Return the sensor itself.

        """
        (ds, de) = self.get_kwargs('ds', 'de', **kwargs)

        self.x = []
        self.y = []

        # temporarily file to collect the data
        fn = "trd_%s_%s_%s.txt" % (self.tag, ds.isoformat(), de.isoformat())
        fn = fn.replace('->', 'to').replace(" ", "_")
        if os.path.exists(fn):
            os.remove(fn)

        # setup the bash script to interrogate the trending file
        cmd = "ExtractFromTrendingFile.exe %s '%s' %s %s > %s"

        cmd = cmd % (self.file,
                     self.tag,
                     ds.isoformat(),
                     de.isoformat(),
                     fn)

        # execute the script and decode data
        call(cmd, shell=True)

        if not os.path.exists(fn):
            print "No trend data", self.tag, ds.isoformat(), de.isoformat()
            os.remove(fn)
            return self

        fi = open(fn)
        for line in fi:

            if line.startswith("***"):
                print "No trend sensor", self.tag
                break

            if line.find("ERROR") != -1:
                print line.strip('\n')
                break

            k, v = line.strip('\n').split()
            self.x.append(string_to_date(k, fmt='%Y-%m-%dT%H:%M:%S'))
            self.y.append(float(v))

        fi.close()
        os.remove(fn)

        self.prtdbg()
        return self


def execute(cmd, timeout, errors=[]):
    """Helper function to execute the command cmd
    To avoid to be stucked for ever run a watchdog

    """
    with TemporaryFile() as fi:

        p = Popen(' '.join(cmd), bufsize=-1, stdout=fi, shell=True)

        def kill_me():
            p.kill()
            errors.append('timeout')

        timer = Timer(timeout, kill_me)
        timer.start()
        p.wait()
        timer.cancel()


def string_to_date(s, fmt='%Y-%m-%d %H:%M:%S'):
    """Helper function to convert a string as a date.
    Remove systematically '.000' at the end of the string.

    """
    if not s:
        return None

    if '.' in s:
        s = s[:s.index('.')]

    return datetime.strptime(s, fmt)


def string_to_number(data):
    """Scan the dictionary value to convert string into float or int.

    Args:
        data (dict):

    """
    for k, v in data.iteritems():
        if not isinstance(v, (unicode, str)):
            continue

        if IS_FLOAT.match(v):
            data[k] = float(v)

        elif IS_FLOAT.match(v):
            data[k] = int(v)
