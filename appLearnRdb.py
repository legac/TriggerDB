#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" NAME
        appLearnRdb -- script to learn the JSON API of the run database

    SYNOPSIS
        appLearnRdb [Options]

    DESCRIPTION

    OPTIONS
            --version
                Return the version number of this scripts

    EXAMPLES

        > ./appLearnRdb.py -h

    AUTHOR
        R. Le Gac, legac@cppm.in2p3.fr

"""
import json
import urllib
import os
import sys

from pprint import pprint

URL_FILL = "http://lbrundb.cern.ch/api/fill/%s"
URL_PHYSICSFILLS = \
"http://lbrundb.cern.ch/api/fills/physics/?starttime=%sT00:00:00&endtime=%sT00:00:00"
URL_RUN = "http://lbrundb.cern.ch/api/run/%s"
URL_RUNDATA = "http://lbrundb.cern.ch/api/rundata/%s"
URL_RUNSINFILL = "http://lbrundb.cern.ch/api/runs_in_fill/%s"


def json2dict(url):
    """Convert the JSON string returns by the URL to a dictionary.

    Args:
        url (str):

    Returns:
        dict:

    """
    fi = urllib.urlopen(url)
    return json.load(fi)


def print_options(args):
    """
    Args:
        args (argparse;Namespace)

    """
    di = vars(args)
    for key in di:
        print "%20s: %s" % (key, di[key])


if __name__ == "__main__":

    from argparse import ArgumentParser

    AGP = ArgumentParser()

    AGP.add_argument(
        "-a", "--api",
        action="store_true",
        help="print method / function available from the API [%(default)s].")

    AGP.add_argument(
        "-f", "--fill",
        help="print information for a given fill.",
        metavar="<number>",
        type=int)

    AGP.add_argument(
        "-p", "--physics-fills",
        nargs=2,
        help="print the list of physics fills for the period define by a "
             "start and an end date, encoded as YYYY-MM-DD.",
        metavar="<date>")

    AGP.add_argument(
        "-r", "--run",
        help="print information for a given run.",
        metavar="<number>",
        type=int)

    ARGS = AGP.parse_args()

    print "script parameters:"
    print_options(ARGS)

    if ARGS.api:
        print "\nJSON API documentation:",
        print "https://lbdokuwiki.cern.ch/rundb:rundb_web_interface_api\n"

    if ARGS.fill:
        fill = ARGS.fill
        print "\nFILL information:"
        pprint(json2dict(URL_FILL % fill), indent=8)

        print "\nRUN belonging to fill", fill
        li = [di["runid"] for di in json2dict(URL_RUNSINFILL % fill)]
        pprint(li, indent=8)

    if ARGS.physics_fills:
        dates = tuple(ARGS.physics_fills)
        print "\nPHYSICS FILLS between %s and %s" % dates
        obj = json2dict(URL_PHYSICSFILLS % dates)
        li = [di["fill_id"] for di in obj["fills"]]
        pprint(li, indent=8)

    if ARGS.run:
        run = ARGS.run
        print "\nRUN information:"
        pprint(json2dict(URL_RUN % run), indent=8)

        print "\nSTREAM belongin to run", run
        obj = json2dict(URL_RUNDATA % run)
        pprint(obj["stream_data"], indent=8)

        sys.exit(0)