#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    NAME
        appRecover -- recover sensors values for a series of runs

    SYNOPSIS
        appRecover [Options]

    DESCRIPTION
        Recover sensors values for a series of runs.
        One script per issue.

    OPTIONS


    EXAMPLES

        > SetupGaudi
        > ./appRecover --db foo.sqlite

    AUTHOR
        R. Le Gac, legac@cppm.in2p3.fr

"""

__author__ = "legac@cppm.in2p3.fr"


import os
import sys

from appFillDB import (create_sensors,
                       insert_measures_days,
                       insert_or_update)
from base import (create_db,
                  create_sql_functions,
                  log)
from datetime import datetime, timedelta
from optparse import OptionParser
from pprint import pprint
from ROOT import *
from sensors import (SensorSvc,
                     SensorMix,
                     SensorMnt,
                     SensorPvss,
                     SensorRdbR,
                     SensorTrend)


def ratio(x, y):
    if x == None or y == None:
        return None

    if x and y:
        return (x - y) / x

    return None

#===============================================================================
#
# 	Command line options
#
#===============================================================================
parser = OptionParser()

parser.add_option("--db",
                  dest="dbname",
                  help="File name for the sqlite database [%default]."
                       "The special name ':memory:' create the database in RAM.")

parser.add_option("-d", "--debug",
                  action="store_true",
                  dest="debug",
                  help="Activate the debug mode [%default].")

parser.add_option("-y", "--year",
                  action="store",
                  dest="year",
                  help="Select the year [%default].",
                  metavar="YYYY",
                  type="int")

parser.set_defaults(dbname='triggerdb.sqlite',
                    debug=False,
                    year=datetime.now().year)

(opt, args) = parser.parse_args()

log("Start appRecover.py %s" % str(opt))

#===============================================================================
#
# 	Database and sensors
#
#===============================================================================
db = create_db(opt.dbname)
create_sql_functions(db)
sensorSvc = create_sensors(debug=opt.debug)

#===============================================================================
#
# 	Process a list of runs belonging to a fill
#
#===============================================================================
sql = """select fill_number,
                measures_runs.id as run_id,
                measures_fills.id as fill_id,
                run_number,
                measures_runs.start_time as start_time,
                end_time,
                run_duration
         from   measures_runs, measures_fills, axis_times
         where  measures_runs.id_measures_fills = measures_fills.id
         and    measures_runs.id_axis_times = axis_times.id
         and    year = ?
         order by run_number"""

fill_ref = None
for row in db.execute(sql, (opt.year,)):
# for row in db.execute(sql):

    fill_number = row['fill_number']

    if fill_number != fill_ref:

        print "Get PVSS data for fill", fill_number

        fill_ref = fill_number
        duration = sensorSvc.get('time_total', fill=fill_number).value()
        ds_fill = sensorSvc.get('fillStartTime', fill=fill_number).value()
        de_fill = ds_fill + timedelta(seconds=duration)

        sHltPhysRate = sensorSvc.get('HLTPhysRate', ds=ds_fill, de=de_fill)
        sTotalHltPhysRate = sensorSvc.get('TotalHLTPhysRate', ds=ds_fill, de=de_fill)
        sTotalHltRate = sensorSvc.get('TotalHLTRate', ds=ds_fill, de=de_fill)
        sHltDeferred = sensorSvc.get('HLTDeferred', ds=ds_fill, de=de_fill)
        sPhysDeadTime = sensorSvc.get('PhysDeadTime', ds=ds_fill, de=de_fill)

        mdnfill_total_hlt_physics_rate = sTotalHltPhysRate.y_median(n=2, excludeZero=True, nz=True)
        mdnfill_total_hlt_rate = sTotalHltRate.y_median(n=2, excludeZero=True, nz=True)
        mdnfill_hlt_deferred = sHltDeferred.y_median(n=2)
        mdnfill_physics_dead_time = sPhysDeadTime.y_median(n=2)

        d = dict(id=row["fill_id"],
                 mdnfill_total_hlt_physics_rate=mdnfill_total_hlt_physics_rate,
                 mdnfill_total_hlt_rate=mdnfill_total_hlt_rate,
                 mdnfill_hlt_deferred=mdnfill_hlt_deferred,
                 mdnfill_physics_dead_time=mdnfill_physics_dead_time)

        print ("FILL", fill_number,
               mdnfill_total_hlt_physics_rate,
               mdnfill_total_hlt_rate,
               mdnfill_hlt_deferred,
               mdnfill_physics_dead_time)

        insert_or_update(db, 'measures_fills', **d)

    run = row['run_number']

    ds_run = sensorSvc.get('startTime', run=run).value()
    de_run = sensorSvc.get('endTime', run=run).value()

    # deferred
    mdnrun_hlt_deferred = sHltDeferred.y_median(n=2, ds=ds_run, de=de_run)

    scale = 1.
    if mdnrun_hlt_deferred:
        if mdnrun_hlt_deferred < 100.:
            scale = 1. / (1. - (mdnrun_hlt_deferred / 100.))
        else:
            scale = 0.

    # total hlt physics rate
    s1 = sensorSvc.get('RoutingBit77', ds=ds_run, de=de_run)
    mdnrun_hlt_physics_rate = s1.y_median(n=2, excludeZero=True, nz=True)
    mdnrun_total_hlt_physics_rate = sTotalHltPhysRate.y_median(n=2, nz=True, ds=ds_run, de=de_run)

    if (not mdnrun_total_hlt_physics_rate) and mdnrun_hlt_physics_rate:
        mdnrun_total_hlt_physics_rate = round(mdnrun_hlt_physics_rate * scale, 2)

    # total hlt rate
    s1 = sensorSvc.get('RoutingBit64', ds=ds_run, de=de_run)
    mdnrun_hlt_rate = s1.y_median(n=2, excludeZero=True, nz=True)

    mdnrun_total_hlt_rate = sTotalHltRate.y_median(n=2, nz=True, ds=ds_run, de=de_run)

    if (not mdnrun_total_hlt_rate) and mdnrun_hlt_rate:
        mdnrun_total_hlt_rate = round(mdnrun_hlt_rate * scale, 2)

    # physics dead time
    mdnrun_physics_dead_time = sPhysDeadTime.y_median(n=2, nz=True, ds=ds_run, de=de_run)

    d = dict(id=row['run_id'],
           mdnrun_total_hlt_physics_rate=mdnrun_total_hlt_physics_rate,
           mdnrun_total_hlt_rate=mdnrun_total_hlt_rate,
           mdnrun_physics_dead_time=mdnrun_physics_dead_time,
           mdnrun_hlt_deferred=mdnrun_hlt_deferred)

    print ("RUN", run,
           mdnrun_total_hlt_physics_rate,
           mdnrun_total_hlt_rate,
           mdnrun_physics_dead_time,
           mdnrun_hlt_deferred)

    insert_or_update(db, 'measures_runs', **d)

#===============================================================================
#
# 	Exit
#
#===============================================================================
log("End appRecover.py")
sys.exit(0)
