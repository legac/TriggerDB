#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" NAME
        appPDF -- generate report as a pdf file

    SYNOPSIS
        appPDF [Options]

    DESCRIPTION
        generate a summary report as a pdf file

    OPTIONS
        --version
            Return the version number of this scripts

    EXAMPLES

        > lb-run MooreOnline/latest ./appPDF.py -h

    AUTHOR
        R. Le Gac, legac@cppm.in2p3.fr

"""
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import os
import rdbjson
import shutil
import sys


from appFillDB import create_sensors
from base import (create_db,
                  create_sql_functions,
                  get_beam_energy,
                  is_inhibit,
                  log,
                  print_options)
from datetime import datetime, timedelta
from math import exp, sqrt
from matplotlib.backends.backend_pdf import PdfPages
from matplotlib.ticker import IndexLocator, FixedFormatter
from matplotlib.transforms import Bbox, blended_transform_factory


HZ = 1.
KHZ = 0.001
NOW = datetime.now()

MONTHS = {
    "01": "Jan",
    "02": "Feb",
    "03": "Mar",
    "04": "Apr",
    "05": "May",
    "06": "Jun",
    "07": "Jul",
    "08": "Aug",
    "09": "Sep",
    "10": "Oct",
    "11": "Nov",
    "12": "Dec"}

CHART1 = (0, 69/255., 134/255.)
CHART2 = (255/255., 66/255., 14/255.)
CHART3 = (255/255., 211/255., 32/255.)
CHART4 = (174/255., 207/255., 0/255.)
CYANM8 = (153/255., 204/255., 204/255.)

BBOX_REF = dict(boxstyle="larrow",
                edgecolor="lightsteelblue",
                facecolor="none",
                linewidth=0.5)

DISK_USAGE_PNG = "disk_usage%s.png"
OUTPUT_STREAMS_PNG = "output_streams%s.png"


class LumiVsTime(object):
    """Contain delivered and recorded luminosity as a function of time (month,
    week, day or fill).

    Args:
        db (sqlite3.connection): database connection.
        time_unit (str): possible values are month, week, day and fill.

    Attributes:
        df (pandas.DataFrame):
        unit (str): time unit

    """
    def __init__(self, db, time_unit):

        if time_unit == "day":
            sql = """SELECT day AS time,
                        delivered_lumi*pb() AS 'delivered luminosity',
                        recorded_lumi*pb() AS 'recorded luminosity'
                 FROM   view_days
                 ORDER BY day """

        elif time_unit == "fill":
            sql = """SELECT fill_number AS time,
                            lumi_total*pb() AS 'delivered luminosity',
                            lumi_logged*pb() AS 'recorded luminosity'
                     FROM   view_fills
                     ORDER BY fill_number """

        elif time_unit == "month":
            sql = \
                """SELECT month AS time,
                          total(delivered_lumi)*pb() AS 'delivered luminosity',
                          total(recorded_lumi)*pb() AS 'recorded luminosity'
                   FROM   view_days
                   GROUP BY month
                   ORDER BY month """

        elif time_unit == "week":
           sql = """SELECT week AS time,
                        total(delivered_lumi)*pb() AS 'delivered luminosity',
                        total(recorded_lumi)*pb() AS 'recorded luminosity',
                        total(time_total)/604800.0 AS hubner
                 FROM   view_days
                 GROUP BY week
                 ORDER BY week """
        else:
            raise Exception("Unknown time unit.")

        df = pd.read_sql_query(sql, db)

        # compute efficiency
        df["efficiency"] = \
            (df["recorded luminosity"] / df["delivered luminosity"]) * 100

        self.df = df
        self.unit = time_unit

    def plot_cumulate(self, ax, **kwargs):
        """Plot delivered and recorded luminosity cumulate over time.

        Args:
            ax (matplotlib.Axes):

        Keyword arguments:
            those of the method DataFrame.plot
        """
        df = self.df
        unit = self.unit

        default = dict(color=[CHART1, CHART2],
                       grid=True,
                       kind="line",
                       linewidth=1.2,
                       ylim=[0., None])

        li = ["delivered luminosity", "recorded luminosity"]
        ylabel = "cumulative luminosity [pb$^{-1}$]"

        # single point !
        if df.shape[0] == 1:
            df = df.copy()
            df = pd.concat([df, df, df])
            df.index = [0, 1, 2]

            time = df.iloc[0]["time"]
            df["time"] = [time-0.5, time, time+0.5]

            df[li].plot(ax=ax, x=df["time"], **default)
            xticks_and_labels(ax,
                              df["time"],
                              xlabel=unit,
                              ylabel=ylabel,
                              base=1,
                              offset=-0.5)

        # many points
        else:
            df[li].cumsum().plot(ax=ax, x=df["time"], **default)
            ticks_and_labels(ax, unit, ylabel)

    def plot_efficiency(self, ax, value=False, **kwargs):
        """Plot the data taking efficiency versus time as a bar graph.

        Args:
            ax (matplotlib.Axes):
            value (bool): show value in bar

        Keyword arguments:
            those of the method DataFrame.plot

        """
        df = self.df
        unit = self.unit

        default = dict(color=CHART4,
                       x=df["time"],
                       grid=True,
                       kind="bar",
                       linewidth=0.4,
                       rot=0,
                       width=0.8)

        default.update(**kwargs)

        # protection -- delivered lumi = 0 while recorded > 0
        query = df["delivered luminosity"] > 0

        df[query]["efficiency"].plot(ax=ax, **default)

        xticks_and_labels(ax,
                          df["time"],
                          xlabel=unit,
                          ylabel="data taking efficiency [%]",
                          base=1,
                          offset=0.4)

        plot_xticks(ax)

        if value:
            plot_values(ax, df[query]["efficiency"])

    def plot_summary(self, fig):
        """Plot summary information.

        Args:
            fig (matplotlib.Figure):

        """
        df = self.df

        x = round(df["recorded luminosity"].sum(), 0)
        y = round(df["delivered luminosity"].sum(), 0)
        mx = round(df["recorded luminosity"].max(), 0)
        e = (round((x/y)*100, 0) if y > 0  else 0.)

        txt = "Delivered luminosity: %.0f pb$^{-1}$\n"\
              "Recorded luminosity: %.0f pb$^{-1}$\n\n"\
              "Global efficiency: %.0f %%\n\n"\
              "Best integrated luminosity per %s: %.0f pb$^{-1}$"

        txt = txt % (y, x, e, self.unit, mx)

        fig.text(0.57, 0.408,
                 txt,
                 fontsize=8,
                 verticalalignment="top",
                 bbox={"facecolor": "none", "linewidth":1., "pad":19})

        for y in (0.444, 0.855):
            fig.text(0.549, y,
                     "Fills for calibration not included",
                     fontsize=8,
                     style="italic")


    def plot_recorded(self, ax, value=False, **kwargs):
        """Plot the recorded luminosity versus time as a bar graph.

        Args:
            ax (matplotlib.Axes):
            value (bool): show value in bar

        Keyword arguments:
            those of the method DataFrame.plot

        """
        df = self.df
        unit = self.unit

        default = dict(color=CHART3,
                       grid=True,
                       kind="bar",
                       linewidth=0.4,
                       rot=0,
                       width=0.8)

        default.update(**kwargs)

        df["recorded luminosity"].plot(ax=ax, **default)

        ylabel = "recorded luminosity per %s [pb$^{-1}$]" % unit
        xticks_and_labels(ax,
                          df["time"],
                          xlabel=unit,
                          ylabel=ylabel,
                          base=1,
                          offset=0.4)

        plot_xticks(ax)

        if value:
            fmt = ("%.1f" if unit == "fill" else "%.0f")
            plot_values(ax, df["recorded luminosity"], fmt=fmt)


class RatesVsFill(object):
    """Contain rates as a function of fill for the l0, hlt1, hlt2 lines
    and streams.

    Args:
        db (sqlite3.connection): database connection.
        ref (Reference): reference values for rates.

    Attributes:
        df (pandas.DataFrame): contains rates as a function of gill.
        df4line (pandas.DataFRame): internal, require to plot line

    """
    def __init__(self, db, ref):

        df = self.collect_global(db)
        df1 = self.collect_l0(db)
        df2 = self.collect_hlt1(db)
        df3 = self.collect_hlt2(db)
        df4 = self.collect_streams(db)

        for el in (df1, df2, df3, df4):
            df = pd.merge(df, el, how="outer", on="fill")

        self.df = df
        self.ref = ref

        self.compute()

        # add two point at the start and at the beginning
        # require to have nice line plot

        di = dict.fromkeys(df.columns, [0])
        di["fill"] = [df.iloc[0]["fill"] - 0.5]
        df4 = pd.DataFrame(di)

        di["fill"] = [df.iloc[-1]["fill"] + 0.5]
        df5 = pd.DataFrame(di)

        df4line = pd.concat([df4, df, df5])
        df4line.index = xrange(len(df4line.index))

        self.df4line = df4line

    def collect_global(self, db):
        """Collect main rates for bb, l0, hlt1 and  hlT2.

        Args:
            db (sqlite3.connection): database connection.

        Returns:
            pandas.DataFrame:

        """
        sql = \
        """SELECT fill_number AS fill,
                  mdnfill_nobias_rate*kHz() AS 'no bias',
                  mdnfill_bb_rate*kHz() AS 'visible crossing',
                  mdnfill_l0_physics_rate*kHz() AS 'l0 physics',
                  median(mdnrun_rb_46, run_duration)*kHz() AS 'hlt1 physics'
           FROM   view_runs,
                  view_fills,
                  view_routingbits
           WHERE  view_runs.id_measures_fills = view_fills.id
           AND    view_routingbits.id_measures_fills = view_fills.id
           GROUP BY fill_number
           ORDER BY fill_number """

        df = pd.read_sql_query(sql, db)
        return df.fillna(0.)

    def collect_l0(self, db):
        """Collect rates for l0 lines as a function of fill.

        Args:
            db (sqlite3.connection): database connection.

        Returns:
            pandas.DataFrame:

        """
        sql = \
        """SELECT fill_number AS fill,
                  median(mdnrun_l0_diem_lowmult, run_duration)*kHz() AS 'l0 diem lowmult',
                  median(mdnrun_l0_dihadron_lowmult, run_duration)*kHz() AS 'l0 dihadron lowmult',
                  median(mdnrun_l0_dimuon, run_duration)*kHz() AS 'l0 dimuon',
                  median(mdnrun_l0_dimuon_lowmult, run_duration)*kHz() AS 'l0 dimuon lowmult',
                  median(mdnrun_l0_electron, run_duration)*kHz() AS 'l0 electron',
                  median(mdnrun_l0_electron_lowmult, run_duration)*kHz() AS 'l0 electron lowmult',
                  median(mdnrun_l0_hadron, run_duration)*kHz() AS 'l0 hadron',
                  median(mdnrun_l0_jetel, run_duration)*kHz() AS 'l0 jetel',
                  median(mdnrun_l0_jetph, run_duration)*kHz() AS 'l0 jetph',
                  median(mdnrun_l0_lowmult, run_duration)*kHz() AS 'l0 lowmult',
                  median(mdnrun_l0_muon, run_duration)*kHz() AS 'l0 muon',
                  median(mdnrun_l0_muon_lowmult, run_duration)*kHz() AS 'l0 muon lowmult',
                  median(mdnrun_l0_photon_lowmult, run_duration)*kHz() AS 'l0 photon lowmult',
                  median(mdnrun_l0_spdlowmult, run_duration)*kHz() AS 'l0 spd lowmult'
           FROM   view_runs,
                  view_fills,
                  view_hlt1_l0rates
           WHERE  view_runs.id_measures_fills = view_fills.id
           AND    view_hlt1_l0rates.id_measures_fills = view_fills.id
           GROUP BY fill_number
           ORDER BY fill_number """

        df = pd.read_sql_query(sql, db)
        return df.fillna(0.)

    def collect_hlt1(self, db):
        """Collect rates for hlt1 lines as a function of fill.

        Args:
            db (sqlite3.connection): database connection.

        Returns:
            pandas.DataFrame:

        """
        sql = \
        """SELECT fill_number AS fill,
                  median(mdnrun_hltrate_hlt1_mva, run_duration)*kHz() AS 'hlt1 mva',
                  median(mdnrun_hltrate_hlt1_singlemuon, run_duration)*kHz() AS 'hlt1 muon',
                  median(mdnrun_hltrate_hlt1_dimuon, run_duration)*kHz() AS 'hlt1 dimuon',
                  median(mdnrun_hltrate_hlt1_lowmult, run_duration)*kHz() AS 'hlt1 lowmult',
                  median(mdnrun_hltrate_hlt1_electron, run_duration)*kHz() AS 'hlt1 electron',
                  median(mdnrun_hltrate_hlt1_nobias, run_duration)*kHz() AS 'hlt1 nobias'
           FROM   view_hlt1_rates,
                  view_fills,
                  view_runs
           WHERE  view_hlt1_rates.id_measures_fills = view_fills.id
             AND  view_hlt1_rates.id_measures_runs = view_runs.id
           GROUP BY fill_number
           ORDER BY fill_number """

        df = pd.read_sql_query(sql, db)
        return df.fillna(0.)

    def collect_hlt2(self, db):
        """Collect rates for hlt1 lines as a function of fill.

        Args:
            db (sqlite3.connection): database connection.

        Returns:
            pandas.DataFrame:

        """
        sql = \
        """SELECT fill_number AS fill,
                  median(mdnrun_hlt2_charmhad, run_duration)*kHz() AS 'hlt2 charmhad.*',
                  median(mdnrun_hlt2_dimuon, run_duration)*kHz() AS 'hlt2 dimuon.*',
                  median(mdnrun_hlt2_ew, run_duration)*kHz() AS 'hlt2 ew.*',
                  median(mdnrun_hlt2_lowmult, run_duration)*kHz() AS 'hlt2 lowmult.*',
                  median(mdnrun_hlt2_singlemuon, run_duration)*kHz() AS 'hlt2 singlemuon.*',
                  median(mdnrun_hlt2_topo, run_duration)*kHz() AS 'hlt2 topo.*'
           FROM   view_hlt2_rates,
                  view_fills,
                  view_runs
           WHERE  view_hlt2_rates.id_measures_fills = view_fills.id
             AND  view_hlt2_rates.id_measures_runs = view_runs.id
           GROUP BY fill_number
           ORDER BY fill_number """

        df = pd.read_sql_query(sql, db)
        return df.fillna(0.)

    def collect_streams(self, db):
        """Collect rates for stream as a function of fill.

        Args:
            db (sqlite3.connection): database connection.

        Returns:
            pandas.DataFrame:

        """
        sql = \
        """SELECT fill_number AS fill,
                  median(mdnrun_beamgas_events, run_duration)*kHz() AS 'beamgas (event)',
                  median(mdnrun_beamgas_physstat, run_duration)*kHz() AS 'beamgas (physstat)',
                  median(mdnrun_calib_events, run_duration)*kHz() AS 'calib (event)',
                  median(mdnrun_calib_physstat, run_duration)*kHz() AS 'calib (physstat)',
                  median(mdnrun_express_events, run_duration)*kHz() AS 'express (event)',
                  median(mdnrun_express_physstat, run_duration)*kHz() AS 'express (physstat)',
                  median(mdnrun_full_events, run_duration)*kHz() AS 'full (event)',
                  median(mdnrun_full_physstat, run_duration)*kHz() AS 'full (physstat)',
                  median(mdnrun_lumi_events, run_duration)*kHz() AS 'lumi (event)',
                  median(mdnrun_lumi_physstat, run_duration)*kHz() AS 'lumi (physstat)',
                  median(mdnrun_turbo_events, run_duration)*kHz() AS 'turbo (event)',
                  median(mdnrun_turbo_physstat, run_duration)*kHz() AS 'turbo (physstat)',
                  median(mdnrun_turcal_events, run_duration)*kHz() AS 'turcal (event)',
                  median(mdnrun_turcal_physstat, run_duration)*kHz() AS 'turcal (physstat)'
           FROM   view_streams,
                  view_fills,
                  view_runs
           WHERE  view_streams.id_measures_fills = view_fills.id
             AND  view_streams.id_measures_runs = view_runs.id
           GROUP BY fill_number
           ORDER BY fill_number """

        df = pd.read_sql_query(sql, db)
        return df.fillna(0.)

    def compute(self):
        """Compute additional rates.

        """
        df = self.df
        ref = self.ref

        # delta with respect to references
        li = ["l0 dihadron lowmult",
              "l0 lowmult",
              "l0 physics",
              "hlt1 physics",
              "full (physstat)",
              "turbo (physstat)"]

        for key in li:
            df["$\Delta$ %s" % key] = (df[key] / ref[key] -1) * 100

        # full + turbo
        df["full+turbo (physstat)"] = \
            df["full (physstat)"] + df["turbo (physstat)"]

        # sum all streams
        df["all (event)"] = \
            df["full (event)"] + \
            df["turbo (event)"] + \
            df["turcal (event)"] + \
            df["beamgas (event)"] + \
            df["lumi (event)"]

        # ratio of rates
        df["l0 dihadron lowmult/l0 muon"] = \
            df["l0 dihadron lowmult"] / df["l0 muon"]

        df["l0 hadron/l0 muon"] = df["l0 hadron"] / df["l0 muon"]
        df["l0 electron/l0 muon"] = df["l0 electron"] / df["l0 muon"]
        df["l0 dimuon/l0 muon"] = df["l0 dimuon"] / df["l0 muon"]

    def plot_bar(self, ax=None, lines=[], **kwargs):
        """Plot a bar graph as a function of the fill number.

        Args:
            ax (matplotlib.Axes):
            lines (list):

        Keyword args:
            those of the DataFrame.plot method

        """
        df = self.df

        default = dict(alpha=0.7,
                       colormap="Pastel1",
                       grid=True,
                       kind="bar",
                       legend=True,
                       linewidth=0,
                       rot=45.,
                       width=1)

        default.update(**kwargs)
        df[lines].plot(ax=ax, **default)
        xticks_and_labels(ax,
                          df.fill,
                          xlabel="fill",
                          ylabel="median rate [kHz]",
                          base=1,
                          offset=0.5)

        plot_xticks_all(ax)

    def plot_last_fill(self,
                       fig,
                       lines,
                       fmt="% 20s: % 4.0f kHz (% 3.0f%%)",
                       xfig=0.57,
                       yfig=0.408):
        """ Plot values for the last fill and for the given lines.

        Args:
            fig (matplotlib.Figure):
            lines (list):
            xfig (float):
            yfig (float):

        """
        df = self.df
        ref = self.ref

        txt = ["last fill", ""]
        for key in lines:
            value = df[key].iloc[-1]
            delta = (value / ref[key] -1) * 100
            txt.append(fmt % (key, value, delta))

        bbox={"facecolor": "lightyellow", "linewidth":1., "pad": 15}

        fig.text(xfig,
                 yfig,
                 "\n".join(txt),
                 family="monospace",
                 fontsize=7,
                 verticalalignment="top",
                 bbox=bbox)

    def plot_lines(self, ax, lines, ylabel="median rate [kHz]", **kwargs):
        """Plot a set of lines as a function of the fill number.

        Args:
            ax (matplotlib.Axes):
            lines (list):

        Keyword args:
            those of the DataFrame.plot method

        """
        colors = plt.cm.Set3(np.linspace(0., 1., len(lines)))

        # default plotting option
        default = dict(color=colors,
                       grid=True,
                       kind="line",
                       linestyle="steps",
                       linewidth=1.2,
                       rot=45)

        # update default
        default.update(**kwargs)

        # plot lines, labels, ...
        # protection against null value with log plot
        if "logy" in default and default["logy"]:
            df = self.df4line[lines].replace(0., 1E-06)
        else:
            df = self.df4line[lines]

        df.plot(ax=ax, **default)

	base = (10 if len(df.index) > 100 else 1)

        xticks_and_labels(ax,
                          self.df["fill"],
                          xlabel="fill",
                          ylabel=ylabel,
                          base=base,
                          offset=0.5)

	if base < 10:
            plot_xticks_all(ax)

    def plot_references(self, ax, lines):
        """Plot reference markers.

        Args:
            ax (matplotlib.Axes):
            lines (list):

        """
        ref = self.ref

        legend = ax.get_legend()
        colors = [el.get_color() for el in legend.get_lines()]
        texts = [el.get_text() for el in legend.get_texts()]

        for line in lines:
            i = texts.index(line)

            mycolor = colors[i]
            ax.plot(self.df.index[-1] + 0.5,
                    ref[line],
                    color=mycolor,
                    marker="<",
                    markeredgecolor=mycolor,
                    markersize=5,
                    transform=ax.transData)

        ax.text(1., 1., "markers show references",
                        color="lightsteelblue",
                        fontsize=8,
                        horizontalalignment="right",
                        transform=ax.transAxes,
                        style="italic")


class Reference(dict):
    """Contains reference values for rates, etc. Unit is kHz.

    """
    def __init__(self):

        self.pad = 15

        # NOTE:
        #   - estimated for 2442 colliding bunches
        #   - L0 rates do not include readout dead time nor overlap
        #   - readout dead time is estimated at 7%
        #   - Hlt2 (turbo+physstat) is the sum of the two streams
        #     including overlap
        #   - ref: https://lhcbpr-docs.web.cern.ch/lhcbpr-docs/ (1606)

        nbb = 2332
        mu = 1.1

        deadtime = 0.07
        scale = 1 - deadtime

        self["nbb"] = nbb
        self["deadtime"] = deadtime
        self["visible crossing"] = nbb * 11.245 * (1 - exp(-mu))
        self["l0 physics"] = 938
        self["l0 diem lowmult"] = 1.1 * scale
        self["l0 dimuon"] =  74. * scale
        self["l0 dimuon lowmult"] = 0.24 * scale
        self["l0 dihadron lowmult"] = 24. * scale
        self["l0 electron"] = 190. * scale
        self["l0 electron lowmult"] = 1.1 * scale
        self["l0 hadron"] =  441. * scale
        self["l0 lowmult"] = 29. * scale
        self["l0 muon"] =  309. * scale
        self["l0 muon lowmult"] =  4.3 * scale
        self["l0 photon"] = 50. * scale
        self["l0 photon lowmult"] = 1.1 * scale
        self["hlt1 physics"] = 114.
        self["hlt1 mva"] = 79.
        self["hlt1 muon"] = 9.
        self["hlt1 dimuon"] = 10.
        self["hlt1 lowmult"] = 6.
        self["hlt2 charmhad.*"] = 7.1 # inclusive
        self["hlt2 dimuon.*"] = 1.7 # inclusive
        self["hlt2 lowmult.*"] = 0.5 # inclusive
        self["hlt2 topo.*"] = 2.9 # inclusive
        self["full (physstat)"] = 8.4
        self["turbo (physstat)"] = 5.8
        self["full+turbo (physstat)"] = 14.

    def plot_summary(self, fig):
        """Plot summary information.

        Args:
            fig (matplotlib.Figure):

        """

        pad = self.pad

        #.......... Global ...................................................

        self.plot_summary_condition(fig)

        #.......... L0 .......................................................

        lines = ["l0 physics",
                 "l0 hadron",
                 "l0 muon",
                 "l0 electron",
                 "l0 dimuon",
                 "l0 lowmult",
                 "l0 dihadron lowmult",
                 "l0 dimuon lowmult",
                 "l0 diem lowmult"]

        self.plot_summary_lines(fig,
                                lines,
                                title="L0 (incl. dead time)",
                                xfig=0.345)

        #.......... HLT1 .....................................................

        lines = ["hlt1 physics",
                 "hlt1 mva",
                 "hlt1 muon",
                 "hlt1 lowmult"]

        self.plot_summary_lines(fig,
                                lines,
                                fmt="% 12s: % 4.0f kHz",
                                title="Hlt1",
                                xfig=0.575)

        #.......... HLT2 .....................................................

        lines = ["full (physstat)",
                 "turbo (physstat)",
                 "hlt2 charmhad.*",
                 "hlt2 topo.*",
                 "hlt2 dimuon.*",
                 "hlt2 lowmult.*"]

        self.plot_summary_lines(fig,
                                lines,
                                fmt="% 16s: % 4.1f kHz",
                                title="Hlt2",
                                xfig=0.75)


    def plot_summary_condition(self, fig):
        """Plot summary information related to global information.

        Args:
            fig (matplotlib.Figure):
            lines (list)

        """
        fbb_mhz = self["visible crossing"]
        scale = 1 - self["deadtime"]

        pad = self.pad

        txt = ["references conditions",
               "",
               "% 16s: %i" % ("bb crossing", self["nbb"]),
               "% 16s: %.0f %%" % ("dead time", self["deadtime"]*100),
               "% 16s: %.0f kHz" % ("visible crossing", fbb_mhz)]

        fig.text(0.145, 0.408,
                 "\n".join(txt),
                 family="monospace",
                 fontsize=7,
                 verticalalignment="top",
                 bbox={"facecolor": "none", "linewidth":1., "pad":pad})


    def plot_summary_lines(self,
                           fig,
                           lines,
                           fmt="% 20s: % 4.0f kHz",
                           title="",
                           xfig=0.345,
                           yfig=0.408):
        """Plot summary information for a set of lines

        Args:
            fig (matplotlib.Figure):
            lines (list):

        Keyword Args:
            fmt (str):
            title (str)
            xfig (float):

        """
        self.plot_summary_condition(fig)

        txt = ["references %s" % title, ""]
        for key in lines:
                txt.append(fmt % (key, self[key]))

        fig.text(xfig,
                 yfig,
                 "\n".join(txt),
                 family="monospace",
                 fontsize=7,
                 verticalalignment="top",
                 bbox={"facecolor": "none", "linewidth":1., "pad": self.pad})


def create_pdf(opt, *args):
    """Generate the pdf file. It name depends on the user criteria and on
    the arguments.

    Args:
        opt (argparse.Namespace):
        *args (list): additional key

    Returns:
        matplotlib.backends.backend_pdf.PdfPages

    """
    li = ['last_report',
          str(opt.year),
          str(int(get_beam_energy(opt)))]

    li.extend(args)

    fn = "%s.pdf" % "_".join(li)

    return PdfPages(fn)


def create_view_days(db, opt):
    """Create days view used by the rest of the code.

    This technique allows to apply selection criteria only once.
    In addition, they can depend on the user options.

    Args:
        db (sqlite3.connection): database connection.
        opt (argparse.Namespace): command line options.

    Returns:
        int: the number of days in the view_days

    """
    # filter days for a full year
    ds = datetime(opt.year, 1, 1)
    de = datetime(opt.year, 12, 31)

    # filter days according to run chief periods
    if opt.runchief:
        ds, de = runchief_dates()

    # filter days according to a range of dates
    if opt.dates:
        ds, de = decode_dates(opt)

    sql = """CREATE TEMP VIEW view_days AS
             SELECT * FROM measures_days
             WHERE day >= %s AND day <= %s AND year = %s"""

    sql = sql % (ds.strftime('%j'), de.strftime('%j'), opt.year)
    db.execute(sql)

    if opt.debug:
        sql = "select day from view_days"
        print "\nDays selected:", [row['day'] for row in db.execute(sql)]

    # count the number of days in the view
    sql = 'select count(*) from view_days group by day'
    row = db.execute(sql).fetchone()

    if not row:
        return 0

    return row[0]


def create_view_fills(db, opt):
    """Create runs and fills view used by the rest of the code.
    This technique allows to apply selection criteria only once.
    In addition, they can depend on the user options.

    Args:
        db (sqlite3.connection): database connection.
        opt (argparse.Namespace): command line options.

    Returns:
        int: the number of fills in the view_fills

    """
    # filter fill according to year -- default
    sql = """CREATE TEMP VIEW view_fills AS
             SELECT * FROM measures_fills
             WHERE strftime('%s', start_time)='%s' """ % ('%Y', opt.year)

    # filter fills according to run chief periods
    if opt.runchief:
        ds, de = runchief_dates()

        sql = \
        """CREATE TEMP VIEW view_fills AS
           SELECT *,
                  datetime(start_time, time_total || ' seconds') AS end_time
           FROM   measures_fills
           WHERE  (start_time >= '%s' AND start_time < '%s')
           OR     (end_time > '%s' AND end_time <= '%s') """

        sql = sql % (str(ds), str(de), str(ds), str(de))

    # filter fills according to a range of dates
    if opt.dates:
        ds, de = decode_dates(opt)

        sql = """CREATE TEMP VIEW view_fills AS
             SELECT *,
                    datetime(start_time, time_total || ' seconds') AS end_time
             FROM   measures_fills
             WHERE  (start_time >= '%s' AND start_time < '%s' )
             OR     (end_time >= '%s' AND end_time < '%s') """

        sql = sql % (str(ds), str(de), str(ds), str(de))

    db.execute(sql)

    if opt.debug:
        sql = "select fill_number from view_fills"
        print "\nFills selected:", \
              [row['fill_number'] for row in db.execute(sql)]

    # count the number of fill in the view
    sql = 'select count(*) from view_fills group by fill_number'
    row = db.execute(sql).fetchone()

    if not row:
        return 0

    return row[0]


def create_view_rates(db, opt):
    """Create views for routing bits, hlt1_l0_rates, hlt1_rates and streams.

    This technique allows to apply selection criteria only once.
    In addition, they can depend on the user options.

    This function has to be call after the create_view_fills
    since it relies on it.

    Args:
        db (sqlite3.connection): database connection.
        opt (argparse.Namespace): command line options.

    """
    # filter runs belonging to fills previously selected
    sql = """CREATE TEMP VIEW view_routingbits AS
             SELECT measures_routingbits.*
             FROM   measures_routingbits,
                    view_fills
             WHERE  view_fills.id = measures_routingbits.id_measures_fills"""

    db.execute(sql)

    sql = """CREATE TEMP VIEW view_hlt1_l0rates AS
             SELECT measures_hlt1_l0rates.*
             FROM   measures_hlt1_l0rates,
                    view_fills
             WHERE  view_fills.id = measures_hlt1_l0rates.id_measures_fills"""

    db.execute(sql)

    sql = """CREATE TEMP VIEW view_hlt1_rates AS
             SELECT measures_hlt1_rates.*
             FROM   measures_hlt1_rates,
                    view_fills
             WHERE  view_fills.id = measures_hlt1_rates.id_measures_fills"""

    db.execute(sql)

    sql = """CREATE TEMP VIEW view_hlt2_rates AS
             SELECT measures_hlt2_rates.*
             FROM   measures_hlt2_rates,
                    view_fills
             WHERE  view_fills.id = measures_hlt2_rates.id_measures_fills"""

    db.execute(sql)

    sql = """CREATE TEMP VIEW view_streams AS
             SELECT measures_streams.*
             FROM   measures_streams,
                    view_fills
             WHERE  view_fills.id = measures_streams.id_measures_fills"""

    db.execute(sql)


def create_view_runs(db, opt):
    """Create runs view used by the rest of the code.
    This technique allows to apply selection criteria only once.
    In addition, they can depend on the user options.

    This function has to be call after the create_view_fills
    since it relies on it.

    Args:
        db (sqlite3.connection): database connection.
        opt (argparse.Namespace): command line options.

    Returns:
        int: the number of runs in the view_runs

    """
    # filter runs belonging to fills previously selected
    sql = """CREATE TEMP VIEW view_runs AS
             SELECT measures_runs.*
             FROM   measures_runs,
                    view_fills
             WHERE  view_fills.id = measures_runs.id_measures_fills"""

    db.execute(sql)

    if opt.debug:
        sql = "select run_number from view_runs"
        print "\nRuns selected:", [row['run_number'] for row in db.execute(sql)]

    # count the number of fill in the view
    sql = 'select count(*) from view_runs group by run_number'
    row = db.execute(sql).fetchone()

    if not row:
        return 0

    return row[0]


def decode_dates(opt):
    """Decode the dates option.

    The format can be 2016-08-28T09:00:00 or 2016-08-28

    Args:
        opt (argparse.Namespace): command line options.

    Return:
        tuple: with two datetime objects.

    """
    ds, de = opt.dates

    fmt = ('%Y-%m-%dT%H:%M:%S' if "T" in ds else '%Y-%m-%d')

    ds = datetime.strptime(ds, fmt)
    de = datetime.strptime(de, fmt)

    return (ds, de)


def do_empty(db, opt, pdf):
    """Build pages when there is no data for the selected period.
    Only the page showing the disk usage is shown.

    Args:
        db (sqlite3.connection): database connection
        opt (argparse.Namespace): user criteria
        pdf (matplotlib.PdfPages)

    """
    title = 'No data for the selected period -- HLT1 Disk usage last 2 weeks'
    page_hlt_farm(opt, title)
    pdf.savefig()


def do_pages(db, opt, pdf):
    """Build pages and save them in the PDF file.

    Args:
        db (sqlite3.connection): database connection
        opt (argparse.Namespace): user criteria
        pdf (matplotlib.PdfPages)

    """
    page = opt.page

    if opt.runchief:
        activity = "LHCb -- from %s to %s"
        ds, de = runchief_dates()
        activity = activity % (ds.strftime("%d %b %H:%M"),
                               de.strftime("%d %b %H:%M"))

    else:
        activity = 'LHCb %i' % opt.year

    ref = Reference()

    if page == 999 or page > 8:
        rates = RatesVsFill(db, ref)

    if page in (999, 1):
        title = 'LHCb -- Luminosity over years'
        page_lumi_year(db, opt, title)
        pdf.savefig()

    if page in (999, 2):
        title = '%s -- Luminosity per month' % activity
        page_lumi_month(db, opt, title)
        pdf.savefig()

    if page in (999, 3):
        title = '%s -- Luminosity per week' % activity
        page_lumi_week(db, opt, title)
        pdf.savefig()

    if page in (999, 4):
        title = '%s -- Luminosity per day' % activity
        page_lumi_day(db, opt, title)
        pdf.savefig()

    if page in (999, 5):
        title = '%s -- Luminosity per fill' % activity
        page_lumi_fill(db, opt, title)
        pdf.savefig()

    if page in (999, 6):
        title = '%s -- Luminosity per TCK and magnet polarity' % activity
        page_tck_field(db, title)
        pdf.savefig()

    if page in (999, 7):
        title = '%s -- Running conditions per fill' % activity
        page_condition(db, opt, title)
        pdf.savefig()

    if page in (999, 8) and opt.year == NOW.year:
        title = 'LHCb -- HLT farm last 2 weeks'
        page_hlt_farm(opt, title)
        pdf.savefig()

    if page in (999, 9):
        title = '%s -- Trigger rates overview' % activity
        page_trigger_overview(rates, title)
        pdf.savefig()

    if page in (999, 10):
        title = '%s -- L0 main lines' % activity
        page_trigger_l0(rates, title)
        pdf.savefig()

    if page in (999, 11):
        title = '%s -- L0 low multiplicity lines (CEP)' % activity
        page_trigger_l0_lowmult(rates, title)
        pdf.savefig()

    if page in (999, 12):
        title = '%s -- HLT1 main lines' % activity
        page_trigger_hlt1(rates, title)
        pdf.savefig()

    if page in (999, 13):
        title = '%s -- HLT2 main lines' % activity
        page_trigger_hlt2(rates, title)
        pdf.savefig()

    if page in (999, 14):
        title = '%s -- Streams' % activity
        page_trigger_streams(rates, title)
        pdf.savefig()

    if page in (999, 15):
        title = '%s -- Ratio of L0 rates versus fill' % activity
        page_ratio_fill(rates, title)
        pdf.savefig()

    if page in (999, 16):
        title = '%s -- Ratio of L0 rates versus integrated luminosity' % activity
        page_ratio_lumi(db, title)
        pdf.savefig()

    if page in (999, 17):
        title = '%s -- Relative rate difference w.r.t reference' % activity
        page_reference(rates, title)
        pdf.savefig()

    if page in (999, 18):
        title = '%s -- TCKs history' % activity
        page_tck_map(db, title)
        pdf.savefig()


def header_footer(fig, title):
    """write the header_footer and the footer.

    Args:
        ax (matplotlib.Figure):
        title (str):

    """
    msg = NOW.strftime("%b %d, %Y %H:%M\nday %j -- week %W")

    fig.text(0.05, 0.9, title, fontsize=14)
    fig.text(0.94, 0.89, msg, fontsize=8, horizontalalignment='right')

    fig.text(0.94, 0.04, fig.number, fontsize=10, horizontalalignment='right')




def page_condition(db, opt, title):
    """Plots running conditions per fills.

    Args:
        db (sqlite3.connection): database connection
        opt (argparse.Namespace): user option
        title (str):

    """
    fig = plt.figure()
    header_footer(fig, title)

    #.........................................................................
    #
    # build the data frame

    sql = """SELECT fill_number AS fill,
                    crossing_bunches,
                    mdnfill_mu AS mdn_mu,
                    mdnfill_lumi*pcm2s() AS mdn_lumi,
                    mdnfill_l0_physics_raw_rate AS l0raw,
                    mdnfill_l0_physics_rate AS l0
             FROM view_fills
             ORDER BY fill_number """

    df = pd.read_sql_query(sql, db, index_col="fill")

    #.........................................................................
    #
    # plot number of crossing bunches

    ax = plt.subplot(221)

    df["crossing_bunches"].plot(ax=ax,
                                color=CYANM8,
                                grid=True,
                                kind="bar",
                                linewidth=0.4,
                                rot=45,
                                width=0.8)

    ticks_and_labels(ax, "fill", "crossing bunches")
    ax.xaxis.set_tick_params(which='minor', size=0)

    plot_xticks(ax)

    if opt.runchief or opt.dates:
        plot_values(ax, df["crossing_bunches"])

    #.........................................................................
    #
    # plot readout dead time

    ax = plt.subplot(222)

    df["deadtime"] = (1. - df["l0"] / df["l0raw"]) * 100.
    df["deadtime"].plot(ax=ax,
                        color=CYANM8,
                        grid=True,
                        kind="bar",
                        linewidth=0.4,
                        rot=45,
                        width=0.8)

    ticks_and_labels(ax, "fill", "physics dead time [%]")
    ax.xaxis.set_tick_params(which='minor', size=0)

    plot_xticks(ax)

    if opt.runchief or opt.dates:
        plot_values(ax, df["deadtime"], fmt="%.1f")

    #.........................................................................
    #
    # plot median mu

    ax = plt.subplot(223)

    df["mdn_mu"].plot(ax=ax,
                      color=CHART4,
                      grid=True,
                      kind="bar",
                      linewidth=0.4,
                      rot=45,
                      width=0.8,
                      ylim=[0., 2.])

    ticks_and_labels(ax, "fill", "median $\mu$")
    ax.xaxis.set_tick_params(which='minor', size=0)

    plot_xticks(ax)

    if opt.runchief or opt.dates:
        plot_values(ax, df["mdn_mu"], fmt="%.1f")

    #.........................................................................
    #
    # plot median luminosity

    ax = plt.subplot(224)

    lumi = df["mdn_lumi"] / 1.E32
    lumi.plot(ax=ax,
              color=CHART4,
              grid=True,
              kind="bar",
              linewidth=0.4,
              rot=45,
              width=0.8,
              ylim=[0., 4.5])

    ticks_and_labels(ax,
                     "fill",
                     "median luminosity [10$^{32}$cm$^{-2}$s$^{-1}$]")
    ax.xaxis.set_tick_params(which='minor', size=0)

    plot_xticks(ax)

    if opt.runchief or opt.dates:
        plot_values(ax, lumi, fmt="%.1f")


def page_hlt_farm(opt, title):
    """Disk usage at the output of HLT1 for the last month.

    Args:
        opt (argparse.Namespace): user option
        title (str):

    """
    fig = plt.figure()
    header_footer(fig, title)

    #.........................................................................
    #
    # retrieve the data from the PVSS sensor

    if opt.dates:
        ds, de = decode_dates(opt)

    else:
        de = NOW
        ds = de - timedelta(days=14)

    svc = create_sensors()

    s1 = svc.get("DiskPercentage")
    s1.read(ds=ds, de=de)

    s2 = svc.get("Hlt2InputEventRate")
    s2.read(ds=ds, de=de)

    #.........................................................................
    #
    # plot disk usage

    df = pd.DataFrame(s1.y, index=s1.x)

    ax = plt.subplot(211)

    if not df.empty:

        df.plot(ax=ax,
                grid=True,
                kind="line",
                legend=False,
                xlim=[ds, de])

        ticks_and_labels(ax, ylabel="occupancy [%]")

        msg = "HLT1 disk occupancy"
        ax.text(0.03,
                0.9,
                msg,
                fontsize=9,
                transform=ax.transAxes,
                style="italic")

        # save the plot as a png file
        # duplicate it for the daily report with a different name
        if not OPT.runchief:

            fn1 = DISK_USAGE_PNG % ""

            fig.savefig(fn1,
                        bbox_inches=Bbox([[1., 3.7], [10.6, 7.3]]),
                        dpi=300)

            if os.path.exists(fn1):
                fn2 = DISK_USAGE_PNG % "_%s" % NOW.strftime("%Y%m%d")
                shutil.copy2(fn1, fn2)

    #.........................................................................
    #
    # plot Hlt2 processing rates

    df = pd.DataFrame(s2.y, index=s2.x)
    df = df * 1E-03

    ax = plt.subplot(212)

    if not df.empty:

        df.plot(ax=ax,
                drawstyle="steps-pre",
                grid=True,
                kind="line",
                legend=False,
                xlim=[ds, de],
                ylim=[0., 100.])

        ticks_and_labels(ax, ylabel="rate [kHz]")

        msg = "HLT2 event processing rate"
        ax.text(0.03,
                0.9,
                msg,
                fontsize=9,
                transform=ax.transAxes,
                style="italic")


def page_lumi_day(db, opt, title):
    """Lumi per day for the current year.

    Args:
        db (sqlite3.connection): database connection
        opt (argparse.Namespace): user option
        title (str):

    """
    fig = plt.figure()
    header_footer(fig, title)

    show_value = opt.runchief or opt.dates

    #.........................................................................
    #
    # build the data frame

    lvt = LumiVsTime(db, "day")

    #.........................................................................
    #
    # plot recorded luminosity per day

    ax = plt.subplot(221)
    lvt.plot_recorded(ax, value=show_value, rot=45)

    #.........................................................................
    #
    # plot delivered / recorded luminosity per day

    ax = plt.subplot(222)
    lvt.plot_cumulate(ax)

    #.........................................................................
    #
    # plot data taking efficiency

    ax = plt.subplot(223)
    lvt.plot_efficiency(ax, value=show_value, rot=45)

    #.........................................................................
    #
    # summary

    lvt.plot_summary(fig)


def page_lumi_fill(db, opt, title):
    """Lumi per fill for the current year.

    Args:
        db (sqlite3.connection): database connection
        opt (argparse.Namespace): user option
        title (str):

    """
    fig = plt.figure()
    header_footer(fig, title)

    show_value = opt.runchief or opt.dates

    #.........................................................................
    #
    # build the data frame

    lvt = LumiVsTime(db, "fill")

    #.........................................................................
    #
    # plot recorded luminosity per fill

    ax = plt.subplot(221)
    lvt.plot_recorded(ax, value=show_value, rot=45)

    #.........................................................................
    #
    # plot delivered / recorded luminosity per fill

    ax = plt.subplot(222)
    lvt.plot_cumulate(ax)

    #.........................................................................
    #
    # plot data taking efficiency

    ax = plt.subplot(223)
    lvt.plot_efficiency(ax, value=show_value, rot=45)

    #.........................................................................
    #
    # summary

    lvt.plot_summary(fig)


def page_lumi_month(db, opt, title):
    """Lumi per month for the current year.

    Args:
        db (sqlite3.connection): database connection
        opt (argparse.Namespace): user option
        title (str):

    """
    fig = plt.figure()
    header_footer(fig, title)

    #.........................................................................
    #
    # build the data frame

    lvt = LumiVsTime(db, "month")

    #.........................................................................
    #
    # plot recorded luminosity per month

    ax = plt.subplot(221)
    lvt.plot_recorded(ax, value=True)

    #.........................................................................
    #
    # plot delivered recorded luminosity per month

    ax = plt.subplot(222)
    lvt.plot_cumulate(ax)

    #.........................................................................
    #
    # plot data taking efficiency

    ax = plt.subplot(223)
    lvt.plot_efficiency(ax, value=True)

    #.........................................................................
    #
    # summary

    lvt.plot_summary(fig)


def page_lumi_week(db, opt, title):
    """Lumi per week for the current year.

    Args:
        db (sqlite3.connection): database connection
        opt (argparse.Namespace): user option
        title (str):

    """
    fig = plt.figure()
    header_footer(fig, title)

    #.........................................................................
    #
    # build the data frame

    lvt = LumiVsTime(db, "week")

    #.........................................................................
    #
    # plot recorded luminosity per week

    ax = plt.subplot(221)
    lvt.plot_recorded(ax, value=True)

    #.........................................................................
    #
    # plot delivered recorded luminosity per week

    ax = plt.subplot(222)
    lvt.plot_cumulate(ax)

    #.........................................................................
    #
    # plot data taking efficiency

    ax = plt.subplot(223)
    lvt.plot_efficiency(ax, value=True)

    #.........................................................................
    #
    # plot Hubner factor

    ax = plt.subplot(224)

    lvt.df["hubner"].plot(ax=ax,
                          color="0.9",
                          grid=True,
                          kind="bar",
                          linewidth=0.4,
                          rot=0,
                          width=0.8)

    xticks_and_labels(ax,
                      lvt.df["time"],
                      xlabel="week",
                      ylabel="hubner factor",
                      base=1,
                      offset=0.4)

    plot_values(ax, lvt.df["hubner"], fmt="%.2f", fontsize=7)

    txt = "mean: %.2f $\\pm$ %.2f"
    ds = lvt.df.hubner

    fig.text(0.565, 0.43,
             txt % (ds.mean(), ds.std()/sqrt(len(ds))),
             family='monospace',
             fontsize=8,
             verticalalignment="top",
             bbox={"facecolor": "none", "linewidth": 1., "pad": 2})


def page_lumi_year(db, opt, title):
    """Lumi per year.

    Args:
        db (sqlite3.connection): database connection
        opt (argparse.Namespace): user option
        title (str):

    """
    fig = plt.figure()
    header_footer(fig, title)

    #.........................................................................
    #
    # build the data frame

    sql = """SELECT  (year || '-' || day) AS year_day,
                     delivered_lumi*fb() AS 'delivered luminosity',
                     recorded_lumi*fb() AS 'recorded luminosity',
                     year
             FROM   measures_days
             GROUP BY year, day
             ORDER BY year,day """

    df_lumi = pd.read_sql_query(sql,
                                db,
                                index_col="year_day",
                                parse_dates={"year_day": "%Y-%j"})

    #.........................................................................
    #
    # plot integrated luminosity since 2010 up to now

    ax = plt.subplot(211)

    li = ["delivered luminosity",
          "recorded luminosity"]

    df_lumi[li].cumsum().plot(ax=ax,
                              color=[CHART1, CHART2],
                              grid=True,
                              kind="line")

    ticks_and_labels(ax, ylabel="integrated luminosity [fb$^{-1}$]")

    #.........................................................................
    #
    # plot integrated luminosity per year

    ax = plt.subplot(223)

    df_year =  df_lumi.groupby("year").sum()
    df_year["recorded luminosity"].plot(ax=ax,
                                        color=CHART3,
                                        grid=True,
                                        kind="bar",
                                        linewidth=0.4,
                                        rot=45,
                                        width=0.8)

    ticks_and_labels(ax, ylabel="recorded luminosity [fb$^{-1}$]")
    ax.xaxis.set_tick_params(which='minor', size=0)

    plot_values(ax, df_year["recorded luminosity"], fmt="%.2f")

    #.........................................................................
    #
    # summary

    x = df_year["recorded luminosity"].sum()
    y = df_year["delivered luminosity"].sum()
    mx = df_year["recorded luminosity"].max()

    txt = "Delivered luminosity: %.2f fb$^{-1}$\n"\
          "Recorded luminosity: %.2f fb$^{-1}$\n\n"\
          "Global efficiency: %.0f %%\n\n"\
          "Best integrated luminosity per year: %.2f fb$^{-1}$"

    txt = txt % (y, x, (x/y)*100, mx)

    fig.text(0.57, 0.46,
             txt,
             fontsize=8,
             verticalalignment="top",
             bbox={"facecolor": "none", "linewidth": 1., "pad": 20})


def page_ratio_fill(rates, title):
    """Plots ratio of L0 rates as a function of the fill.
    They are used to follow the calorimeters calibrations.

    Args:
        rates (RatesVsFill): database connection
        title (str):

    """
    fig = plt.figure()
    header_footer(fig, title)

    #.........................................................................
    #
    # plot ratio of l0 rates (calo calibration)

    ax = plt.subplot(211)

    lines = ["l0 hadron/l0 muon",
             "l0 electron/l0 muon",
             "l0 dimuon/l0 muon"]

    rates.plot_lines(ax,
                     lines,
                     ylabel="ratio of median rates",
                     ylim=[0., 2.])

    #.........................................................................
    #
    # ratio of l0 lowmult rates

    ax = plt.subplot(212)

    rates.plot_lines(ax,
                     ["l0 dihadron lowmult/l0 muon"],
                     color="blue",
                     ylabel="l0 dihadron lowmult/l0 muon",
                     ylim=[0., None])


def page_ratio_lumi(db, title):
    """Plots ratio of L0 rates as a function of the recorded luminosity.
    They are used to follow the calorimeters calibrations.

    Args:
        db (sqlite3.connection): database connection
        title (str):

    """
    fig = plt.figure()
    header_footer(fig, title)

    #.........................................................................
    #
    # build the data frame

    sql = \
    """SELECT run_number AS run,
              integrated_lumi*pb() AS recorded_lumi,
              mdnrun_l0_electron*kHz() AS 'L0 electron',
              mdnrun_l0_hadron*kHz() AS 'L0 hadron',
              mdnrun_l0_muon*kHz() AS 'L0 muon'
       FROM   view_runs,
              view_hlt1_l0rates
       WHERE  view_hlt1_l0rates.id_measures_runs = view_runs.id
       ORDER BY run_number """

    df = pd.read_sql_query(sql, db, index_col="run")
    df = df.fillna(0.)

    df["L0 Electron / L0 Muon"] = df["L0 electron"] / df["L0 muon"]
    df["L0 Hadron / L0 Muon"] = df["L0 hadron"] / df["L0 muon"]
    df["cumu_lumi"] = df["recorded_lumi"].cumsum()

    df.index = df["cumu_lumi"]

    #.........................................................................
    #
    # plot ratio of rates

    ax = plt.subplot(211)

    df["L0 Hadron / L0 Muon"].plot(ax=ax,
                                   grid=True,
                                   kind="line",
                                   ylim=[0.8, 1.8])

    ticks_and_labels(ax,
                     "recorded luminosity [pb$^{-1}$]",
                     "L0 Hadron / L0 Muon (HLT1)")


    msg = "L0 rates include readout dead time."
    ax.text(0., 1., msg, fontsize=8, transform=ax.transAxes, style="italic")

    ax = plt.subplot(212)

    df["L0 Electron / L0 Muon"].plot(ax=ax,
                                     grid=True,
                                     kind="line",
                                     ylim=[0.2, 1.2])

    ticks_and_labels(ax,
                     "recorded luminosity [pb$^{-1}$]",
                     "L0 Electron / L0 Muon (HLT1)")

    msg = "L0 rates include readout dead time."
    ax.text(0., 1., msg, fontsize=8, transform=ax.transAxes, style="italic")


def page_reference(rates, title):
    """Plots showint the difference with respect to the refrence for
    some key lines.

    Args:
        rates (RatesVsFill):
        title (str):

    """
    fig = plt.figure()
    header_footer(fig, title)

    #.........................................................................
    #
    # plot delta

    ax = plt.subplot(211)

    lines = ["$\Delta$ l0 physics",
             "$\Delta$ hlt1 physics",
             "$\Delta$ full (physstat)",
             "$\Delta$ turbo (physstat)",
             "$\Delta$ l0 lowmult"]

    rates.plot_lines(ax,
                     lines,
                     ylabel="percentage [%]",
                     ylim=[-110., 50.])

    ax.axhline(0., linestyle="--", linewidth=1.2, color="blue")

    msg = "$\Delta = (rate / reference - 1) \\times 100$"
    ax.text(0.03, 0.9, msg, fontsize=9, transform=ax.transAxes, style="italic")

    #.........................................................................
    #
    # plot summary

    rates.ref.plot_summary(fig)


def page_tck_field(db, title):
    """Plots lumi per l0 tcks and per magnetic field polarity.

    Args:
        db (sqlite3.connection): database connection
        title (str):

    """
    fig = plt.figure()
    header_footer(fig, title)

    plt.subplots_adjust(hspace=0.07)

    #.........................................................................
    #
    # build the data frame

    sql = \
    """SELECT tck2s(tck) AS tck,
              total(integrated_lumi)*pb() as lumi_tck,
              total(CASE WHEN magnet_polarity='UP' THEN integrated_lumi END)*pb() AS up,
              total(CASE WHEN magnet_polarity='DOWN' THEN integrated_lumi END)*pb() AS down
       FROM   view_runs,
              axis_triggers
       WHERE  view_runs.id_axis_triggers = axis_triggers.id
       GROUP BY tck """

    df = pd.read_sql_query(sql, db, index_col="tck")


    #.........................................................................
    #
    # plot tck

    ax = plt.subplot(211)

    df["lumi_tck"].plot(ax=ax,
                        color="lightskyblue",
                        grid=True,
                        kind="bar",
                        linewidth=0.4,
                        rot=0.,
                        width=0.8)

    ticks_and_labels(ax, ylabel="integrated luminosity [pb$^{-1}$]")

    plot_values(ax, df["lumi_tck"])

    #.........................................................................
    #
    # plot tck per field polarity

    ax = plt.subplot(212)

    df[["up", "down"]].plot(ax=ax,
                            color=[CHART3, CHART2],
                            grid=True,
                            kind="bar",
                            linewidth=0.4,
                            rot=15.,
                            sharex=True,
                            width=0.8)

    ticks_and_labels(ax,
                     xlabel="tck",
                     ylabel="integrated luminosity [pb$^{-1}$]")


    maxseries = max((max(df["up"]), max(df["down"])))
    plot_values(ax, df["up"], maxseries=maxseries, xoffset=-0.2)
    plot_values(ax, df["down"], maxseries=maxseries, xoffset=+0.2)

    sup = df.up.sum()
    sdw = df.down.sum()

    pup = sup * 100 / (sup + sdw)
    pdw = sdw * 100 / (sup + sdw)

    txt = "$\\int$ up   = %7.1f pb$^{-1}$ (%.1f %%)\n" \
          "$\\int$ down = %7.1f pb$^{-1}$ (%.1f %%)"

    fig.text(0.15, 0.43,
             txt % (sup, pup, sdw, pdw),
             family='monospace',
             fontsize=8,
             verticalalignment="top",
             bbox={"facecolor": "none", "linewidth": 1., "pad": 5})


def page_tck_map(db, title):
    """Plots tck as a function of fill.

    Args:
        db (sqlite3.connection): database connection
        title (str):

    """
    fig = plt.figure()
    header_footer(fig, title)

    #.........................................................................
    #
    # build the data frame

    sql = """SELECT lower(fill_number) AS fill,
                    tck AS tck,
                    total(integrated_lumi)*pb() as lumi
             FROM   view_runs,
                    view_fills,
                    axis_triggers
             WHERE  view_runs.id_measures_fills = view_fills.id
             AND    view_runs.id_axis_triggers = axis_triggers.id
             GROUP BY fill, tck"""

    df = pd.read_sql_query(sql, db)
    df = df.fillna(0.)

    #.........................................................................
    #
    # list of tcks and fills (unique value)
    #

    tcks = list(set(df["tck"]))
    tcks.sort()

    fills, ifills = [], []
    for fill in df["fill"]:
        if fill not in fills:
            fills.append(fill)
        ifills.append(fills.index(fill))

    nfills = len(fills)
    ntcks = len(tcks)

    #.........................................................................
    #
    # add a contiguous numerical id for tck and fill
    #

    df["itck"] = pd.Series([tcks.index(el) for el in df["tck"]])
    df["ifill"] = pd.Series(ifills)

    #.........................................................................
    #
    # 2D histograms
    #
    ax = plt.subplot(211)

    xedges = [x for x in xrange(nfills+1)]
    yedges = [y for y in xrange(ntcks+1)]

    plt.hist2d(df["ifill"],
               df["itck"],
               weights=df["lumi"],
               bins=(xedges, yedges),
               cmap="Reds")

    clb = plt.colorbar()
    clb.set_label("integraded luminosity [pb$^{-1}$]")

    ax.grid(True, color='r')

    # x-axis
    xaxis = ax.xaxis
    xaxis.set_major_locator(IndexLocator(1, 0.5))

    xlabels = list(fills)
    xlabels.append("")

    xaxis.set_major_formatter(FixedFormatter(xlabels))

    for label in xaxis.get_ticklabels():
        label.set_rotation(45.)

    ax.set_xlabel("fill", horizontalalignment='right', x=1.)

    # y-axis
    yaxis = ax.yaxis
    yaxis.set_major_locator(IndexLocator(1, 0.5))

    ylabels = ["0x%X" % el for el in tcks]
    ylabels.append("")

    yaxis.set_major_formatter(FixedFormatter(ylabels))
    xaxis.set_tick_params(color="r")

    yaxis.set_tick_params(color="r")

    ax.set_ylabel("tck", horizontalalignment='right', y=1.)

    # final polishing
    plot_xticks_all(ax)


def page_trigger_hlt1(rates, title):
    """Plots HLT1 rates per fills.

    Args:
        rate (RatesVsFill):
        title (str):

    """
    fig = plt.figure()
    header_footer(fig, title)

    #.........................................................................
    #
    # plot hlt1 rates

    ax = plt.subplot(211)

    lines = ["hlt1 physics",
             "hlt1 mva",
             "hlt1 muon",
             "hlt1 dimuon",
             "hlt1 lowmult",
             "hlt1 electron",
             "hlt1 nobias"]

    rates.plot_lines(ax,
                     lines,
                     logy=True,
                     ylim=[1., 200])

    # show reference values
    lines = ["hlt1 physics",
             "hlt1 mva",
             "hlt1 muon",
             "hlt1 dimuon",
             "hlt1 lowmult"]

    rates.plot_references(ax, lines)
    rates.ref.plot_summary_lines(fig,
                                 lines,
                                 fmt="% 20s: % 4.0f kHz",
                                 title="Hlt1",
                                 xfig=0.33)

    rates.plot_last_fill(fig, lines, fmt="% 20s: % 4.0f kHz (%+3.0f%%)")


def page_trigger_hlt2(rates, title):
    """Plots HLT2 rates per fills.

    Args:
        rate (RatesVsFill):
        title (str):

    """
    fig = plt.figure()
    header_footer(fig, title)

    #.........................................................................
    #
    # plot hlt1 rates

    ax = plt.subplot(211)

    lines = ["hlt2 charmhad.*",
             "hlt2 topo.*",
             "hlt2 dimuon.*",
             "hlt2 lowmult.*",
             "hlt2 singlemuon.*"]

    rates.plot_lines(ax,
                     lines,
                     logy=False,
                     ylim=[0., 8.])

    # show reference values
    lines = ["hlt2 charmhad.*",
             "hlt2 topo.*",
             "hlt2 dimuon.*",
             "hlt2 lowmult.*"]

    rates.plot_references(ax, lines)
    rates.ref.plot_summary_lines(fig,
                                 lines,
                                 fmt="% 19s: % 4.1f kHz",
                                 title="Hlt2",
                                 xfig=0.33)


def page_trigger_l0(rates, title):
    """Plots l0 trigger rates per fills.

    Args:
        rates (RatesVsFill): database connection
        title (str):

    """
    fig = plt.figure()
    header_footer(fig, title)

    #.........................................................................
    #
    # plot l0 rates

    ax = plt.subplot(211)

    lines = ["l0 physics",
             "l0 hadron",
             "l0 muon",
             "l0 electron",
             "l0 dimuon",
             "l0 lowmult",
             "l0 jetel",
             "l0 jetph"]

    rates.plot_lines(ax,
                     lines,
                     logy=True,
                     ylim=[1., 2.E3])

    msg = "L0 rates includes readout dead time."
    ax.text(0., 1., msg, fontsize=8, transform=ax.transAxes, style="italic")

    # add reference values
    lines = ["l0 physics",
             "l0 hadron",
             "l0 muon",
             "l0 electron",
             "l0 dimuon",
             "l0 lowmult"]

    rates.plot_references(ax, lines)
    rates.ref.plot_summary_lines(fig,
                                 lines,
                                 fmt="% 22s: % 5.0f kHz",
                                 title="L0 (incl. dead time)",
                                 xfig=0.33)

    rates.plot_last_fill(fig, lines, fmt="% 22s: % 5.0f kHz (%+3.0f%%)")


def page_trigger_l0_lowmult(rates, title):
    """Plots l0 trigger rates per fills for low multiplicity lines

    Args:
        rates (RatesVsFill):
        title (str):

    """
    fig = plt.figure()
    header_footer(fig, title)

    #.........................................................................
    #
    # plot l0 LowMult rates

    ax = plt.subplot(211)

    lines = ["l0 lowmult",
             "l0 dihadron lowmult",
             "l0 muon lowmult",
             "l0 diem lowmult",
             "l0 electron lowmult",
             "l0 photon lowmult",
             "l0 dimuon lowmult"]

    rates.plot_lines(ax,
                     lines,
                     logy=True,
                     ylim=[0.01, 40.])

    msg = "L0 rates includes readout dead time."
    ax.text(0., 1., msg, fontsize=8, transform=ax.transAxes, style="italic")

    # show reference values
    rates.plot_references(ax, lines)
    rates.ref.plot_summary_lines(fig,
                                 lines,
                                 fmt="% 22s: % 4.0f kHz",
                                 title="L0 lowmult (incl. dead time)",
                                 xfig=0.33)

    rates.plot_last_fill(fig, lines, fmt="% 22s: % 4.0f kHz (%+3.0f%%)")


def page_trigger_overview(rates, title):
    """Plots trigger and stream rates per fills.

    Args:
        rates (RatesVsFill):
        title (str):

    """
    fig = plt.figure()
    header_footer(fig, title)

    #.........................................................................
    #
    # plot the main rates

    ax = plt.subplot(211)

    lines = ["no bias",
             "visible crossing",
             "l0 physics",
             "hlt1 physics",
             "full+turbo (physstat)",
             "full (physstat)"]

    rates.plot_lines(ax,
                     lines,
                     logy=True,
                     ylim=[1., 2.E4])

    msg = "L0 rates includes readout dead time."
    ax.text(0., 1., msg, fontsize=8, transform=ax.transAxes, style="italic")

    # reference values
    lines = ["visible crossing",
             "l0 physics",
             "hlt1 physics",
             "full+turbo (physstat)",
             "full (physstat)"]

    rates.plot_references(ax, lines)
    rates.ref.plot_summary_lines(fig,
                                 lines,
                                 fmt="% 22s: % 6.0f kHz",
                                 title="overview",
                                 xfig=0.33)

    # last fill values
    lines = ["visible crossing",
             "l0 physics",
             "hlt1 physics"]

    rates.plot_last_fill(fig, lines, fmt="% 22s: % 6.0f kHz (%+3.0f%%)")


def page_trigger_streams(rates, title):
    """Plots stream rates per fills.

    Args:
        rates (RatesVsFill):
        title (str):

    """
    fig = plt.figure()
    header_footer(fig, title)

    #.........................................................................
    #
    # plot stream rates

    ax = plt.subplot(211)

    lines = ["full (event)",
             "turbo (event)",
             "turcal (event)",
             "beamgas (event)",
             "lumi (event)"]

    # draw a stack histograms
    rates.plot_bar(ax=ax,
                   grid=True,
                   lines=lines,
                   stacked=True,
                   ylim=[0., 30.])

    msg = "histograms are stacked"
    ax.text(0., 1., msg, fontsize=8, transform=ax.transAxes, style="italic")

    # save the plot as a png file
    # duplicate it with a different name for the daily report
    if not OPT.runchief:
        fn1 = OUTPUT_STREAMS_PNG % ""

        fig.savefig(fn1,
                    bbox_inches=Bbox([[1., 3.7], [10.6, 7.3]]),
                    dpi=300)

        if os.path.exists(fn1):
            fn2 = OUTPUT_STREAMS_PNG % "_%s" % NOW.strftime("%Y%m%d")
            shutil.copy2(fn1, fn2)


def plot_values(ax, series, fmt="%.0f",
                            fontsize=8.,
                            maxseries=None,
                            percentage=0.10,
                            xoffset=0,
                            yoffset=-10,
                            zero=1E-05,
                            **kwargs):
    """plot values in bar graph.

    Args:
        ax (matplotlib.Axes):
        series (pandas.Series):
        fmt (str):
        fontsize (float): fontsize for the text.
        maxseries (float): maximum of the serie. The maximum of the serie is
            computed by the function and used to determined the threshld.
            In some case, it is usefull to specified a value.
        percentage (float): yoffset is applied if value is a threshold.
            The threshold is computed as percentage*max(series).
            Otherwise the value is written above the bar.
        xoffset (float): offset with respect to the center of the bar in
            data coordinate.
        yoffset (int): offset of the text with respect to the top of the bar
            in the display coordinate.
        zero (float): value below the threhols is not displayed

    keyward arguments:
        those of the matplotlib.text.Text

    """
    ndata = len(series)

    # fontsize depend on the number of data
    fsize = (fontsize if ndata <= 15 else fontsize * sqrt(15. / ndata))

    default = dict(color="0.4", fontsize=fsize, horizontalalignment="center")
    default.update(kwargs)

    # trans formation go to frma the transData to the display coordinate
    # and vice versa
    ftrans = ax.transData.transform
    finv = ax.transData.inverted().transform

    # plot values
    if maxseries is None:
        maxseries = max(series)
    cut = round( maxseries * percentage)

    # protection against np.nan

    isnan = series.isna()
    for i in xrange(ndata):
        value = series.iloc[i]

        if value < zero or isnan.iloc[i]:
            continue

        yoff = (yoffset if value > cut else 2)

        i, y = ftrans((i, value))
        i, y = finv((i, y+yoff))
        i += xoffset
        ax.text(i, y, fmt % value, **default)


def plot_xticks(ax, turn_on=25):
    """Plot the labels along the x-axis for a bar graph

        * plot one tick over 1, 5, 10 bar.
        * optimize the spacing in such a way that the number
          of label is below or equal to 25.
        * plot always the last tick

    Args:
        ax (matplotlib.Axes):
        turn_on (int): the spacing is turn on when the number of tick
            is above this limit.


    """
    xticks = ax.xaxis.get_major_ticks()
    nticks = len(xticks)
    spaces = (5, 10, 25, 50, 100)

    if nticks <= turn_on:
        return

    # optimize the spacing: the number of label do not exceed 25
    for nspace in spaces:
        if nticks / nspace < 25:
            break

    # select tick with label
    for i in xrange(len(xticks)):

        if i % nspace == 0:
            continue

        xticks[i].set_visible(False)

    # display the last point
    for i in xrange(len(xticks)-1, -1, -1):

        tick =  xticks[i]
        if len(tick.label.get_text()) > 0:
            tick.set_visible(True)
            break


def plot_xticks_all(ax, turn_on=60):
    """Plot the tick along the x-axis

        * All tick label are shown
        * the label size deacrease when the number of tick is above
          the turn_on limit.
        * grid is size is 1 over 5

    Args
        ax (matplotlib.Axes):

    Keyword Args:
        turn_on (int):

    """
    xticks = ax.xaxis.get_major_ticks()
    nticks = len(xticks)

    # size of the label
    if nticks > turn_on:
        size = mpl.rcParams['xtick.labelsize'] * (turn_on / float(nticks))
        ax.xaxis.set_tick_params(which='major', labelsize=size)

    # grid 1 over 5
    for i in xrange(len(xticks)):

        if i % 5 == 0:
            continue

        xticks[i].gridOn = False


def runchief_dates():
    """Helper function to return the date of the current run chief period.
    A week starting Tuesday at noon.

    Returns
        tuple: with two datetime object.

    """
    week_day = int(NOW.strftime('%w'))

    # start a new run chief period Tuesday after 2 pm
    if week_day == 2 and NOW.hour < 14:
        shift = 7

    else:
        shift = (week_day - 2) % 7

    ds = datetime(NOW.year, NOW.month, NOW.day, 12, 00)
    ds -= timedelta(days=shift)
    de = ds + timedelta(days=7)

    return (ds, de)


def style():
    """Define the matplotlib style, namely font size, line width, etc.

    """
    mpl.rcParams['axes.labelsize'] = 9
    mpl.rcParams['figure.figsize'] = (29.7 / 2.54, 21 / 2.54)
    mpl.rcParams['figure.subplot.top'] = 0.85
    mpl.rcParams['grid.linestyle'] = 'dotted'
    mpl.rcParams['grid.alpha'] = 0.5
    mpl.rcParams['legend.fontsize'] = 7
    mpl.rcParams['lines.linewidth'] = 1.
    mpl.rcParams['xtick.direction'] = 'in'
    mpl.rcParams['xtick.top'] = True
    mpl.rcParams['xtick.bottom'] = True
    mpl.rcParams['xtick.major.size'] = 8
    mpl.rcParams['xtick.minor.size'] = 4
    mpl.rcParams['xtick.labelsize'] = 8
    mpl.rcParams['ytick.direction'] = 'in'
    mpl.rcParams['ytick.left'] = True
    mpl.rcParams['ytick.right'] = True
    mpl.rcParams['ytick.major.size'] = 8
    mpl.rcParams['ytick.minor.size'] = 4
    mpl.rcParams['ytick.labelsize'] = 8


def ticks_and_labels(ax, xlabel="", ylabel=""):
    """Set ylabel and activate minor ticks.

    Args:
        ax (matplotlib.Axes):
        xlabel (str):
        ylabel (str):

    """
    ax.minorticks_on()

    ax.set_xlabel(xlabel, horizontalalignment='right', x=1.)
    ax.set_ylabel(ylabel, horizontalalignment='right', y=1.)


def xticks_and_labels(ax, xticks, xlabel="", ylabel="", base=1., offset=0.5):
    """Set ylabel and activate minor ticks.
    To be used with line graph showing data as a function of the fill.

    Args:
        ax (matplotlib.Axes):
        xticks (list):
        xlabel (str):
        ylabel (str):
        base (int): base for the IndexLocator
        offset (float): offset for the IndexLocator

    """

    ax.set_xlabel(xlabel, horizontalalignment='right', x=1.)
    ax.set_ylabel(ylabel, horizontalalignment='right', y=1.)

    ax.xaxis.set_major_locator(IndexLocator(base, offset))

    lst = [el for i, el in enumerate(xticks) if i % base == 0]
    ax.xaxis.set_ticklabels(lst)

    ax.minorticks_on()
    ax.xaxis.set_tick_params(which='minor', size=0)


if __name__ == "__main__":

    from argparse import ArgumentParser

    #.........................................................................
    #
    # User criteria

    AGP = ArgumentParser()

    AGP.add_argument(
        "--db",
        default="triggerdb.sqlite",
        help="File name for the sqlite database [%(default)s].")

    AGP.add_argument(
        "-d", "--debug",
        action="store_true",
        help="activate the debug mode [%(default)s]")

    AGP.add_argument(
        "--dates",
        nargs=2,
        help="Define a range of dates, i.e. 2011-03-12 2011-05-24 or "
             "2016-08-26T09:00:00 2016-08-29T09:00:00",
        metavar="<date>")

    AGP.add_argument(
        "-p", "--page",
        default=999,
        help="Select one page identified by its page number.",
        metavar="<number>",
        type=int)

    AGP.add_argument(
        "-r", "--runchief",
        action="store_true",
        help="Produce a report for the current run chief period [%(default)s].")

    AGP.add_argument(
        "-y", "--year",
        default=NOW.year,
        help="Select the year [%(default)s].",
        metavar="<number>",
        type=int)

    OPT = AGP.parse_args()

    if  OPT.runchief and OPT.year != NOW.year:
        print "\n\tThe option --runchief can only be used",
        print "with the current year.\n"
        sys.exit(1)

    log("Start appPDF.py")
    print_options(vars(OPT))

    print "       matplotlib:", mpl.__version__
    print "           pandas:", pd.__version__

    #.........................................................................
    #
    # database connection and views

    DB = create_db(OPT.db)
    create_sql_functions(DB)

    if is_inhibit(DB, OPT.year):
        log("The database is in inhibit state, exit")
        sys.exit(0)

    create_view_days(DB, OPT)
    NFILLS = create_view_fills(DB, OPT)

    if NFILLS > 0:
        create_view_runs(DB, OPT)
        create_view_rates(DB, OPT)

    #.........................................................................
    #
    # matplotlib style and PDF file

    style()

    if OPT.runchief:
        PDF = create_pdf(OPT, "runchief")
    else:
        PDF = create_pdf(OPT, "summary")

    #.........................................................................
    #
    # generate pages

    if NFILLS > 0:
        do_pages(DB, OPT, PDF)

    else:
        do_empty(DB, OPT, PDF)

    #.........................................................................
    #

    PDF.close()
    log("PDF report written")

    log("End appPDF.py")
    sys.exit(0)
