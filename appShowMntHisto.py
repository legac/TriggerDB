#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" NAME
        appShowMntHisto -- show available Mnt histograms

    SYNOPSIS
        appShowMntHisto [Options]

    DESCRIPTION
        show available Mnt histograms for a given fill

    OPTIONS
        --version

    EXAMPLES

        > lb-run MooreOnline/latest ./appShowMntHisto.py -h

    AUTHOR
        R. Le Gac, legac@cppm.in2p3.fr

"""
import os
import sys

from argparse import ArgumentParser
from ROOT import TFile, TH1D

if __name__ == "__main__":


    PATH_BYRUN = '/hist/Savesets/ByRun/%s/%i0000/%i000/%s-run%i.root'

    #.........................................................................

    agp = ArgumentParser()
    agp.add_argument("-r", "--run", metavar="<number>", type=int)
    opt = agp.parse_args()

    #.........................................................................

    run = opt.run
    task = "Moore2Saver"

    path = PATH_BYRUN % (task,
                         run / 10000,
                         run / 1000,
                         task,
                         run)

    print "\n", path
    if not os.path.exists(path):
        print "file do not exist (yet).\n"
        sys.exit(1)

    #.........................................................................

    fi = TFile(path)

    keys = fi.GetListOfKeys()
    for el in keys:
        kname = el.GetName()
        print "\n", kname

        rdir = fi.Get(kname)
        for el in rdir.GetListOfKeys():
            hname = el.GetName()
            hi = rdir.Get(hname)
            if isinstance(hi, TH1D):
                print "\t% 35s: %.0f" % (hname, hi.GetEntries())

    #.........................................................................

    fi.Close()
    sys.exit(0)
