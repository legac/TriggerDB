"""rdbjson

Wrapper around the JSON API for the run database.

The documentation of the JSON API is available at
https://lbdokuwiki.cern.ch/rundb:rundb_web_interface_api

"""
import json
import urllib


URL_FILL = "http://lbrundb.cern.ch/api/fill/%s"
URL_PHYSICSFILLS = "http://lbrundb.cern.ch/api/fills/physics/?starttime=%s&endtime=%s"
URL_RUN = "http://lbrundb.cern.ch/api/run/%s"
URL_RUNDATA = "http://lbrundb.cern.ch/api/rundata/%s"
URL_RUNSINFILL = "http://lbrundb.cern.ch/api/runs_in_fill/%s"


def get_fill_info(fillnumber):
    """
    Args:
        fillnumber (int):

    Returns:
        dict: fill information. The keys are: fill_id, lumi_hvon, lumi_logged
            lumi_running, lumi_total, lumi_veloin, nBunchesB1, nBunchesB2,
            nCollidingBunches, peakHltPhysRate, peakL0PhysRate, peakLumi,
            peakMu, peakPhysDeadTime, prev_fill_id, time_hvon, time_logged,
            time_running, time_total, time_veloin, timestamp, veloLostLumi,
            veloLostTime

    """
    if fillnumber == 0 or fillnumber < 0:
        raise ValueError("Invalid fill number %s" % fillnumber)

    return json2dict(URL_FILL % fillnumber)


def get_physics_fills(start_date, end_date):
    """Information on physics fills located in the period defined
    by the start and by the end dates.

    Args:
        start_date (datetime.datetime):
        end_date (datetime.datetime):

    Returns:
        list: of dictionaries containing the fill information.

    """
    ds = start_date.replace(microsecond=0).isoformat()
    de = end_date.replace(microsecond=0).isoformat()

    return json2dict(URL_PHYSICSFILLS % (ds, de))


def get_run_data(runnumber):
    """
    Args:
        runnumber (int):

    Returns:
        dict: with two keys run_data and stream_data. The value for the first
            one are those returns by get_run_info while the second contains
            the data for the streams (see get_streams).

    """
    if runnumber == 0 or runnumber < 0:
        raise ValueError("Invalid run number %s" % runnumber)

    return json2dict(URL_RUNDATA % runnumber)


def get_run_info(runnumber):
    """
    Args:
        runnumber (int):

    Returns:
        dict: run information. The keys are: LHCState, PHYSICS, avHltPhysRate,
            avL0PhysRate, avLumi, avMu, avPhysDeadTime, beamenergy,
            beamgasTrigger, betaStar, conddbTag, dddbTag, destination,
            endtime, fillid, hlt2AlignVersions, hlt2CalibVersions,
            hlt2TriggerConfig, hlt2TriggerSettings, lumiTrigger, magnetCurrent,
            magnetState, next_runid, nobiasTrigger, partitionid, partitionname,
            prev_runid, program, programVersion, runid, runtype, starttime,
            state, tck, triggerConfiguration, veloOpening, veloPosition'

    """
    if runnumber == 0 or runnumber < 0:
        raise ValueError("Invalid run number %s" % runnumber)

    return json2dict(URL_RUN % runnumber)


def get_runs_for_fill(fillnumber):
    """
    Args:
        fillnumber (int):

    Returns:
        list: of dictionaries containing run information.

    """
    if fillnumber == 0 or fillnumber < 0:
        raise ValueError("Invalid fill number %s" % fillnumber)

    return json2dict(URL_RUNSINFILL % fillnumber)


def get_streams(runnumber):
    """
    Args:
        runnumber (int):

    Returns:
        dict: one entry for each streams. Stream information is in a dictionary
            with the following keys: events, files, n_beamgas_exc,
            n_beamgas_inc, n_lumi_exc, n_lumi_high, n_lumi_inc, n_lumi_low,
            n_minbias_exc, n_minbias_inc, n_other_exc, n_other_inc,
            n_physics_exc, n_physics_inc, nevent_0, nevent_1, nevent_3,
            nevent_4, nevent_5, nevent_6, nevent_7, nevents2, physstat

    """
    if runnumber == 0 or runnumber < 0:
        raise ValueError("Invalid run number %s" % runnumber)

    return get_run_data(runnumber)[u"stream_data"]


def json2dict(url):
    """Convert the JSON string returns by the URL into a python object.

    Args:
        url (str):

    Returns:
        dict, list, string

    """
    fi = urllib.urlopen(url)
    return json.load(fi)


