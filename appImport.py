#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" NAME
        appImport -- Import a CSV file into a database table

    SYNOPSIS
        appImport [Options] 

    DESCRIPTION

    OPTIONS

    EXAMPLES

        > ./appImport.py -c foo.cvs  -t table

    AUTHOR
        R. Le Gac, legac@cppm.in2p3.fr

"""

__author__ = "legac@cppm.in2p3.fr"


import csv
import sqlite3
import sys


if __name__ == "__main__":
    
    from optparse import OptionParser
    global db, opt, rdb
    
    # NOTE:
    # the database connection db and the script options opt
    # are global variables
    
    # Define and process script options
    parser = OptionParser(version="\n\t$Revision$\n".replace('$', ''))

    parser.add_option("-d", "--db",
                      dest="dbname",
                      help="File name for the sqlite database [%default].")
    
    parser.add_option("-c", "--csv",
                  dest="csv",
                  help="Name of the CSV file.")

    parser.add_option("-t", "--table",
                  dest="table",
                  help="Name of the database table.")

    parser.set_defaults(dbname='triggerdb.sqlite')
    
    (opt, args) = parser.parse_args()

    # connection to the databases
    db = sqlite3.connect(opt.dbname)
    db.row_factory = sqlite3.Row

    # open the CSV file
    # the first line should contains the column names
    ifirst = True
    sql = "INSERT INTO %s (%s) VALUES (%s)"
    for row in csv.reader(open(opt.csv, 'rb')):
        
        if ifirst:
            sql = sql % (opt.table, ','.join(row), ','.join(['?']*len(row)))
            ifirst = False
            continue
        
        db.execute(sql, row)
        
    db.commit()    
    sys.exit(0)
    