#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" NAME
        fixHlt1L0Rates -- fix L0 rates measured by HLT1 for a given run

    SYNOPSIS
        fixStreams [Options]

    DESCRIPTION
        In June 2016, redesign the measures_streams table.

        The script create one entry for each run. Values for the stream
        columns are empty.

    OPTIONS
            --version
                Return the version number of this scripts

            --fill
                fill number

    EXAMPLES

        > SetupProject MooreOnline
        > ./fixHlt1L0Rates.py -f 4924

    AUTHOR
        R. Le Gac, legac@cppm.in2p3.fr

"""
import sys

from appFillDB import create_sensors, insert_or_update
from base import (create_db,
                  log,
                  print_options)

def fix(db, svc, opt):
    """Fix measures_streams table by adding one row for each existing run.
    Values for the streams columns are NULL.

    Args:
        db (sqlite3.connection): database connection
        svc (SensorSvc): sensors service
        opt (argparse.Namespace): user criteria

    """
    # existing runs
    sql = """SELECT id, id_measures_fills
             FROM measures_runs
             WHERE substr(start_time, 1, 4) = '2016'
    """

    for row in db.execute(sql):

        values = dict(id_measures_runs=row["id"],
                      id_measures_fills=row["id_measures_fills"])

        insert_or_update(db, "measures_streams", **values)


if __name__ == "__main__":

    from argparse import ArgumentParser

    #.........................................................................
    #
    # user criteria
    #

    AGP = ArgumentParser()

    AGP.add_argument(
        "--db",
        default="triggerdb.sqlite",
        help="File name for the sqlite database [%(default)s].")

    ARGS = AGP.parse_args()

    log("Start fixStreams.py")
    print_options(vars(ARGS))

    #.........................................................................
    #
    # data base connection and sensors
    #

    DB = create_db(ARGS.db)
    SVC = create_sensors()

    #.........................................................................
    #
    # do the job and exit
    #

    fix(DB, SVC, ARGS)

    log("End fixStreams.py")
    sys.exit(0)

