#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" NAME
        appFillDBHlt2 -- fill the trigger database with HLT2 information

    SYNOPSIS
        appFillDBHlt2 [Options]

    DESCRIPTION
        The HLT2 processing is run asynchronously with respect to HLT2.
        This script complete the database with the rate of the routing bits
        published by HLT2 processing.

    OPTIONS


    EXAMPLES

        > lb-run MooreOnline/latest ./appFillDBHlt2.py -y 2015
        > lb-run MooreOnline/latest ./appFillDBHlt2.py -y 2015 -r 160843 -r 160844
        > lb-run MooreOnline/latest ./appFillDBHlt2.py -y 2015 -f 4246

    AUTHOR
        R. Le Gac, legac@cppm.in2p3.fr

"""
import math
import rdbjson
import sys


from appFillDB import create_sensors, insert_or_update ,SENSORS_HLT2_RATES
from argparse import ArgumentParser
from base import (create_db,
                  is_inhibit,
                  log,
                  print_options)
from datetime import datetime
from pprint import pprint
from sensors import STREAMS


NOW = datetime.now()


def get_runs(db, sensor_svc):
    """Determine the runs without Hlt2 information.

    Search is performed using the measures_streams table.

    Args:
        db (sqlite3.Connection): database connection
        sensor_svc(sensor.SensorSvc): sensors service

    Returns:
        sqlite3.Connection: the underlying row contains:
            * id_measures_fills
            * id_measures_runs
            * fill_number
            * run number

    """
    if ARGS.debug:
        log("Start get_runs")

    # SQL template get the list of run to be updated
    sql = """SELECT measures_streams.id_measures_fills,
                    measures_streams.id_measures_runs,
                    fill_number,
                    run_number
             FROM measures_streams,
                  measures_fills,
                  measures_runs,
                  axis_times
             WHERE measures_streams.id_measures_fills = measures_fills.id
             AND   measures_streams.id_measures_runs = measures_runs.id
             AND   measures_runs.id_axis_times = axis_times.id
             %s
          """

    # List of runs defined by a fill number
    if ARGS.fill:
        tmp = "AND fill_number = %i" % ARGS.fill
        sql = sql % tmp

    # List of runs if given in the script arguments
    elif ARGS.runs:
        runs = [str(el) for el in ARGS.runs]
        tmp = "AND run_number in (%s)" % ', '.join(runs)
        sql = sql % tmp

    # force the scan for a given year
    elif ARGS.force:
        tmp = "AND year = %s" % ARGS.year
        sql = sql % tmp

    # force the scan for the last n run
    #
    # NOTE 2016
    #
    # It happen that full event rates is not null
    # but the value is not the right one (to low).
    # It is because a run is made of several file. File are add to the run
    # data base one after the others. It is why the number of events increase
    # as a function of time.
    #
    # This option allows to fix this issue.
    #
    elif ARGS.year == NOW.year and ARGS.last > 0:

        sql1 = """select fill_number
                  from   measures_fills
                  where  substr(start_time, 1, 4) = '%s'
                  order by fill_number"""

        sql1 = sql1 % ARGS.year
        rows = db.execute(sql1)

        if rows is None:
            print "No fills found."
            sys.exit(1)

        fills = [str(row["fill_number"]) for row in rows]
        if len(fills) > ARGS.last:
            fills = fills[-ARGS.last:]

        tmp = "AND fill_number in (%s)" % ', '.join(fills)
        sql = sql % tmp

    # Scan the database to find run for which the hlt2 rate is null
    else:
        tmp = """AND mdnrun_full_events ISNULL
                 AND year  = %s""" % ARGS.year
        sql = sql % tmp

    return db.execute(sql).fetchall()


def is_hlt2_over(db, svc, row):
    """Check that the run is process by HLT2 and that the processing is over.

    Args:
        db (sqlite3.Connection): database connection
        svc(sensor.SensorSvc): sensors service
        row (sqlite3.Row):

            * id_measures_fills
            * id_measures_runs
            * fill_number
            * run number

    Return:
        bool

    """
    fill = row["fill_number"]
    run = row["run_number"]

    svcget = svc.get

    # check full / turbo stream rate
    for stream in ("full", "turbo"):
        s1 = svcget("stream_%s_physstat" % stream, run=run)
        val = s1.value()
        if val is None or int(val) == 0:
            txt = "%s (physstat) rate is null" % stream
            log("Reject run %i (%i) -- %s" % (run, fill, txt))
            return False

    # processing start but it is not finished
    # routing bit 87 is for the full stream
    # estimate the run duration from the routing bit histogram and compare
    # with the value in the run database

    s1 = svcget("RoutingBit87", run=run)

    li = s1.y
    if len(li) == 0:
        txt = "RoutingBit87 rate is null"
        log("Reject run %i (%i) -- %s" % (run, fill, txt))
        return False

    duration_rb = None
    for i in xrange(len(li)-1, -1, -1):
        value = li[i]
        if math.isnan(value):
            continue

        if int(value) != 0:
            duration_rb = (i + 1) * s1.h.GetBinWidth(i)
            break

    if duration_rb is None:
        txt = "Run duration from RoutingBit87 is null"
        log("Reject run %i (%i) -- %s" % (run, fill, txt))
        return False

    ds = svcget("startTime", run=run).value()
    de = svcget("endTime", run=run).value()

    duration_run = (de-ds).total_seconds()

    if duration_rb/duration_run < 0.9:
        txt = "processing is below 90 %"
        log("Reject run %i (%i) -- %s" % (run, fill, txt))
        return False

    return True


def fill_db_hlt2(db, svc):
    """Fill the database with the Hlt2 information.

    Args:
        db (sqlite3.Connection): database connection
        svc(sensor.SensorSvc): sensors service

    """
    if ARGS.debug:
        log("Start fill_db_hlt2")

    # process each run
    for row in get_runs(db, svc):

        if not is_hlt2_over(db, svc, row):
            continue

        log("Process run %i (%i)" % (row["run_number"], row["fill_number"]))

        process_hlt2(db, svc, row)
        process_routing_bits(db, svc, row)
        process_stream(db, svc, row)


def process_hlt2(db, svc, row):
    """Process hlt2 rate for the run.

    Args:
        db (sqlite3.Connection): database connection
        svc(sensor.SensorSvc): sensors service
        row (sqlite3.Row):

            * id_measures_fills
            * id_measures_runs
            * fill_number
            * run number

    """
    if ARGS.debug:
        log("Start process hlt2 rates")

    svcget = svc.get

    id_fill = row["id_measures_fills"]
    id_run = row["id_measures_runs"]
    run = row["run_number"]

    data = dict(id_measures_fills=id_fill, id_measures_runs=id_run)

    # existing entry
    sql = """SELECT id
             FROM measures_hlt2_rates
             WHERE id_measures_fills = ?
             AND id_measures_runs = ?"""

    row1 = db.execute(sql, (id_fill, id_run)).fetchone()
    if row1 is not None:
        data["id"] = row1["id"]

    # loop on sensors
    for name in SENSORS_HLT2_RATES:

        tmp = name.lower().replace("hltrate_", "").replace("_rate", "")
        field_name = "mdnrun_%s" % tmp

        if name in svc:
            s1 = svcget(name, run=run)
            value = s1.y_median(n=2, excludeZero=True, nz=True)
            data[field_name] = value

    if ARGS.debug:
        print "Insert or update measares_hl2_rates"
        pprint(data)

    insert_or_update(db, 'measures_hlt2_rates', **data)


def process_routing_bits(db, svc, row):
    """Process routing bits for the run.

    Args:
        db (sqlite3.Connection): database connection
        svc(sensor.SensorSvc): sensors service
        row (sqlite3.Row):

            * id_measures_fills
            * id_measures_runs
            * fill_number
            * run number

    """
    if ARGS.debug:
        log("Start process routing bits")

    data = {}

    # existing entry
    sql = """SELECT id
             FROM measures_routingbits
             WHERE id_measures_fills = ?
             AND id_measures_runs = ?"""

    tpl = (row["id_measures_fills"], row["id_measures_runs"])
    row1 = db.execute(sql, tpl).fetchone()

    if row1 is not None:
        data["id"] = row1["id"]

    # interrogate sensors
    run = row["run_number"]
    svcget = svc.get

    # the routing bits rate from RB64 to RB91
    for i in xrange(64, 92):
        field_name = "mdnrun_rb_%02i" % i
        sensor_name = "RoutingBit%02i" % i

        if sensor_name in svc:
            s1 = svcget(sensor_name, run=run)
            value = s1.y_median(n=2, excludeZero=True, nz=True)
            data[field_name] = value

    insert_or_update(db, "measures_routingbits", **data)

    if ARGS.debug:
        print "Insert or update measures_routingbits"
        pprint(data)

    # the event size after HLT2
    data = dict(id=row["id_measures_runs"])

    s1 = svcget('TotSize(HLT-Pass)', run=run)
    data["avg_evt_size"] = s1.get_mean(n=1, nz=True)

    if ARGS.debug:
        print "Insert or update measures_runs"
        pprint(data)

    insert_or_update(db, "measures_runs", **data)


def process_stream(db, svc, row):
    """Process stream information.

    Args:
        db (sqlite3.Connection): database connection
        svc(sensor.SensorSvc): sensors service
        row (sqlite3.Row):

            * id_measures_fills
            * id_measures_runs
            * fill_number
            * run number

    """
    if ARGS.debug:
        log("Start process streams rates")

    svcget = svc.get

    id_fill = row["id_measures_fills"]
    id_run = row["id_measures_runs"]
    run = row["run_number"]

    data = dict(id_measures_fills=id_fill, id_measures_runs=id_run)

    # existing entry
    sql = """SELECT id
             FROM measures_streams
             WHERE id_measures_fills = ?
             AND id_measures_runs = ?"""

    row1 = db.execute(sql, (id_fill, id_run)).fetchone()
    if row1 is not None:
        data["id"] = row1["id"]

    # loop on the stream
    for stream in STREAMS:

        stream_lower_case = stream.lower()
        s1 = svcget("stream_%s_events" % stream_lower_case, run=run)
        s2 = svcget("stream_%s_physstat" % stream_lower_case, run=run)

        key1 = "mdnrun_%s_events" % stream_lower_case
        key2 = "mdnrun_%s_physstat" % stream_lower_case

        data[key1] = s1.rate(n=2)
        data[key2] = s2.rate(n=2)

    if ARGS.debug:
        print "Insert or update measures_streams"
        pprint(data)

    insert_or_update(db, "measures_streams", **data)


if __name__ == "__main__":

    # Command line options
    AGP = ArgumentParser()

    AGP.add_argument(
        "--db",
        default="triggerdb.sqlite",
        help="file name for the sqlite database [%(default)s]. "
             "The special value ':memory:' create the database in RAM.")

    AGP.add_argument(
        "-d", "--debug",
        action="store_true",
        help="activate the debug mode [%(default)s].")

    AGP.add_argument(
        "-f", "--fill",
        help="the fill number.",
        metavar="<number>",
        type=int)

    AGP.add_argument(
        "-F", "--force",
        action="store_true",
        help="recompute all values and update the database for a given year.")

    AGP.add_argument(
        "-l", "--last",
        help="process the last n fills."
             "The option year have to be equal to the current year.",
        metavar="<number>",
        type=int)

    AGP.add_argument(
        "-r", "--runs",
        action="append",
        help="the run number. This option can be repeated for several runs.",
        metavar="<number>",
        type=int)

    AGP.add_argument(
        "-y", "--year",
        default=datetime.now().year,
        help="select the year [%(default)s]. "
             "Make no sense with the options --fill and --runs.",
        metavar="<number>",
        type=int)

    ARGS = AGP.parse_args()

    log("Start appFillDBHlt2.py")
    print_options(vars(ARGS))

    # protection -- Only for Run 2
    if ARGS.year < 2015:
        log("Invalid year. Valid only for Run2 or later.  exit")
        sys.exit(1)

    # connection to the databases
    DB = create_db(ARGS.db)

    # inhibit state exit immediately
    if is_inhibit(DB, ARGS.year):
        log("The database is in inhibit state, exit")
        sys.exit(1)

    # initialise all sensors
    SVC = create_sensors(ARGS.debug)

    # fill the database
    fill_db_hlt2(DB, SVC)

    # exit
    log("End appFillDBHlt2.py")
    sys.exit(0)

