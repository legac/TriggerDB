#!/bin/bash

# Produce summary reports related to running conditions and triggers
# using the run database, the online PVSS archive and monitoring save sets.

cwd=/group/trg/reports
mylog=last_report.log
webdir=/web/sites/triggerreport/production/html/reports
tckdir=l0tcks

year=`date +%Y`

source /cvmfs/lhcb.cern.ch/lib/LbLogin.sh &>> /dev/null
source /sw/oracle/set_oraenv.sh &>> /dev/null

# create directory structure for a new year
# clean reports of previous year

if [ ! -d $webdir/$year ]
then
  mkdir $webdir/$year
  mkdir $webdir/$year/csv
  mkdir $webdir/$year/html
  mkdir $webdir/$year/json
  mkdir $webdir/$year/png
  rm -f $webdir/last_report*.ods
  rm -f $webdir/last_report*.pdf
fi

# run python scripts to fill the local sqlite database
# and to produce reports as well as TCK documentation

cd $cwd
lb-run MooreOnline/latest ./appFillDB.py     -y $year &> $mylog
lb-run MooreOnline/latest ./appFillDBHlt2.py -y $year --last 25 &>> $mylog
lb-run MooreOnline/latest ./appFillDBHlt2.py -y $year &>> $mylog
lb-run MooreOnline/latest ./appReport.py     -y $year -CHJ &>> $mylog
lb-run MooreOnline/latest ./appPDF.py        -y $year &>> $mylog
lb-run MooreOnline/latest ./appPDF.py        -y $year --runchief &>> $mylog
lb-run MooreOnline/latest ./appL0TCK.py      -y $year &>> $mylog

# copy pdf report in the web area top directory
cp -a last_report_${year}*.pdf $webdir

# move png image in the web area top directory
mv disk_usage.png $webdir
mv output_streams.png $webdir

# move png image on the web area yearly directory for daily report
mv disk_usage_*.png $webdir/$year/png/
mv output_streams_*.png $webdir/$year/png/

# move reports on the web area yearly directory
mv last_report_${year}*.csv  $webdir/$year/csv/
mv last_report_${year}*.html $webdir/$year/html/
mv last_report_${year}*.json $webdir/$year/json/
mv last_report_${year}*.pdf  $webdir/$year/

# copy l0tck documentation to the web area
rsync ${tckdir}/*.txt ${webdir}/${tckdir}/

exit
