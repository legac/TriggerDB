#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" NAME
        fixHlt1L0Rates -- fix L0 rates measured by HLT1 for a given run

    SYNOPSIS
        fixHlt1L0Rates [Options]

    DESCRIPTION
        Starting in 2016, the L0 rates are measured by a dedicated algorithm
        which replaces the routing bits.

        The data are stored in trending file.
        For an unknown reason, from time to time the history in the trending
        file is lost.

        The aim of this script is to reconstruct the HLT1 L0 rates
        from the value measured by the L0DU PVSS data point, taking into
        account the readout dead time, satellite bunches, ....

        It is also possible to recover the rate for the L0 LowMult. But this
        behaviour has been deactivated since removing the effect of the
         satellite bunches on the  DiHadronLowMult line is not trivial...

        The process is performed on a fill basis

    OPTIONS
            --version
                Return the version number of this scripts

            --fill
                fill number

    EXAMPLES

        > SetupProject MooreOnline
        > ./fixHlt1L0Rates.py -f 4924

    AUTHOR
        R. Le Gac, legac@cppm.in2p3.fr

"""
import sys

from appFillDB import create_sensors, insert_or_update
from base import (create_db,
                  create_sql_functions,
                  log,
                  print_options)
from datetime import datetime, timedelta

LI_DB = [("mdnrun_l0du_electron_rate", "mdnrun_l0_electron"),
         ("mdnrun_l0du_dimuon_rate", "mdnrun_l0_dimuon"),
         ("mdnrun_l0du_muon_rate", "mdnrun_l0_muon"),
         ("mdnrun_l0du_photon_rate", "mdnrun_l0_photon")]

LI_PVSS = [("TrgRate_L0ElectronLowMult", "mdnrun_l0_electron_lowmult"),
           ("TrgRate_L0DiEMLowMult", "mdnrun_l0_diem_lowmult"),
           ("TrgRate_L0DiHadronLowMult", "mdnrun_l0_dihadron_lowmult"),
           ("TrgRate_L0DiMuonLowMult", "mdnrun_l0_dimuon_lowmult"),
           ("TrgRate_L0MuonLowMult", "mdnrun_l0_muon_lowmult"),
           ("TrgRate_L0PhotonLowMult", "mdnrun_l0_photon_lowmult")]


def fix(db, svc, opt):
    """Fix L0 rates measured by Hlt1.

    Args:
        db (sqlite3.connection): database connection
        svc (SensorSvc): sensors service
        opt (argparse.Namespace): user criteria

    """
    fill = opt.fill
    svcget = svc.get

    # get the fill identifier
    sql = """SELECT id, mdnfill_led_rate, start_time
             FROM   measures_fills
             WHERE fill_number = ? """

    row = db.execute(sql, (fill,)).fetchone()
    if row is None:
        log("Fill %s not found" % fill)
        sys.exit(1)

    fill_id, mdnfill_led_rate, start_time = row

    if mdnfill_led_rate is None:
        mdnfill_led_rate = 0.

#     # estimate the contamination on DiHadron rate due to satellite bunches
#     ds_fill = datetime.strptime(start_time, "%Y-%m-%d %H:%M:%S")
#     ds = ds_fill - timedelta(minutes=15)
#     de = ds_fill - timedelta(minutes=10)
#     s1 = svcget('TrgRate_L0DiHadronLowMult', ds=ds, de=de)
#     mdnfill_dihadron_lowmult_led =s1.y_median(n=0, nz=True)
#     if mdnfill_dihadron_lowmult_led is None:
#         mdnfill_dihadron_lowmult_led = 0.0

    # collections of run fors the givent fill
    sql = """SELECT id,
                    run_number,
                    mdnrun_physics_dead_time,
                    mdnrun_l0_physics_rate,
                    mdnrun_l0_physics_raw_rate,
                    mdnrun_l0du_electron_rate,
                    mdnrun_l0du_dimuon_rate,
                    mdnrun_l0du_hadron_rate,
                    mdnrun_l0du_muon_rate,
                    mdnrun_l0du_photon_rate
             FROM   measures_runs
             WHERE  id_measures_fills = ? """

    for row in db.execute(sql, (fill_id,)):
        data = {}
        run_id =  row["id"]

        data["id_measures_fills"] = fill_id
        data["id_measures_runs"] = run_id

        #.....................................................................
        #
        # Use values store in the database
        #
        # readout dead time
        x = row["mdnrun_l0_physics_rate"]
        y = row["mdnrun_l0_physics_raw_rate"]
        if x is None or y is None:
            corr = 1.
        else:
            corr = x / y

        # hadron rate
        value = row["mdnrun_l0du_hadron_rate"]
        if value is not None:
            data["mdnrun_l0_hadron"] = round((value - mdnfill_led_rate) * corr, 1)

        # other rate
        for el, key in LI_DB:
            value = row[el]
            if value is not None:
                data[key] = round(value * corr, 1)

#         #.....................................................................
#         #
#         # Use PVSS sensors for LowMult rates
#         #
#         run = row["run_number"]
#         ds_run = svcget("startTime", run=run).value()
#         de_run = svcget("endTime", run=run).value()
#
#         for name, key in LI_PVSS:
#             s1 = svcget(name, ds=ds_run, de=de_run)
#             value = s1.y_median(excludeZero=True, nz=True)
#             if value is not None and key == "mdnrun_l0_dihadron_lowmult":
#                 value = value - mdnfill_dihadron_lowmult_led
#                 if value > 0:
#                     data[key] = round(value * corr, 1)
#
#             elif value is not None:
#                 data[key] = round(value * corr, 1)

        # existing raw in measures_hlt1_l0rates
        sql = """SELECT id
                 FROM   measures_hlt1_l0rates
                 WHERE  id_measures_fills = ?
                 AND    id_measures_runs = ? """

        rep = db.execute(sql, (fill_id, run_id)).fetchone()
        if rep is not None:
            data["id"] = rep["id"]

        log("Inserting or updating %i..." % row["run_number"])

        insert_or_update(db, "measures_hlt1_l0rates", **data)

        # missing raw in measures_hlt1_rates
        sql = """SELECT id
                 FROM   measures_hlt1_rates
                 WHERE  id_measures_fills = ?
                 AND    id_measures_runs = ? """

        rep = db.execute(sql, (fill_id, run_id)).fetchone()
        if rep is None:
            data = {}
            data["id_measures_fills"] = fill_id
            data["id_measures_runs"] = run_id
            insert_or_update(db, "measures_hlt1_rates", **data)


if __name__ == "__main__":


    from argparse import ArgumentParser

    #.........................................................................
    #
    # user criteria
    #

    AGP = ArgumentParser()

    AGP.add_argument(
        "--db",
        default="triggerdb.sqlite",
        help="File name for the sqlite database [%(default)s].")

    AGP.add_argument(
        "-d", "--debug",
        action="store_true",
        help="Activate the debug mode [%default].")

    AGP.add_argument(
        "-f", "--fill",
        help="Select a fill number.",
        metavar="<number>",
        type=int)

    ARGS = AGP.parse_args()

    log("Start fixHlt1L0Rates.py")
    print_options(vars(ARGS))

    #.........................................................................
    #
    # data base connection and sensors
    #

    DB = create_db(ARGS.db)
    create_sql_functions(DB)

    SVC = create_sensors()

    #.........................................................................
    #
    # do the job and exit
    #

    fix(DB, SVC, ARGS)

    log("End fixHlt1L0Rates.py")
    sys.exit(0)

