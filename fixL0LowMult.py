#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" NAME
        fixL0LowMult -- fix L0 LowMult rates measured by HLT1

    SYNOPSIS
        fixL0LowMult [Options]

    DESCRIPTION
        In June 2016, add the HltRate_L0_LowMult sensors and a corresponding
        entry in the database.

        The script recover the history.

    OPTIONS

    EXAMPLES

        > SetupProject MooreOnline
        > ./fixL0LowMult.py

    AUTHOR
        R. Le Gac, legac@cppm.in2p3.fr

"""
import sys

from appFillDB import create_sensors, insert_or_update
from base import (create_db,
                  log,
                  print_options)

def fix(db, svc, opt):
    """Fill measures_hlt1_l0rates.mdnrun_l0_lowmult for 2016

    Args:
        db (sqlite3.connection): database connection
        svc (SensorSvc): sensors service
        opt (argparse.Namespace): user criteria

    """
    svcget = svc.get

    # existing runs
    sql = """SELECT id, id_measures_fills, run_number
             FROM measures_runs
             WHERE substr(start_time, 1, 4) = '2016'
    """

    for row in db.execute(sql):

        id_fill = row["id_measures_fills"]
        id_run = row["id"]
        run = row["run_number"]

        ds_run = svcget('startTime', run=run).value()
        de_run = svcget('endTime', run=run).value()

        s1 = svcget("HltRate_L0_LowMult", ds=ds_run, de=de_run)
        value = s1.y_median(n=2, excludeZero=True, nz=True)

        if value is None:
            continue

        sql = """SELECT id
                 FROM measures_hlt1_l0rates
                 WHERE id_measures_fills = ? and id_measures_runs = ? """

        row1 = db.execute(sql, (id_fill, id_run)).fetchone()
        if row1 is None:
            log("No entry in measures_hlt1_l0rates %si" % run)
            continue

        data = dict(id= row1["id"],
                    id_measures_runs=row["id"],
                    id_measures_fills=row["id_measures_fills"],
                    mdnrun_l0_lowmult=value)

        log("Update %i" % run)

        insert_or_update(db, "measures_hlt1_l0rates", **data)


if __name__ == "__main__":

    from argparse import ArgumentParser

    #.........................................................................
    #
    # user criteria
    #

    AGP = ArgumentParser()

    AGP.add_argument(
        "--db",
        default="triggerdb.sqlite",
        help="File name for the sqlite database [%(default)s].")

    ARGS = AGP.parse_args()

    log("Start fixL0LowMult.py")
    print_options(vars(ARGS))

    #.........................................................................
    #
    # data base connection and sensors
    #

    DB = create_db(ARGS.db)
    SVC = create_sensors()

    #.........................................................................
    #
    # do the job and exit
    #

    fix(DB, SVC, ARGS)

    log("End fixL0LowMult.py")
    sys.exit(0)

