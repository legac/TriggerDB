#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
	NAME
		appROOT -- run an ROOT interactive session

	SYNOPSIS
		appROOT [Options] fn

	DESCRIPTION
		run a ROOT interactive session on online monitoring savesets
		
	OPTIONS


	EXAMPLES

		> SetupGaudi
		> ./appROOT -t Moore -r 81430 -j GauchoJob

	AUTHOR
		R. Le Gac, legac@cppm.in2p3.fr

"""

__author__ = "legac@cppm.in2p3.fr"


import os
import sys

from optparse import OptionParser
from ROOT import *
from sensors import SensorSvc, SensorMnt


if __name__ == "__main__":

	#===============================================================================
	#
	#	Command line options
	#
	#===============================================================================
	parser = OptionParser()


	parser.add_option("-r", "--run",
					  action="store",
                      dest="run",
                      help="The run number.",
                      type="int")

	parser.add_option("-t", "--task",
                  dest="task",
                  help="The name of the monitoring task [%default].")

	parser.set_defaults(task='GauchoJob')
	
	(opt, args) = parser.parse_args()

	#===============================================================================
	#
	#	Build the ROOT file
	#
	#===============================================================================
	svc = SensorSvc()
	s1 = SensorMnt(svc, 'dummy', histopaths=['%s/foo' % opt.task])
	fn = s1._path_eor(0, opt.run)
	if not fn:
		print 'No file found'
		sys.exit(1)
	#===============================================================================
	#
	#	Start the ROOT browser and exit
	#
	#===============================================================================
	print 'monitoring file', fn
	f = TFile(fn)
	b = TBrowser()
	f.Browse(b)
	raw_input("Type CR to exit:")

	sys.exit(0)