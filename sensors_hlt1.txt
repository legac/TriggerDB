#
# list of sensors to check hlt1 rates
#
# From ODIN
#
NoBias_Accepted
AllODINtoHLT_Accepted
#
# From RUNCON/RUNDEF/RUNINF
#
TotalHLTRate Hlt1_rate_from_odin
#
# From Trend plot
#
RoutingBit32 Hlt1Global
RoutingBit33 Hlt1Lumi
RoutingBit34 ??
RoutingBit35 Hlt1BeamGas
RoutingBit37 !Hlt1BeamGas
RoutingBit38 Hlt1ODINTechnicalDecision
RoutingBit39 Hlt1L0
RoutingBit40 Hlt1(Velo|BeamGas)
RoutingBit41 Hlt1(Single|Track)Muon
RoutingBit42 Hlt1*Dimuon
RoutingBit43 Hlt1*MuonNoL0
RoutingBit44 Hlt1*Electron
RoutingBit45 Hlt1*Gamma
RoutingBit46 Hlt1Physics
RoutingBit47 Hlt1MBMicroBias
RoutingBit48 Hlt1MBNoBiasDecision
RoutingBit49 Hlt1*MVA
RoutingBit50 Hlt1LumiLowBeamCrossing
RoutingBit51 Hlt1LumiMidBeamCrossing
RoutingBit53 Hlt1Calib
RoutingBit54 Hlt1CalibRICH
RoutingBit55 Hlt1MBNoBiasRateLimited
RoutingBit56 Hlt1CalibMuonAlignJpsi
RoutingBit60 Hlt1TrackAllL0
