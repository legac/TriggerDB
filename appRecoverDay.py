#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    NAME
        appRecoverDay -- recover the measures_days table

    SYNOPSIS
        appRecoverDay [Options]

    DESCRIPTION
        Rebuild the measures_days table for a given year and month.

    OPTIONS


    EXAMPLES

        > lb-run MooreOnline/latest ./appRecoverDay -y 2017 -m 10

    AUTHOR
        R. Le Gac, legac@cppm.in2p3.fr

"""

__author__ = "legac@cppm.in2p3.fr"


import os
import sys

from appFillDB import create_sensors, insert_measures_days
from base import (create_db,
                  create_sql_functions,
                  log,
                  print_options)
from datetime import datetime, timedelta
from optparse import OptionParser
from pprint import pprint

#===============================================================================
#
# 	Command line options
#
#===============================================================================
parser = OptionParser()

parser.add_option("--db",
                  dest="dbname",
                  help="File name for the sqlite database [%default]."
                       "The special name ':memory:' create the database in RAM.")

parser.add_option("-d", "--debug",
                  action="store_true",
                  dest="debug",
                  help="Activate the debug mode [%default].")

parser.add_option("-m", "--month",
                  action="store",
                  dest="month",
                  help="Select the month [%default].",
                  metavar="MM",
                  type="int")

parser.add_option("-y", "--year",
                  action="store",
                  dest="year",
                  help="Select the year [%default].",
                  metavar="YYYY",
                  type="int")

parser.set_defaults(dbname='triggerdb.sqlite',
                    debug=False,
                    year=datetime.now().year)

(opt, args) = parser.parse_args()

log("Start appRecoverDay.py")

print_options(vars(opt))

#===============================================================================
#
# 	Database and sensors
#
#===============================================================================
db = create_db(opt.dbname)
create_sql_functions(db)
sensorSvc = create_sensors(debug=opt.debug)

#===============================================================================
#
# 	Process a list of fill belonging to the selected year/month
#
#===============================================================================
sql = """select fill_number,
                substr(start_time, 1, 4) as year,
                substr(start_time, 6, 2) as month,
                datetime(start_time, time_total || ' seconds') as end_time
         from   measures_fills
         where  year = '{0}'
             and (month = '{1}' or strftime('%m', end_time) = '{1}')
         order by fill_number"""

sql = sql.format(opt.year, opt.month)
fills = [row["fill_number"] for row in db.execute(sql)]

insert_measures_days(db, sensorSvc, opt.year, fills)

#===============================================================================
#
# 	Exit
#
#===============================================================================
log("End appRecoverDay.py")
sys.exit(0)
