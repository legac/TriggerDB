#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    NAME
        appSensor -- dump data from a sensor

    SYNOPSIS
        appSensor [Options]

    DESCRIPTION
        Helper script to see data from a sensor for a
        dedicated run or fill.

    OPTIONS


    EXAMPLES

        > lb-run MooreOnline/latest ./appSensorPVSS -l pvss
        > lb-run MooreOnline/latest ./appSensorPVSS -r 81431 -s Mu

    AUTHOR
        R. Le Gac, legac@cppm.in2p3.fr

"""
import csv
import os
import sys


from appFillDB import create_sensors
from argparse import ArgumentParser, FileType
from datetime import datetime, timedelta
from pprint import pprint
from sensors import (SensorMix,
                     SensorMnt,
                     SensorPvss,
                     SensorRdbF,
                     SensorRdbR,
                     SensorTrend)

SENSOR_MAP = {'mix': SensorMix,
              'mnt': SensorMnt,
              'pvss': SensorPvss,
              'rdbf': SensorRdbF,
              'rdbr': SensorRdbR,
              'trend': SensorTrend}


def dump_sensors(svc, opt):
    """Dump time and value for a list of sensors

    """
    data = []
    header = []

    for sensor_name, comment in IterSensors(opt.scan):
        opt.sensor = sensor_name
        sensor = read_sensor(svc, opt)

        header.extend(["date_%s" % sensor_name, sensor_name])

        # keep only PVSS and Trend sensor
        cls = sensor.__class__
        if cls not in (SensorPvss, SensorTrend):
            continue

        # size of the data block
        ndata = len(data)
        nx = len(sensor.x)

        # size of the existing row
        nrow = 0
        if data:
            nrow = len(data[0])

        # extend existing row
        [data[i].extend([None] * 2) for i in xrange(ndata)]

        # additional row
        if nx > ndata:
            nrow = nrow + 2
            [data.append([None] * nrow) for i in xrange(nx - ndata)]

        # copy the data
        for i in xrange(nx):
            data[i][-2] = str(sensor.x[i])
            data[i][-1] = sensor.y[i]

    data.insert(0, header)

    # write the CSV file
    title = os.path.splitext(opt.scan.name)[0]
    if opt.fill:
        fname = "%s_fill_%s.csv" % (title, opt.fill)

    elif opt.runs:
        fname = "%s_runs_%s.csv" % (title, opt.runs)

    elif opt.run:
        fname = "%s_run_%s.csv" % (title, opt.run)

    fout = open(fname, 'wb')
    writer = csv.writer(fout, quoting=csv.QUOTE_NONNUMERIC)
    writer.writerows(data)

    print "CSV file", fname, "written."


class IterSensors(object):
    """Iterate on a list of sensor define in txt file.
    Line starting with a # are ignored.
    Information after a space are ignored

    """
    def __init__(self, file):
        self._file = file

    def __iter__(self):
        return self

    def next(self):
        line = self._file.readline()

        if len(line) == 0:
            raise StopIteration

        if line.startswith("#"):
            return self.next()

        # remove end of line
        line = line.replace("\n", "")

        # value, comment
        values = line.split(" ")

        if len(values) == 1:
            values.append("")

        return values


def list_sensors(sensor_svc, opt):
    """List the sensors

    """
    svc = sensor_svc

    if opt.list == 'all':
        sensors = svc.keys()

    else:
        cls = SENSOR_MAP[opt.list]
        sensors = []
        for sensor_name in svc.iterkeys():
            if svc.get(sensor_name).__class__ == cls:
                sensors.append(sensor_name)

    sensors.sort()
    pprint(sensors)


def print_stat(sensor, opt):
    """Print some statitic for a read sensor.

    """

    # compute the median value
    is_mean = 'y_mean' in dir(sensor)
    is_median = 'y_median' in dir(sensor)

    if is_mean:
        print "mean  :", sensor.y_mean()

    if is_median:
        print "median:", sensor.y_median()

    if is_mean or is_median:
        print "-" * 80


def read_sensor(sensor_svc, opt):
    """Read the sensor value. It depends on the sensor type.

    Returns: the sensor

    """
    svc = sensor_svc
    sensor = svc.get(opt.sensor)

    cls = sensor.__class__
    fill = opt.fill
    run = opt.run
    runs = opt.runs

    # some sensors requires start /end date
    if cls in (SensorMix, SensorPvss, SensorTrend):
        if run:
            ds = svc.get('startTime', run=run).value()
            de = svc.get('endTime', run=run).value()

        elif runs:
            (run_start, run_end) = runs.split('-')
            ds = svc.get('startTime', run=int(run_start)).value()
            de = svc.get('endTime', run=int(run_end)).value()

        elif fill:
            time_total = svc.get('time_total', fill=fill).value()
            ds = svc.get('fillStartTime', fill=fill).value()
            de = ds + timedelta(seconds=time_total)

    # define the parameter to read the data
    kwargs = {}
    if cls in (SensorMnt, SensorRdbR):
        if not run:
            print "The --run option has to be defined."
        kwargs['run'] = run

    elif cls == SensorRdbF:
        if not fill:
            print "The --fill option has to be defined."
        kwargs['fill'] = fill

    elif cls in (SensorPvss, SensorTrend):
        kwargs['ds'] = ds
        kwargs['de'] = de

    elif cls == SensorMix:
        kwargs['ds'] = ds
        kwargs['de'] = de
        if run:
            kwargs['run'] = run
        elif fill:
            kwargs['fill'] = fill

    # read the data and display them on the console
    if opt.verbose:
        sensor.debug = True

    sensor.read(**kwargs)

    return sensor


def scan_sensors(svc, opt):
    """Scan a list of sensors defined in a text file.

    One sensor name per line.
    Line starting by a # are ignored (comment)

    """
    li = [('Sensor', 'median / value', '')]
    for sensor_name, comment in IterSensors(opt.scan):

        opt.sensor = sensor_name
        try:
            sensor = read_sensor(svc, opt)

            cls = sensor.__class__
            if cls in (SensorMnt, SensorPvss, SensorTrend):
                li.append((sensor_name, sensor.y_median(), comment))

            elif cls in (SensorRdbF, SensorRdbR):
                li.append((sensor_name, sensor.value(), comment))

            else:
                li.append((sensor_name, None, comment))

        except:
            print "skip", sensor_name
            continue

    # pretty printing
    print '-' * 80
    for t in li:
        if t[1] is None:
            print "%25s: %s %s" % (t[0], ' ' * 11, t[2])

        elif isinstance(t[1], (str, unicode)):
            try:
                val = float(t[1])
                print "%25s: % 12.2f %s" % (t[0], val, t[2])
            except:
                print "%25s: %9s %s" % t

        elif isinstance(t[1], datetime):
            print "%25s: %s %s" % (t[0], t[1].strftime("%d %b %Y %H:%M:%S"), t[2])

        elif isinstance(t[1], int):
            print "%25s: % 9i %s" % t

        else:
            print "%25s: % 12.2f %s" % t
    print '-' * 80

if __name__ == "__main__":

    # Command line options
    PARSER = ArgumentParser()

    PARSER.add_argument("-D", "--dump",
                        action="store_true",
                        help="To be used with the --scan option. "
                             "Dump the sensors time and value in a CSV file.")

    PARSER.add_argument("-f", "--fill",
                        type=int,
                        help="the fill number.",
                        metavar="<number>")

    sensor_types = ['all', 'mix', 'mnt', 'pvss', 'rdbf', 'rdbr', 'trend']
    PARSER.add_argument("-l", "--list",
                        choices=sensor_types,
                        help="list sensors of a given type.")

    PARSER.add_argument("-r", "--run",
                        type=int,
                        help="the run number.",
                        metavar="<number>")

    PARSER.add_argument("-R", "--runs",
                        help="A set of consecutive runs, e.g 154860-154888",
                        metavar="<number-number>")

    PARSER.add_argument("-s", "--sensor",
                        help="the name of the sensor.",
                        metavar="<name>")

    PARSER.add_argument("-S", "--scan",
                        help="scan a list of sensor define in the text file "
                             "given in the argument. ",
                        metavar="<file>",
                        type=FileType('r'))

    PARSER.add_argument("-v", "--verbose",
                        action="store_true",
                        help="Show the data points, time and value.")

    ARGS = PARSER.parse_args()

    # initialise all sensors
    SVC = create_sensors()

    # list of all sensors
    if ARGS.list:
        list_sensors(SVC, ARGS)
        sys.exit(0)

    # scan a list of sensor
    if ARGS.scan and not ARGS.dump:
        scan_sensors(SVC, ARGS)

    # dump a list of sensor
    if ARGS.scan and ARGS.dump:
        dump_sensors(SVC, ARGS)

    # read the selected sensor
    else:
        SENSOR = read_sensor(SVC, ARGS)
        print_stat(SENSOR, ARGS)

    # Exit
    sys.exit(0)
