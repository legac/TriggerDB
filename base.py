# -*- coding: utf-8 -*-
""" base module contains a collections of useful functions

"""
__author__ = "legac@cppm.in2p3.fr"

import math
import sqlite3
import sys

from datetime import datetime


INHIBIT_FLAG = '0001-01-01 01:01:01'


def create_db(dbname=':memory:'):
    """ Create the local sqlite database and return a connection.

    """
    sql = """

    CREATE TABLE IF NOT EXISTS axis_times (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        year INTEGER NOT NULL,
        month INTEGER NOT NULL,
        week INTEGER NOT NULL,
        day INTEGER NOT NULL,
        hour INTEGER NOT NULL
    );

    CREATE TABLE IF NOT EXISTS axis_triggers (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        tck INTEGER NOT NULL UNIQUE,
        l0 INTEGER NOT NULL,
        hlt INTEGER NOT NULL,
        moore_version VARCHAR NOT NULL,
        nickname VARCHAR NOT NULL
    );

    CREATE TABLE IF NOT EXISTS measures_days (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        year INTEGER NOT NULL,
        month INTEGER NOT NULL,
        week INTEGER NOT NULL,
        day INTEGER NOT NULL,
        delivered_lumi DECIMAL NOT NULL,
        recorded_lumi DECIMAL NOT NULL,
        time_total INTEGER NOT NULL,
        UNIQUE (year, day)
    );

    CREATE TABLE IF NOT EXISTS measures_fills (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        fill_number INTEGER NOT NULL UNIQUE,
        crossing_bunches INTEGER DEFAULT NULL,
        start_time VARCHAR NOT NULL,
        time_total DECIMAL DEFAULT NULL,
        time_hvon DECIMAL DEFAULT NULL,
        time_veloin DECIMAL DEFAULT NULL,
        time_running DECIMAL DEFAULT NULL,
        time_logged DECIMAL DEFAULT NULL,
        lumi_total DECIMAL DEFAULT NULL,
        lumi_hvon DECIMAL DEFAULT NULL,
        lumi_veloin DECIMAL DEFAULT NULL,
        lumi_running DECIMAL DEFAULT NULL,
        lumi_logged DECIMAL DEFAULT NULL,
        mdnfill_lumi DECIMAL DEFAULT NULL,
        mdnfill_mu DECIMAL DEFAULT NULL,
        mdnfill_nobias_rate DECIMAL DEFAULT NULL,
        mdnfill_bb_rate DECIMAL DEFAULT NULL,
        mdnfill_l0_physics_rate DECIMAL DEFAULT NULL,
        mdnfill_l0_physics_raw_rate DECIMAL DEFAULT NULL,
        mdnfill_l0du_electron_rate DECIMAL DEFAULT NULL,
        mdnfill_l0du_dimuon_rate DECIMAL DEFAULT NULL,
        mdnfill_l0du_hadron_rate DECIMAL DEFAULT NULL,
        mdnfill_l0du_muon_rate DECIMAL DEFAULT NULL,
        mdnfill_l0du_photon_rate DECIMAL DEFAULT NULL,
        mdnfill_led_rate DECIMAL DEFAULT NULL,
        mdnfill_total_hlt_physics_rate DECIMAL DEFAULT NULL,
        mdnfill_total_hlt_rate DECIMAL DEFAULT NULL,
        mdnfill_hlt_deferred DECIMAL DEFAULT NULL,
        mdnfill_physics_dead_time DECIMAL DEFAULT NULL
    );

    CREATE TABLE IF NOT EXISTS measures_runs (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        id_axis_times INTEGER NOT NULL,
        id_axis_triggers INTEGER NOT NULL,
        id_measures_fills INTEGER NOT NULL,
        id_measures_triggers INTEGER NOT NULL,
        run_number INTEGER NOT NULL UNIQUE,
        start_time VARCHAR NOT NULL,
        end_time VARCHAR NOT NULL,
        is_collision_alldetector_veloclosed INTEGER NOT NULL DEFAULT 0,
        run_duration INTEGER NOT NULL DEFAULT NULL,
        integrated_lumi DECIMAL DEFAULT NULL,
        magnet_polarity VARCHAR NOT NULL,
        beta_star DECIMAL DEFAULT NULL,
        avg_evt_size DECIMAL DEFAULT NULL,
        mdnrun_nobias_rate DECIMAL DEFAULT NULL,
        mdnrun_bb_rate DECIMAL DEFAULT NULL,
        mdnrun_l0_physics_rate DECIMAL DEFAULT NULL,
        mdnrun_l0_physics_raw_rate DECIMAL DEFAULT NULL,
        mdnrun_total_hlt_physics_rate DECIMAL DEFAULT NULL,
        mdnrun_total_hlt_rate DECIMAL DEFAULT NULL,
        mdnrun_lumi DECIMAL DEFAULT NULL,
        mdnrun_mu DECIMAL DEFAULT NULL,
        mdnrun_physics_dead_time DECIMAL DEFAULT NULL,
        mdnrun_hlt_time DECIMAL DEFAULT NULL,
        mdnrun_l0du_electron_rate DECIMAL DEFAULT NULL,
        mdnrun_l0du_dimuon_rate DECIMAL DEFAULT NULL,
        mdnrun_l0du_hadron_rate DECIMAL DEFAULT NULL,
        mdnrun_l0du_muon_rate DECIMAL DEFAULT NULL,
        mdnrun_l0du_photon_rate DECIMAL DEFAULT NULL,
        mdnrun_hlt_l0_hadron_rate DECIMAL DEFAULT NULL,
        mdnrun_hlt_l0_muon_dimuon_rate DECIMAL DEFAULT NULL,
        mdnrun_hlt_exclusive_rate DECIMAL DEFAULT NULL,
        mdnrun_hlt_hadron_rate DECIMAL DEFAULT NULL,
        mdnrun_hlt_muon_rate DECIMAL DEFAULT NULL,
        mdnrun_hlt_lumi_rate DECIMAL DEFAULT NULL,
        mdnrun_hlt_beamgas_rate DECIMAL DEFAULT NULL,
        mdnrun_hlt_l0any_rate DECIMAL DEFAULT NULL,
        mdnrun_hlt_microbias_rate DECIMAL DEFAULT NULL,
        mdnrun_hlt_nobias_rate DECIMAL DEFAULT NULL,
        mdnrun_hlt_singlemuon_rate DECIMAL DEFAULT NULL,
        mdnrun_hlt_dimuon_rate DECIMAL DEFAULT NULL,
        mdnrun_hlt_charm_rate DECIMAL DEFAULT NULL,
        mdnrun_hlt_inclusivephi_rate DECIMAL DEFAULT NULL,
        mdnrun_hlt_topo234body_rate DECIMAL DEFAULT NULL,
        mdnrun_jpsi_rate DECIMAL DEFAULT NULL,
        mdnrun_inclusivephi_rate DECIMAL DEFAULT NULL,
        mdnrun_d0kpi_rate DECIMAL DEFAULT NULL,
        mdnrun_dhhh_rate DECIMAL DEFAULT NULL,
        mdnrun_hlt_deferred DECIMAL DEFAULT NULL
    );

    CREATE TABLE IF NOT EXISTS measures_triggers (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        bb_rate_at_peak DECIMAL DEFAULT NULL,
        l0_rate_at_peak DECIMAL DECIMAL DEFAULT NULL,
        hlt1_physics_rate_at_peak DECIMAL DEFAULT NULL,
        hlt2_rate_at_peak DECIMAL DEFAULT NULL,
        hlt2_physics_rate_at_peak DECIMAL DEFAULT NULL,
        lumi_at_peak DECIMAL DEFAULT NULL,
        mu_at_peak DECIMAL DEFAULT NULL,
        live_time_at_peak DECIMAL DEFAULT NULL,
        avg_hlt_time_at_peak DECIMAL DEFAULT NULL
    );

    CREATE TABLE IF NOT EXISTS scans (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        year INTEGER NOT NULL,
        last_fill INTEGER NOT NULL DEFAULT 0,
        last_run INTEGER NOT NULL DEFAULT 0,
        status VARCHAR
    );

    CREATE TABLE IF NOT EXISTS measures_routingbits (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        id_measures_fills INTEGER NOT NULL,
        id_measures_runs INTEGER NOT NULL,
        %s
    );

    CREATE TABLE IF NOT EXISTS measures_streams (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        id_measures_fills INTEGER NOT NULL,
        id_measures_runs INTEGER NOT NULL,
        mdnrun_beamgas_events DECIMAL DEFAULT NULL,
        mdnrun_beamgas_physstat DECIMAL DEFAULT NULL,
        mdnrun_calib_events DECIMAL DEFAULT NULL,
        mdnrun_calib_physstat DECIMAL DEFAULT NULL,
        mdnrun_express_events DECIMAL DEFAULT NULL,
        mdnrun_express_physstat DECIMAL DEFAULT NULL,
        mdnrun_full_events DECIMAL DEFAULT NULL,
        mdnrun_full_physstat DECIMAL DEFAULT NULL,
        mdnrun_lumi_events DECIMAL DEFAULT NULL,
        mdnrun_lumi_physstat DECIMAL DEFAULT NULL,
        mdnrun_turbo_events DECIMAL DEFAULT NULL,
        mdnrun_turbo_physstat DECIMAL DEFAULT NULL,
        mdnrun_turcal_events DECIMAL DEFAULT NULL,
        mdnrun_turcal_physstat DECIMAL DEFAULT NULL
    );

    CREATE TABLE IF NOT EXISTS measures_hlt1_l0rates (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        id_measures_fills INTEGER NOT NULL,
        id_measures_runs INTEGER NOT NULL,
        mdnrun_l0_b1gas DECIMAL DEFAULT NULL,
        mdnrun_l0_b2gas DECIMAL DEFAULT NULL,
        mdnrun_l0_calo DECIMAL DEFAULT NULL,
        mdnrun_l0_diem_lowmult DECIMAL DEFAULT NULL,
        mdnrun_l0_dihadron_lowmult DECIMAL DEFAULT NULL,
        mdnrun_l0_dimuon_lowmult DECIMAL DEFAULT NULL,
        mdnrun_l0_dimuon DECIMAL DEFAULT NULL,
        mdnrun_l0_electron_lowmult DECIMAL DEFAULT NULL,
        mdnrun_l0_electron DECIMAL DEFAULT NULL,
        mdnrun_l0_hadron DECIMAL DEFAULT NULL,
        mdnrun_l0_jetel DECIMAL DEFAULT NULL,
        mdnrun_l0_jetph DECIMAL DEFAULT NULL,
        mdnrun_l0_lowmult DECIMAL DEFAULT NULL,
        mdnrun_l0_muon_lowmult DECIMAL DEFAULT NULL,
        mdnrun_l0_muon DECIMAL DEFAULT NULL,
        mdnrun_l0_muonew DECIMAL DEFAULT NULL,
        mdnrun_l0_muonnospd DECIMAL DEFAULT NULL,
        mdnrun_l0_nopvflag DECIMAL DEFAULT NULL,
        mdnrun_l0_photon_lowmult DECIMAL DEFAULT NULL,
        mdnrun_l0_photon DECIMAL DEFAULT NULL,
        mdnrun_l0_spd DECIMAL DEFAULT NULL,
        mdnrun_l0_pu DECIMAL DEFAULT NULL,
        mdnrun_l0_spdlowmult DECIMAL DEFAULT NULL,
        mdnrun_l0_globalpi0 DECIMAL DEFAULT NULL,
        mdnrun_l0_hcal DECIMAL DEFAULT NULL,
        mdnrun_l0_localpi0 DECIMAL DEFAULT NULL,
        mdnrun_l0_muon_minbias DECIMAL DEFAULT NULL,
        mdnrun_l0_pu20 DECIMAL DEFAULT NULL,
        mdnrun_l0_spd40 DECIMAL DEFAULT NULL
    );

    CREATE TABLE IF NOT EXISTS measures_hlt1_rates (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        id_measures_fills INTEGER NOT NULL,
        id_measures_runs INTEGER NOT NULL,
        mdnrun_hlt1commissioning_lines DECIMAL DEFAULT NULL,
        mdnrun_hlt1dimuon_lines DECIMAL DEFAULT NULL,
        mdnrun_hlt1global_lines DECIMAL DEFAULT NULL,
        mdnrun_hlt1l0_lines DECIMAL DEFAULT NULL,
        mdnrun_hlt1lumibeamgas_lines DECIMAL DEFAULT NULL,
        mdnrun_hlt1minbias_lines DECIMAL DEFAULT NULL,
        mdnrun_hlt1other_lines DECIMAL DEFAULT NULL,
        mdnrun_hlt1singlemuon_lines DECIMAL DEFAULT NULL,
        mdnrun_hlt1trackalll0_lines DECIMAL DEFAULT NULL,
        mdnrun_hlt1ecal_lines DECIMAL DEFAULT NULL,
        mdnrun_hlt1calibration_lines DECIMAL DEFAULT NULL,
        mdnrun_hlt1beauty_lines DECIMAL DEFAULT NULL,
        mdnrun_hlt1cep_lines DECIMAL DEFAULT NULL,
        mdnrun_hlt1trackmva_lines DECIMAL DEFAULT NULL,
        mdnrun_hlt1lowmult_lines DECIMAL DEFAULT NULL,
        mdnrun_hltrate_hlt1_dimuon DECIMAL DEFAULT NULL,
        mdnrun_hltrate_hlt1_electron DECIMAL DEFAULT NULL,
        mdnrun_hltrate_hlt1_gamma DECIMAL DEFAULT NULL,
        mdnrun_hltrate_hlt1_global DECIMAL DEFAULT NULL,
        mdnrun_hltrate_hlt1_lumilow DECIMAL DEFAULT NULL,
        mdnrun_hltrate_hlt1_lumimid DECIMAL DEFAULT NULL,
        mdnrun_hltrate_hlt1_mva DECIMAL DEFAULT NULL,
        mdnrun_hltrate_hlt1_microbias DECIMAL DEFAULT NULL,
        mdnrun_hltrate_hlt1_muonnol0 DECIMAL DEFAULT NULL,
        mdnrun_hltrate_hlt1_odintechnical DECIMAL DEFAULT NULL,
        mdnrun_hltrate_hlt1_singlemuon DECIMAL DEFAULT NULL,
        mdnrun_hltrate_hlt1_beamgas DECIMAL DEFAULT NULL,
        mdnrun_hltrate_hlt1_beauty DECIMAL DEFAULT NULL,
        mdnrun_hltrate_hlt1_bottomonium DECIMAL DEFAULT NULL,
        mdnrun_hltrate_hlt1_calib DECIMAL DEFAULT NULL,
        mdnrun_hltrate_hlt1_calibtracking DECIMAL DEFAULT NULL,
        mdnrun_hltrate_hlt1_diproton DECIMAL DEFAULT NULL,
        mdnrun_hltrate_hlt1_l0 DECIMAL DEFAULT NULL,
        mdnrun_hltrate_hlt1_lowmult DECIMAL DEFAULT NULL,
        mdnrun_hltrate_hlt1_lumi DECIMAL DEFAULT NULL,
        mdnrun_hltrate_hlt1_mvaloose DECIMAL DEFAULT NULL,
        mdnrun_hltrate_hlt1_multi DECIMAL DEFAULT NULL,
        mdnrun_hltrate_hlt1_nobias DECIMAL DEFAULT NULL,
        mdnrun_hltrate_hlt1_nonphysics DECIMAL DEFAULT NULL,
        mdnrun_hltrate_hlt1_other DECIMAL DEFAULT NULL,
        mdnrun_hltrate_hlt1_phi DECIMAL DEFAULT NULL,
        mdnrun_hltrate_hlt1_rich DECIMAL DEFAULT NULL,
        mdnrun_hltrate_hlt1_veloclosing DECIMAL DEFAULT NULL,
        mdnrun_hlt1beamgas_lines DECIMAL DEFAULT NULL,
        mdnrun_hlt1calib_lines DECIMAL DEFAULT NULL,
        mdnrun_hlt1calibtracking_lines DECIMAL DEFAULT NULL,
        mdnrun_hlt1lumi_lines DECIMAL DEFAULT NULL,
        mdnrun_hlt1veloclosing_lines DECIMAL DEFAULT NULL,
        mdnrun_hlt1bottomonium_lines DECIMAL DEFAULT NULL,
        mdnrun_hlt1diproton_lines DECIMAL DEFAULT NULL,
        mdnrun_hlt1electron_lines DECIMAL DEFAULT NULL,
        mdnrun_hlt1mva_lines DECIMAL DEFAULT NULL,
        mdnrun_hlt1mvaloose_lines DECIMAL DEFAULT NULL,
        mdnrun_hlt1microbias_lines DECIMAL DEFAULT NULL,
        mdnrun_hlt1multi_lines DECIMAL DEFAULT NULL,
        mdnrun_hlt1nobias_lines DECIMAL DEFAULT NULL,
        mdnrun_hlt1nonphysics_lines DECIMAL DEFAULT NULL,
        mdnrun_hlt1phi_lines DECIMAL DEFAULT NULL);

    CREATE TABLE IF NOT EXISTS measures_hlt2_rates (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        id_measures_fills INTEGER NOT NULL,
        id_measures_runs INTEGER NOT NULL,
        mdnrun_hlt2_charmhad DECIMAL DEFAULT NULL,
        mdnrun_hlt2_dimuon DECIMAL DEFAULT NULL,
        mdnrun_hlt2_ew DECIMAL DEFAULT NULL,
        mdnrun_hlt2_lowmult DECIMAL DEFAULT NULL,
        mdnrun_hlt2_singlemuon DECIMAL DEFAULT NULL,
        mdnrun_hlt2_topo DECIMAL DEFAULT NULL);

    """
    log('Connect to database: %s' % dbname)

    # add the list of routing bits
    li = ["\tmdnrun_rb_%02i DECIMAL DEFAULT NULL" % i for i in xrange(92)]
    sql = sql % ',\n'.join(li)

    # Connection to the datab_se and create the table
    # setup the auto commit mode
    db = sqlite3.connect(dbname)
    db.isolation_level = None
    db.row_factory = sqlite3.Row
    db.executescript(sql)

    # Initialise the scans table
    # Create a dummy entry for the current year
    year = datetime.now().year
    rep = db.execute('SELECT * FROM scans WHERE year=?', (year,))
    if not rep.fetchone():
        sql = """INSERT INTO scans (year,
                                    last_fill,
                                    last_run,
                                    status) VALUES (?, 0, 0,?)"""
        db.execute(sql, (year, None))

    # return the connection
    return db


def create_sql_functions(db):
    """Create a set of sql functions defining units.

    """
    db.create_aggregate("median", 2, FillMedianFromRun)

    db.create_function("tck2s", 1, lambda i: "0x%X" % int(i))
    db.create_function("zip_tcks", 1, zip_tcks)

    # function to define units
    db.create_function("ms", 0, lambda: 1000.)
    db.create_function("s", 0, lambda: 1.)
    db.create_function("m", 0, lambda: 1. / 60.)
    db.create_function("h", 0, lambda: 1. / 3600.)

    db.create_function("Byte", 0, lambda: 1.)
    db.create_function("kB", 0, lambda: 0.001)

    db.create_function("Hz", 0, lambda: 1.)
    db.create_function("kHz", 0, lambda: 0.001)
    db.create_function("MHz", 0, lambda: 0.000001)

    db.create_function("ub", 0, lambda: 1.)
    db.create_function("nb", 0, lambda: 0.001)
    db.create_function("pb", 0, lambda: 0.000001)
    db.create_function("fb", 0, lambda: 0.000000001)

    db.create_function("pubs", 0, lambda: 1.)
    db.create_function("pcm2s", 0, lambda: 1.E30)

    db.create_function("pct", 0, lambda: 0.01)

    db.create_function("corr_l0hadron", 2, corr_l0hadron)
    db.create_function("norm_rate", 3, norm_rate)
    db.create_function("pileup", 1, pileup)
    db.create_function("fixL0", 3, fixL0)

    # function to include deferred rate
    func = \
    lambda x: (1. / (1. - x * 0.01) if x is not None and 0. <= x < 100. else 1.)
    db.create_function("scale", 1, func)


def corr_l0hadron(hadron, led):
    """Correct the L0du hadron rate by subtracting the LED rate.

    """
    if hadron is None:
        return 0.

    # RUN2, fill 4246, at that time the LED rate is ~44.2 kHz
    elif led is None:
        return hadron - 44200.

    else:
        return hadron - led


def db_name(opt):
    """Helper function returning the file name for the sqlite database.
    It depends on the selected options.

    Args:
        opt (argparse.Namespace): the argument db contains the name of the
            database and runs a possible list of run to to process.

    """
    if opt.db == ':memory:' or opt.runs is not None:
        return ':memory:'

    if opt.db.endswith('.sqlite'):
        return opt.db

    return '%s.sqlite' % opt.db


class FillMedianFromRun(object):
    """Compute the median for a fill using median for a run.

    """
    def __init__(self):
        self.li = []

    def step(self, value, run_duration):
        if value is not None and run_duration is not None and value != '':
            values = [value] * int(run_duration)
            self.li.extend(values)

    def finalize(self):
        if self.li:
            return median(self.li)

        return None


def fixL0(fill_number, l0raw, l0muon):
    """Beginning of 2015, the running condition was very peculiar.
    The nobias rate is set to selects almost all the bb crossing.
    In that scenario, the L0 rate (determined by ODIN) is very low since
    all bb crossing are selected by the nobias trigger.

    This function is a tentative to estimate properly the L0 rate.

    """
    if 3819 <= fill_number < 3971:
        return l0muon

    else:
        return l0raw


def is_inhibit(db, year):
    """Return True if the database is inhibit, False otherwise.

    """
    rep = db.execute('SELECT status FROM scans WHERE year=?', (year,))
    row = rep.fetchone()

    if row:
        return row['status'] == INHIBIT_FLAG

    else:
        return False


def is_lock(db, year):
    """Return True if the database is locked by another process,
    False otherwise.

    """
    rep = db.execute('SELECT status FROM scans WHERE year=?', (year,))
    row = rep.fetchone()

    if row:
        return row['status'] is not None

    else:
        return False


def lock(db, year, nhours=3):
    """Lock the database for the current process.

    If the database is already lock by another process exit.
    If the lock was setup nhours ago it is reset and the database is
    lock for the current process.

    All these mechanism have been setup to avoid race condition between
    several process filling the database at the same time. However, they
    also have been design to avoid to be stuck for ever.

    """
    fmt = '%Y-%m-%d %H:%M:%S'

    now = datetime.now()
    status = now.strftime(fmt)

    rep = db.execute('SELECT status FROM scans WHERE year=?', (year,))
    row = rep.fetchone()

    # no entry in the table scans
    if not row:
        sql = """INSERT INTO scans (year,
                                    last_fill,
                                    last_run,
                                    status) VALUES (?, 0, 0,?)"""
        db.execute(sql, (year, status))
        return

    # the database is already locked
    if row["status"]:
        date_lock = datetime.strptime(row["status"], fmt)
        delta = now - date_lock
        if delta.seconds < 3600. * nhours:
            log('The database is locked by another appFillDB script. Exit')
            sys.exit(0)

    # lock the database
    sql = 'UPDATE scans SET status=? WHERE year=?'
    db.execute(sql, (status, year))


def log(msg):
    print datetime.now().strftime('%Y-%m-%d %H:%M:%S'), msg


def median(values):
    """Helper function returning the median of the list values.

    If a < b < c, then the median of the list {a, b, c} is b.
    If a < b < c < d, then the median of the list {a, b, c, d}
    is the mean of b and c = (b + c)/2.

    """
    li = list(values)
    li.sort()
    i = len(li) / 2

    if not li:
        return None

    if len(li) % 2 == 1:
        res = li[i]

    else:
        res = (li[i - 1] + li[i]) / 2.

    return res


def norm_rate(rate, nbb, mu):
    """compute the normalised rate using the exact formula.
    This is the rate generated by one pp interaction
    rate in Hz, nbb number of crossing bunches

    """
    if not (rate and nbb and mu):
        return None

#    x = rate / (nbb * mu)
    freq = 1. / (3564 * 25E-09)
    try:
        x = -math.log(1. - (rate / (nbb * freq))) * (freq / mu)
        return x
    except (ArithmeticError, ValueError):
        return None


def pileup(mu):
    """Number of pp interactions in visible event.

    """
    if not isinstance(mu, (float, int)):
        return None
    return mu / (1. - math.exp(-mu))


def print_options(opt):
    s = str(opt)
    li = s.replace('{', '').replace('}', '').replace("'", '').split(',')

    ip = 0
    for el in li:
        if el.find(':') > ip:
            ip = el.find(':')

    for el in li:
        nb = ip - el.find(':')
        print "\t%s%s" % (' ' * nb, el)


def get_beam_energy(opt):
    """Helper function returning the beam energy depending on the year.

    """
    if opt.year <= 2009:
        value = 450.0

    elif 2010 <= opt.year < 2012:
        value = 3500.0

    elif opt.year <= 2013:
        value = 4000.0

    elif opt.year >= 2015:
        value = 6500.0

    return value


def unlock(db, year):
    """Unlock the database.

    """
    sql = 'UPDATE scans SET status=? WHERE year=?'

    try:
        db.execute(sql, (None, year))

    # The database is lock at a very low level which might be due
    # a crash of a previous process or by another process
    #
    # NOTE: to recover this problem you can do the following
    # 1) Check that another process is using the db
    #    /sbin/fuser -a trigger_3500.sqlite
    #    If there is a process kill it
    #
    # 2) mv trigger_3500.sqlite foo.db
    #    cp foo.db trigger_3500.sqlite
    #    rm foo.db
    #
    # 3) echo ".dump" | sqlite old.db | sqlite new.db
    #
    except sqlite3.OperationalError, e:
        log('*' * 80)
        log(e)
        log('')
        log('This error required manual intervention:')
        log('  1) /sbin/fuser -a trigger_3500.sqlite')
        log('     kill process if any')
        log('')
        log('  2) mv trigger_3500.sqlite foo.db')
        log('     cp foo.db trigger_3500.sqlite')
        log('')
        log('  3) echo ".dump" | sqlite old.db | sqlite new.db')
        log('')
        log('*' * 80)
        log("End appFillDB.py")
        db.close()
        sys.exit(1)


def zip_tcks(s):
    """zip the tcks string return by the group_concat function.

    """
    li = []
    for el in s.split(','):
        if el not in li:
            li.append(el)

    return ', '.join(li)
