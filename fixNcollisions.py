#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" NAME
        fixNcollisions -- fix the number of colliding bunches at IP8

    SYNOPSIS
        fixNcollisions [Options]

    DESCRIPTION
        In Sep 2017, it was realized that the number of colliding bunches
        is under estimated, i.e for the fill 6169 it is 1424 instead of 1524.

        The script update the number of collisions for the year 2017 using
        the new algorithm.

    OPTIONS
            --version
                Return the version number of this scripts

            --fill
                fill number

    EXAMPLES

        > SetupProject MooreOnline
        > lb-run MooreOnline/latest ./fixNcollisions.py

    AUTHOR
        R. Le Gac, legac@cppm.in2p3.fr

"""
import sys


from appFillDB import create_sensors, insert_or_update
from base import (create_db,
                  log,
                  print_options)
from datetime import timedelta


def fix(db, svc, opt):
    """Fix the crossing_bunches for each fill recorded in 2017.

    Args:
        db (sqlite3.connection): database connection
        svc (SensorSvc): sensors service
        opt (argparse.Namespace): user criteria

    """
    svcget = svc.get

    # scan the list of fills for 2017
    sql = """SELECT id, fill_number, crossing_bunches
             FROM measures_fills
             WHERE substr(start_time, 1, 4) = '2017'
    """

    for row in db.execute(sql):

        fill_number = row["fill_number"]

        # get the start and end date of the fill
        time_total = svcget('time_total', fill=fill_number).value()
        ds_fill = svcget('fillStartTime', fill=fill_number).value()
        de_fill = ds_fill + timedelta(seconds=time_total)

        # new algorithm to get the number of colliding bunches
        crossing_bunches = None

        s1 = svcget('Ncollisions_exp', ds=ds_fill, de=de_fill)
        if s1.y:
            crossing_bunches = max(s1.y)

        # update the database
        print fill_number, row["crossing_bunches"], crossing_bunches

        values = dict(id=row["id"],
                      crossing_bunches=crossing_bunches)

        insert_or_update(db, "measures_fills", **values)


if __name__ == "__main__":

    from argparse import ArgumentParser

    #.........................................................................
    #
    # user criteria
    #

    AGP = ArgumentParser()

    AGP.add_argument(
        "--db",
        default="triggerdb.sqlite",
        help="File name for the sqlite database [%(default)s].")

    ARGS = AGP.parse_args()

    log("Start fixNcollisions.py")
    print_options(vars(ARGS))

    #.........................................................................
    #
    # data base connection and sensors
    #

    DB = create_db(ARGS.db)
    SVC = create_sensors()

    #.........................................................................
    #
    # do the job and exit
    #

    fix(DB, SVC, ARGS)

    log("End fixNcollisions.py")
    sys.exit(0)

