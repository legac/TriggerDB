CREATE TABLE measures_runs (
id INTEGER NOT NULL AUTOINCREMENT ,
id_axis_times INTEGER NOT NULL ,
id_axis_triggers INTEGER NOT NULL ,
id_measures_fills INTEGER NOT NULL ,
id_measures_triggers INTEGER NOT NULL ,
run_number INTEGER NOT NULL ,
start_time  (Y-m-d H:M:S) VARCHAR NOT NULL ,
end_time  (Y-m-d H:M:S) VARCHAR NOT NULL ,
is_collision_alldetector_veloclosed INTEGER NOT NULL DEFAULT 0,
run_duration (second) DECIMAL DEFAULT NULL,
integrated_lumi (µb) DECIMAL DEFAULT NULL,
magnet_polarity (UP or DOWN) VARCHAR NOT NULL ,
beta_star (m) DECIMAL DEFAULT NULL,
avg_evt_size (Byte) DECIMAL DEFAULT NULL,
mdnrun_bb_rate (Hz) DECIMAL DEFAULT NULL,
mdnrun_l0_physics_rate (Hz) DECIMAL DEFAULT NULL,
mdnrun_l0_physics_raw_rate (Hz) INTEGER DEFAULT NULL,
mdnrun_total_hlt_physics_rate (Hz) DECIMAL DEFAULT NULL,
mdnrun_total_hlt_rate (Hz) DECIMAL DEFAULT NULL,
mdnrun_lumi (pubs) DECIMAL DEFAULT NULL,
mdnrun_mu DECIMAL DEFAULT NULL,
mdnrun_physics_dead_time (%) DECIMAL DEFAULT NULL,
mdnrun_hlt_time (second) DECIMAL DEFAULT NULL,
mdnrun_l0du_electron_rate (Hz) DECIMAL DEFAULT NULL,
mdnrun_l0du_dimuon_rate (Hz) INTEGER DEFAULT NULL,
mdnrun_l0du_hadron_rate (Hz) DECIMAL DEFAULT NULL,
mdnrun_l0du_muon_rate (Hz) DECIMAL DEFAULT NULL,
mdnrun_l0du_photon_rate (Hz) DECIMAL DEFAULT NULL,
mdnrun_hlt_l0_hadron_rate [RB13] (Hz) DECIMAL DEFAULT NULL,
mdnrun_hlt_l0_muon_dimuon_rate [RB15] (Hz) DECIMAL DEFAULT NULL,
mdnrun_hlt_exclusive_rate (Hz) DECIMAL DEFAULT NULL,
mdnrun_hlt_hadron_rate (Hz) DECIMAL DEFAULT NULL,
mdnrun_hlt_muon_rate (Hz) DECIMAL DEFAULT NULL,
mdnrun_hlt_lumi_rate [RB33] (Hz) DECIMAL DEFAULT NULL,
mdnrun_hlt_beamgas_rate [RB35] (Hz) DECIMAL DEFAULT NULL,
mdnrun_hlt_l0any_rate [RB39] (Hz) DECIMAL DEFAULT NULL,
mdnrun_hlt_microbias_rate [RB47] (Hz) DECIMAL DEFAULT NULL,
mdnrun_hlt_nobias_rate [RB48] (Hz) DECIMAL ,
mdnrun_hlt_singlemuon_rate [RB67] (Hz) DECIMAL ,
mdnrun_hlt_dimuon_rate [RB68] (Hz) DECIMAL ,
mdnrun_hlt_charm_rate [RB71] (Hz) DECIMAL ,
mdnrun_hlt_inclusivephi_rate [RB72] (Hz) DECIMAL ,
mdnrun_hlt_topo234body_rate [RB86] (Hz) DECIMAL ,
mdnrun_jpsi_rate (Hz) DECIMAL ,
mdnrun_inclusivephi_rate (Hz) DECIMAL ,
mdnrun_d0kpi_rate (Hz) DECIMAL ,
mdnrun_d0hhh_rate (Hz) DECIMAL ,
mdnrun_hlt_deferred (%) DECIMAL ,
PRIMARY KEY (id)
);

CREATE TABLE axis_times (
id INTEGER NOT NULL AUTOINCREMENT ,
year INTEGER NOT NULL ,
month INTEGER NOT NULL ,
week INTEGER NOT NULL ,
day (of the year) INTEGER NOT NULL ,
hour INTEGER NOT NULL ,
PRIMARY KEY (id)
);

CREATE TABLE axis_triggers (
id INTEGER NOT NULL AUTOINCREMENT ,
tck INTEGER NOT NULL ,
l0 INTEGER NOT NULL ,
hlt INTEGER NOT NULL ,
moore_version VARCHAR NOT NULL ,
nickname VARCHAR NOT NULL ,
PRIMARY KEY (id)
);

CREATE TABLE measures_fills (
id INTEGER NOT NULL AUTOINCREMENT ,
fill_number INTEGER NOT NULL ,
crossing_bunches INTEGER DEFAULT 0,
start_time  (Y-m-d H:M:S) VARCHAR NOT NULL ,
time_total (second) DECIMAL DEFAULT NULL,
time_hvon (second) DECIMAL DEFAULT NULL,
time_veloin (second) DECIMAL DEFAULT NULL,
time_running (second) DECIMAL DEFAULT NULL,
time_logged (second) DECIMAL DEFAULT 0,
lumi_total (µb) DECIMAL DEFAULT NULL,
lumi_hvon (µb) DECIMAL DEFAULT NULL,
lumi_veloin (µb) DECIMAL DEFAULT NULL,
lumi_running (µb) DECIMAL DEFAULT NULL,
lumi_logged (µb) DECIMAL DEFAULT NULL,
mdnfill_lumi (pubs) DECIMAL DEFAULT NULL,
mdnfill_mu DECIMAL DEFAULT NULL,
mdnfill_bb_rate (Hz) DECIMAL DEFAULT NULL,
mdnfill_l0_physics_rate (Hz) DECIMAL DEFAULT NULL,
mdnfill_l0_physics_raw_rate (Hz) DECIMAL DEFAULT NULL,
mdnfill_l0du_electron_rate (Hz) DECIMAL DEFAULT NULL,
mdnfill_l0du_dimuon_rate (Hz) DECIMAL DEFAULT NULL,
mdnfill_l0du_hadron_rate (Hz) DECIMAL DEFAULT NULL,
mdnfill_l0du_muon_rate (Hz) DECIMAL DEFAULT NULL,
mdnfill_l0du_photon_rate (Hz) DECIMAL DEFAULT NULL,
mdnfill_led_rate (Hz) DECIMAL DEFAULT NULL,
mdnfill_total_hlt_physics_rate (Hz) DECIMAL ,
mdnfill_total_hlt_rate (Hz) DECIMAL ,
mdnfill_hlt_deferred (%) DECIMAL ,
mdnfill_physcs_dead_time (%) DECIMAL ,
PRIMARY KEY (id)
);

CREATE TABLE measures_triggers (
id INTEGER NOT NULL AUTOINCREMENT ,
bb_rate_at_peak (Hz) DECIMAL DEFAULT NULL,
l0_rate_at_peak (Hz) DECIMAL DEFAULT NULL,
hlt1_physics_rate_at_peak (Hz) DECIMAL DEFAULT NULL,
hlt2_rate_at_peak (Hz) DECIMAL DEFAULT NULL,
hlt2_physics_rate_at_peak (Hz) DECIMAL DEFAULT NULL,
lumi_at_peak (pubs) DECIMAL DEFAULT NULL,
mu_at_peak DECIMAL DEFAULT NULL,
live_time_at_peak DECIMAL DEFAULT NULL,
avg_hlt_time_at_peak (second) DECIMAL DEFAULT NULL,
PRIMARY KEY (id)
);

CREATE TABLE scans (
id INTEGER NOT NULL AUTOINCREMENT ,
year INTEGER ,
last_fill INTEGER NOT NULL DEFAULT 0,
last_run INTEGER NOT NULL DEFAULT 0,
status (Y-m-d H:M:S) VARCHAR NOT NULL ,
PRIMARY KEY (id)
);

CREATE TABLE measures_days (
id INTEGER NOT NULL AUTOINCREMENT ,
year INTEGER NOT NULL ,
month INTEGER NOT NULL ,
week INTEGER NOT NULL ,
day (of the year) INTEGER NOT NULL ,
delivered_lumi (µb) DECIMAL NOT NULL ,
recorded_lumi (µb) DECIMAL NOT NULL ,
time_total (second) INTEGER NOT NULL ,
PRIMARY KEY (id)
);

