#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" NAME
        appL0TCK -- generate the documentation for L0 TCK

    SYNOPSIS
        appL0TCK [Options]

    DESCRIPTION
        Generate the documentation for L0 TCK
        The documentation is stored in the repository L0TCK

    OPTIONS
        --version
            Return the version number of this scripts

    EXAMPLES

        > lb-run MooreOnline/latest ./appL0TCK.py

    AUTHOR
        R. Le Gac, legac@cppm.in2p3.fr

"""
import os
import re
import sys

from base import (create_db,
                  is_inhibit,
                  log,
                  print_options)
from datetime import datetime
from TCKUtils.utils import getConfigTree

TCK_DIR = 'l0tcks'
TCK_FN = '%s.txt'


def decode(tckey):
    """ Decode the tree associate to each tck.

    """
    for node in getConfigTree(tckey):
        leaf = node.leaf
        if leaf and leaf.type == 'L0DUConfigProvider':
            return decodeL0(leaf)


def decodeL0(leaf):
    """ Decode the tree associate to the L0.

    Return a dictionary where the key is line name and the value
    a list of tuple (algorithm, property, value).

    Properties are 'AcceptFraction', 'FilterDescriptor', 'Crossing'

            leaf
                a leaf of the tree configuration.

    """
    # Calibration constants change for the Run2
    if OPT.year < 2013:
        ca, cm = 20, 40
    else:
        ca = 24
        cm = 50

    cvt_et = lambda m: m.group(1) + str(int(m.group(2)) * ca) + ' MeV'
    cvt_pt = lambda m: m.group(1) + str(int(m.group(2)) * cm) + ' MeV'
    cvt_pt2 = lambda m: m.group(1) + str(int(m.group(2)) * cm * cm) + ' Mev^2'

    f = lambda i: el[i][el[i].find('[') + 1:el[i].rfind(']')]

    # The element of the props list contains four items
    # ['name=[HCAL]',
    #  'rate==[100.0]',
    #  'conditions= [Hadron(Et)>12]',
    #  'MASK=[000]']
    # The condition can refer to another condition via it name

    # Transform the props list into a dictionary
    di = {}
    for el in eval(leaf.props['Channels']):

        line = f(0)

        di[line] = {'rate': float(f(1)) / 100.,
                    'conditions': f(2),
                    'mask': f(3)}

        # Clean the condition string
        s = di[line]['conditions']
        s = s.replace('[', '').replace(']', '')
        s = s.replace(' ', '').replace('&&', ' && ')

        # Et and Pt conversion in Mev
        s = re.sub(r'(\(Et\)[<>])([0-9]+)', cvt_et, s)
        s = re.sub(r'(SumEt[<>])([0-9]+)', cvt_et, s)
        s = re.sub(r'(Muon[1-2]\(Pt\)[<>])([0-9]+)', cvt_pt, s)
        s = re.sub(r'(Muon12\(Pt\)[<>])([0-9]+)', cvt_pt2, s)

        di[line]['conditions'] = s

        # keyword for crossing
        if di[line]['mask'] == '001':
            di[line]['crossing'] = 'every'

        elif di[line]['mask'] == '010':
            di[line]['crossing'] = 'beam1-empty'

        elif di[line]['mask'] == '100':
            di[f(0)]['crossing'] = 'empty-beam2'

    # resolve simple reference
    for k, v in di.iteritems():
        name = v['conditions']
        if name in di:
            di[k]['conditions'] = di[name]['conditions']

    # replace Spdxxx(Mult) by Spd(Mult)
    for k, v in di.iteritems():
        di[k]['conditions'] = \
        re.sub(r'Spd[A-Za-z0-9]+', 'Spd', di[k]['conditions'])

    # resolve compound reference
    for k, v in di.iteritems():
        for el in v['conditions'].split('&&'):
            s = el.strip()
            if s in di:
                di[k]['conditions'] = \
                di[k]['conditions'].replace(s, di[s]['conditions'])

    # store all lines definition into a dictionary
    lines = {}
    for name in di:
        if di[name]['mask'] == '000':
            continue

        li = [(name, 'AcceptFraction', di[name]['rate']),
              (name, 'FilterDescriptor', di[name]['conditions']),
              (name, 'Crossing', di[name]['crossing'])]

        lines["L0%s" % name] = li

    return lines


def generate_tck_documentation(tckey, out=sys.stdout):

    l0key = tckey & 0xFFFF
    lines = decode(tckey)
    if not lines:
        log('No documentation for 0x%x' % l0key)
        return

    log("Generate 0x%x documentation." % l0key)

    # header
    print >> out, "%s %s 0x%-8x %s" % ('*' * 40, 'TCK', l0key, '*' * 44)

    print >> out, "* %-97s*" % 'Energy, momentum in MeV'
    print >> out, "* %-97s*" % 'Muon12(Pt) is in MeV*MeV'

    msg = 'Acceptfraction 1. means no downscaling, 0.001 is 1/1000 etc.'
    print >> out, "* %-97s*" % msg
    print >> out, "%s%20s%s" % ('*' * 40, '*' * 20, '*' * 40)

    # L0 lines in alphabetic order
    li = list(lines.keys())
    li.sort()

    for line in li:
        print >> out, '\n', line
        for el in lines[line]:
            print >> out, "    %-20s %-17s %s" % el

    print >> out, '\n'


def do_doc(db, opt):
    """Produce the documentation for the TCKs.

    """
    # get the list of documentation files
    existing_tcks = []
    for filename in os.listdir(TCK_DIR):
        (tck, ext) = os.path.splitext(filename)
        existing_tcks.append(tck)

    # get the list of the tck in the database for the given year
    sql = """ SELECT DISTINCT l0, tck
              FROM axis_times, axis_triggers, measures_runs
              WHERE measures_runs.id_axis_times=axis_times.id
              AND measures_runs.id_axis_triggers=axis_triggers.id
              AND axis_times.year=?  """

    # generate documentation for missing keys
    for row in db.execute(sql, (opt.year,)):
        l0tck = '0x%x' % row['l0']
        tckey = row['tck']
        if l0tck not in existing_tcks and tckey > 0:
            fn = os.path.join(TCK_DIR, TCK_FN % l0tck)
            fi = open(fn, 'wb')
            generate_tck_documentation(tckey, out=fi)
            fi.close()
            existing_tcks.append(l0tck)


if __name__ == "__main__":

    from argparse import ArgumentParser

    # script options
    AGP = ArgumentParser()

    AGP.add_argument(
        "--db",
        default="triggerdb.sqlite",
        help="File name for the sqlite database [%(default)s].",
        metavar="<name>")

    AGP.add_argument(
        "-y", "--year",
        default=datetime.now().year,
        help="Select the year [%(default)s].",
        metavar="<yyyy>",
        type=int)

    OPT = AGP.parse_args()

    log("Start appL0TCK.py")
    print_options(vars(OPT))

    # the sqlite database
    DB = create_db(OPT.db)

    # inhibit state exit immediately
    if is_inhibit(DB, OPT.year):
        log("The database is in inhibit state, exit")
        sys.exit(0)

    # create the directory
    if not os.path.exists(TCK_DIR):
        os.mkdir(TCK_DIR)

    # produce the documentation
    do_doc(DB, OPT)

    # exit
    log("End appL0TCK.py")
    sys.exit(0)
